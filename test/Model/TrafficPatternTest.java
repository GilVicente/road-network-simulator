/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Daniela Ferreira
 */
public class TrafficPatternTest {
    
    public TrafficPatternTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getBegin method, of class TrafficPattern.
     */
    @Test
    public void testGetBegin() {
        System.out.println("getBegin");
        TrafficPattern instance = new TrafficPattern();
        instance.setBegin("begin");
        String expResult = "begin";
        String result = instance.getBegin();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setBegin method, of class TrafficPattern.
     */
    @Test
    public void testSetBegin() {
        System.out.println("setBegin");
        String begin = "begin";
        TrafficPattern instance = new TrafficPattern();
        instance.setBegin(begin);
       
    }

    /**
     * Test of getEnd method, of class TrafficPattern.
     */
    @Test
    public void testGetEnd() {
        System.out.println("getEnd");
        TrafficPattern instance = new TrafficPattern();
        instance.setEnd("end");
        String expResult = "end";
        String result = instance.getEnd();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setEnd method, of class TrafficPattern.
     */
    @Test
    public void testSetEnd() {
        System.out.println("setEnd");
        String end = "end";
        TrafficPattern instance = new TrafficPattern();
        instance.setEnd(end);
        
    }

    /**
     * Test of getlVeh method, of class TrafficPattern.
     */
    @Test
    public void testGetlVeh() {
        System.out.println("getlVeh");
        TrafficPattern instance = new TrafficPattern();
        instance.setlVeh("veh");
        String expResult = "veh";
        String result = instance.getlVeh();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setlVeh method, of class TrafficPattern.
     */
    @Test
    public void testSetlVeh() {
        System.out.println("setlVeh");
        String lVeh = "veh";
        TrafficPattern instance = new TrafficPattern();
        instance.setlVeh(lVeh);
      
    }

    /**
     * Test of getArrival_rate method, of class TrafficPattern.
     */
    @Test
    public void testGetArrival_rate() {
        System.out.println("getArrival_rate");
        TrafficPattern instance = new TrafficPattern();
        instance.setArrival_rate("rate");
        String expResult = "rate";
        String result = instance.getArrival_rate();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setArrival_rate method, of class TrafficPattern.
     */
    @Test
    public void testSetArrival_rate() {
        System.out.println("setArrival_rate");
        String arrival_rate = "rate";
        TrafficPattern instance = new TrafficPattern();
        instance.setArrival_rate(arrival_rate);
        
    }
    
}
