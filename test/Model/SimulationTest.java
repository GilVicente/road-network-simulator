/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Vehicle.Vehicle;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import road.RoadNetwork;

/**
 *
 * @author Daniela Ferreira
 */
public class SimulationTest {
    
    public SimulationTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getRn method, of class Simulation.
     */
    @Test
    public void testGetRn() {
        System.out.println("getRn");
        Simulation instance = new Simulation();
        RoadNetwork expResult = new RoadNetwork();
        RoadNetwork result = instance.getRn();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setRn method, of class Simulation.
     */
    @Test
    public void testSetRn() {
        System.out.println("setRn");
        RoadNetwork rn = new RoadNetwork();
        Simulation instance = new Simulation();
        instance.setRn(rn);
       
    }

    /**
     * Test of getlVeh method, of class Simulation.
     */
    @Test
    public void testGetlVeh() {
        System.out.println("getlVeh");
        Simulation instance = new Simulation();
        ArrayList<Vehicle> expResult = new ArrayList<>();
        ArrayList<Vehicle> result = instance.getlVeh();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setlVeh method, of class Simulation.
     */
    @Test
    public void testSetlVeh() {
        System.out.println("setlVeh");
        ArrayList<Vehicle> lVeh = new ArrayList<>();
        Simulation instance = new Simulation();
        instance.setlVeh(lVeh);
        
    }

    /**
     * Test of getId method, of class Simulation.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        Simulation instance = new Simulation();
        instance.setId("ID");
        String expResult = "ID";
        String result = instance.getId();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setId method, of class Simulation.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        String id = "ID";
        Simulation instance = new Simulation();
        instance.setId(id);
        
    }

    /**
     * Test of getDescription method, of class Simulation.
     */
    @Test
    public void testGetDescription() {
        System.out.println("getDescription");
        Simulation instance = new Simulation();
        instance.setDescription("descr");
        String expResult = "descr";
        String result = instance.getDescription();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setDescription method, of class Simulation.
     */
    @Test
    public void testSetDescription() {
        System.out.println("setDescription");
        String description = "descr";
        Simulation instance = new Simulation();
        instance.setDescription(description);
        
    }

    /**
     * Test of getProjectName method, of class Simulation.
     */
    @Test
    public void testGetProjectName() {
        System.out.println("getProjectName");
        Simulation instance = new Simulation();
        instance.setProjectName("Project");
        String expResult = "Project";
        String result = instance.getProjectName();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setProjectName method, of class Simulation.
     */
    @Test
    public void testSetProjectName() {
        System.out.println("setProjectName");
        String projectName = "Project";
        Simulation instance = new Simulation();
        instance.setProjectName(projectName);
        
    }

    /**
     * Test of getlPattern method, of class Simulation.
     */
    @Test
    public void testGetlPattern() {
        System.out.println("getlPattern");
        Simulation instance = new Simulation();
        ArrayList<TrafficPattern> expResult = new ArrayList<>();
        ArrayList<TrafficPattern> result = instance.getlPattern();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setlPattern method, of class Simulation.
     */
    @Test
    public void testSetlPattern() {
        System.out.println("setlPattern");
        ArrayList<TrafficPattern> lPattern = new ArrayList<>();
        Simulation instance = new Simulation();
        instance.setlPattern(lPattern);
        
    }

    /**
     * Test of addTrafficPattern method, of class Simulation.
     */
    @Test
    public void testAddTrafficPattern() {
        System.out.println("addTrafficPattern");
        TrafficPattern t = new TrafficPattern();
        Simulation instance = new Simulation();
        instance.addTrafficPattern(t);
        
    }
    
}
