
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Vehicle.Vehicle;
import java.io.File;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import road.RoadNetwork;

/**
 *
 * @author Daniela Ferreira
 */
public class ProjectTest {
    
    public ProjectTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getName method, of class Project.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Project instance = new Project();
        instance.setName("P1");
        String expResult = "P1";
        String result = instance.getName();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setName method, of class Project.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "P1";
        Project instance = new Project();
        instance.setName(name);
        
    }

    /**
     * Test of getDescription method, of class Project.
     */
    @Test
    public void testGetDescription() {
        System.out.println("getDescription");
        Project instance = new Project();
        instance.setDescription("project description");
        String expResult = "project description";
        String result = instance.getDescription();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setDescription method, of class Project.
     */
    @Test
    public void testSetDescription() {
        System.out.println("setDescription");
        String description = "project description";
        Project instance = new Project();
        instance.setDescription(description);
    }

    /**
     * Test of getRn_dir method, of class Project.
     */
    @Test
    public void testGetRn_dir() {
        System.out.println("getRn_dir");
        Project instance = new Project();
        File expResult = null;
        File result = instance.getRn_dir();
        assertEquals(expResult, result);
    }

    /**
     * Test of getVeh_dir method, of class Project.
     */
    @Test
    public void testGetVeh_dir() {
        System.out.println("getVeh_dir");
        Project instance = new Project();
        File expResult = null;
        File result = instance.getVeh_dir();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of getRn method, of class Project.
     */
    @Test
    public void testGetRn() {
        System.out.println("getRn");
        Project instance = new Project();
        RoadNetwork expResult = new RoadNetwork();
        RoadNetwork result = instance.getRn();
        assertEquals(expResult, result);
    }

    /**  
     * Test of setRn_dir method, of class Project.
     */
    @Test
    public void testSetRn_dir() {
        System.out.println("setRn_dir");
        File rn_dir = null;
        Project instance = new Project();
        instance.setRn_dir(rn_dir);
        
    }

    /**
     * Test of setVeh_dir method, of class Project.
     */
    @Test
    public void testSetVeh_dir() {
        System.out.println("setVeh_dir");
        File veh_dir = null;
        Project instance = new Project();
        instance.setVeh_dir(veh_dir);
       
    }

    /**
     * Test of setRn method, of class Project.
     */
    @Test
    public void testSetRn() {
        System.out.println("setRn");
        RoadNetwork rn = new RoadNetwork();
        Project instance = new Project();
        instance.setRn(rn);
        
    }
  

    /**
     * Test of getListVehicles method, of class Project.
     */
    @Test
    public void testGetListVehicles() {
        System.out.println("getListVehicles");
        Project instance = new Project();
        ArrayList<Vehicle> expResult = new ArrayList();
        ArrayList<Vehicle> result = instance.getListVehicles();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setListVehicles method, of class Project.
     */
    @Test
    public void testSetListVehicles() {
        System.out.println("setListVehicles");
        ArrayList<Vehicle> listVehicles = new ArrayList();
        Project instance = new Project();
        instance.setListVehicles(listVehicles);
        
    }
    
}
