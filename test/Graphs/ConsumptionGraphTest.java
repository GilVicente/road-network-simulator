/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Graphs;

import AnalisysController.EfficientController;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import road.Junction;

/**
 *
 * @author Daniela Ferreira
 */
public class ConsumptionGraphTest {
    
    public ConsumptionGraphTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of insertAllNodes method, of class ConsumptionGraph.
     */
    @Test
    public void testInsertAllNodes() {
        System.out.println("insertAllNodes");
        boolean isDirected =true;
        EfficientController efc=null;
        ConsumptionGraph instance = new ConsumptionGraph(efc, isDirected);
        boolean expResult = true;
        boolean result = instance.insertAllNodes();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of addNode method, of class ConsumptionGraph.
     */
    @Test
    public void testAddNode() {
        System.out.println("addNode");
        String a = "a";
        boolean isDirected= true;
        EfficientController efc =null;
        ConsumptionGraph instance = new ConsumptionGraph(efc, isDirected);
        boolean expResult = true;
        boolean result = instance.addNode(a);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of insertAllSections method, of class ConsumptionGraph.
     */
    @Test
    public void testInsertAllSections() {
        System.out.println("insertAllSections");
        boolean isDirected = true;
        EfficientController efc=null;
        ConsumptionGraph instance = new ConsumptionGraph(efc, isDirected);
        instance.insertAllSections();
        
    }

    /**
     * Test of addEdge method, of class ConsumptionGraph.
     */
    @Test
    public void testAddEdge() {
        System.out.println("addEdge");
        String n1 = "n1";
        String n2 = "n2";
        Double weight = 0.0;
        
        int x = 0;
        boolean isDirected=true;
        EfficientController efc=null;
        ConsumptionGraph instance = new ConsumptionGraph(efc, isDirected);
        boolean expResult = true;
        boolean result = instance.addEdge(n1, n2, weight, x);
        assertEquals(expResult, result);
       
    }

    /**
     * Test of getAllPaths method, of class ConsumptionGraph.
     */
    @Test
    public void testGetAllPaths() {
        System.out.println("getAllPaths");
        Junction a = new Junction();
        Junction b = new Junction();
        EfficientController efc = null;
        boolean isDirected=true;
        ConsumptionGraph instance = new ConsumptionGraph(efc, isDirected);
        ArrayList<Deque<String>> paths = new ArrayList<Deque<String>>();
        HashMap<ArrayList<Junction>, Double> map = new HashMap<ArrayList<Junction>, Double>();
        for(Deque<String> p: paths){
            ArrayList<Junction> lnodes=new ArrayList<>();
        }
        HashMap<ArrayList<Junction>, Double> expResult = instance.getAllPaths(a, b);
        HashMap<ArrayList<Junction>, Double> result = instance.getAllPaths(a, b);
        assertEquals(expResult, result);
    }

    /**
     * Test of getPathConsumption method, of class ConsumptionGraph.
     */
    @Test
    public void testGetPathConsumption() {
        System.out.println("getPathConsumption");
        ArrayList<Junction> lNodes = new ArrayList();
        EfficientController efc=null;
        boolean isDirected=true;
        ConsumptionGraph instance = new ConsumptionGraph(efc, isDirected);
        double expResult = 0.0;
        double result = instance.getPathConsumption(lNodes);
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of getLessConsumptionPath method, of class ConsumptionGraph.
     */
    @Test
    public void testGetLessConsumptionPath() {
        System.out.println("getLessConsumptionPath");
        Junction a = new Junction();
        Junction b = new Junction();
        boolean isDirected=true;
        EfficientController efc=null;
        ConsumptionGraph instance = new ConsumptionGraph(efc, isDirected);
        ArrayList<Junction> expResult = new ArrayList<Junction> ();
        ArrayList<Junction> result = instance.getLessConsumptionPath(a, b);
        assertEquals(expResult, result);
       
    }

    /**
     * Test of lessConsumptionPathLength method, of class ConsumptionGraph.
     */
    @Test
    public void testLessConsumptionPathLength() {
        System.out.println("lessConsumptionPathLength");
        Junction a = new Junction();
        Junction b = new Junction();
        boolean isDirected=true;
        EfficientController efc=null;
        ConsumptionGraph instance = new ConsumptionGraph(efc, isDirected);
        double expResult = 0.0;
        double result = instance.lessConsumptionPathLength(a, b);
        assertEquals(expResult, result, 0.0);
        
    }
    
}
