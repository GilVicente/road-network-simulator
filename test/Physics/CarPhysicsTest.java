/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Physics;

import static Physics.CarPhysics.calculateMaxVel;
import static Physics.CarPhysics.calculateRPM;
import static Physics.CarPhysics.calculateTorque;
import static Physics.CarPhysics.calculateVRpmVehicle;
import static Physics.CarPhysics.conv;
import static Physics.CarPhysics.getSentido;
import static Physics.CarPhysics.relVelocity;
import static Physics.CarPhysics.toRadians;
import Utils.Utils;
import Vehicle.Energy;
import Vehicle.Gear;
import Vehicle.Regime;
import Vehicle.Throttle;
import Vehicle.Vehicle;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import road.Section;
import road.Segment;

/**
 *
 * @author Gil
 */
public class CarPhysicsTest {

    public CarPhysicsTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testConsumo() {

    }
//
//    /**
//     * Test of planeForce method, of class CarPhysics.
//     */
//    @Test
//    public void testPlaneForce() {
//        System.out.println("planeForce");
//        Vehicle vehicle = null;
//        Section sec = null;
//        Segment seg = null;
//        int throttle_id = 0;
//        int gear = 0;
//        double expResult = 0.0;
//        double result = CarPhysics.planeForce(vehicle, sec, seg, throttle_id, gear);
//        assertEquals(expResult, result, 0.0);
//    }
//
//    /**
//     * Test of slopeForce method, of class CarPhysics.
//     */
//    @Test
//    public void testSlopeForce() {
//        System.out.println("slopeForce");
//        Vehicle vehicle = null;
//        Section sec = null;
//        Segment seg = null;
//        int throttle_id = 0;
//        int gear = 0;
//        double expResult = 0.0;
//        double result = CarPhysics.slopeForce(vehicle, sec, seg, throttle_id, gear);
//        assertEquals(expResult, result, 0.0);
//    }
//
//    /**
//     * Test of relVelocity method, of class CarPhysics.
//     */
//    @Test
//    public void testRelVelocity() {
//        System.out.println("relVelocity");
//        double vMax_segm = 0.0;
//        double wind_speed = 0.0;
//        double wind_direc = 0.0;
//        int sentido = 0;
//        double expResult = 0.0;
//        double result = CarPhysics.relVelocity(vMax_segm, wind_speed, wind_direc, sentido);
//        assertEquals(expResult, result, 0.0);
//    }
//
//    /**
//     * Test of calculateRPM method, of class CarPhysics.
//     */
//    @Test
//    public void testCalculateRPM() {
//        System.out.println("calculateRPM");
//        double G = 0.0;
//        double gk = 0.0;
//        double r = 0.0;
//        double v = 0.0;
//        double expResult = 0.0;
//        double result = CarPhysics.calculateRPM(G, gk, r, v);
//        assertEquals(expResult, result, 0.0);
//    }
//
//    /**
//     * Test of calculateTorque method, of class CarPhysics.
//     */
//    @Test
//    public void testCalculateTorque() {
//        System.out.println("calculateTorque");
//        double rpm = 0.0;
//        Vehicle v = null;
//        int id = 0;
//        double expResult = 0.0;
//        double result = CarPhysics.calculateTorque(rpm, v, id);
//        assertEquals(expResult, result, 0.0);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of calculateSFC method, of class CarPhysics.
//     */
//    @Test
//    public void testCalculateSFC() {
//        System.out.println("calculateSFC");
//        double rpm = 0.0;
//        Vehicle v = null;
//        int id = 0;
//        double expResult = 0.0;
//        double result = CarPhysics.calculateSFC(rpm, v, id);
//        assertEquals(expResult, result, 0.0);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of conv method, of class CarPhysics.
//     */
//    @Test
//    public void testConv() {
//        System.out.println("conv");
//        double v = 0.0;
//        double expResult = 0.0;
//        double result = CarPhysics.conv(v);
//        assertEquals(expResult, result, 0.0);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of calculateWork method, of class CarPhysics.
//     */
//    @Test
//    public void testCalculateWork() {
//        System.out.println("calculateWork");
//        double f = 0.0;
//        double d = 0.0;
//        double expResult = 0.0;
//        double result = CarPhysics.calculateWork(f, d);
//        assertEquals(expResult, result, 0.0);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of calculatePower method, of class CarPhysics.
//     */
//    @Test
//    public void testCalculatePower() {
//        System.out.println("calculatePower");
//        Vehicle v = null;
//        Segment seg = null;
//        int throttle_id = 0;
//        int g = 0;
//        double expResult = 0.0;
//        double result = CarPhysics.calculatePower(v, seg, throttle_id, g);
//        assertEquals(expResult, result, 0.0);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of calculateFuelGrams method, of class CarPhysics.
//     */
//    @Test
//    public void testCalculateFuelGrams() {
//        System.out.println("calculateFuelGrams");
//        double P = 0.0;
//        Vehicle v = null;
//        Segment s = null;
//        int gear = 0;
//        int throttle_id = 0;
//        double expResult = 0.0;
//        double result = CarPhysics.calculateFuelGrams(P, v, s, gear, throttle_id);
//        assertEquals(expResult, result, 0.0);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of fromGramsToGas method, of class CarPhysics.
//     */
//    @Test
//    public void testFromGramsToGas() {
//        System.out.println("fromGramsToGas");
//        double g = 0.0;
//        double expResult = 0.0;
//        double result = CarPhysics.fromGramsToGas(g);
//        assertEquals(expResult, result, 0.0);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of cons method, of class CarPhysics.
//     */
//    @Test
//    public void testCons() {
//        System.out.println("cons");
//        double f = 0.0;
//        double expResult = 0.0;
//        double result = CarPhysics.cons(f);
//        assertEquals(expResult, result, 0.0);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of toRadians method, of class CarPhysics.
//     */
//    @Test
//    public void testToRadians() {
//        System.out.println("toRadians");
//        double inc = 0.0;
//        double expResult = 0.0;
//        double result = CarPhysics.toRadians(inc);
//        assertEquals(expResult, result, 0.0);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

//    @Test
//    public void test() {
//        double en = 0.0;
//        Vehicle v = Dummy01();
//        Section s = n0n2();
//        double l = 0.0;
//
//        for (Segment seg : s.getlSegments()) {
//            if (v.getMotor().equalsIgnoreCase("Combustion")) {
//                int g = v.getEnergy().GetMaxGear(); //GETMAXGEAR
//                System.out.println("");
//                System.out.println("INITIAL GEAR: " + g);
//                if (Utils.withoutPercent(seg.getSlope()) != 0) {
//                    double f = CarPhysics.slopeForce(v, s, seg, 25, g);
//                    System.out.println("Gear " + g + ": " + f + "N");
//                    while (f <= 0 && g > 0) {
//                        g--;
//                        f = CarPhysics.slopeForce(v, s, seg, 25, g);
//                        System.out.println("Gear " + g + ": " + f + "N");
//                    }
//
//                    double w = CarPhysics.calculateWork(f, (Utils.fromKmToDouble(seg.getLenght())) * 1000);
//                    double grams = (w/1000) / 44.4;
//
//                    System.out.println(seg.getId_segment() + ": " + w / 1000 + "kj");
//                    l += (grams / 746);
//                    
//                    en += w;
//                }
//
//            }
//        }
//        System.out.println("");
//        System.out.println("Work: " + en / 1000 + " kj");
//        System.out.println("Gas: " + l + " l");
//    }
    @Test
    public void windTest() {
        double vMax_segm = 120;
        double wind_speed = 3;
        double wind_direc = -5;

        double vRel = relVelocity(vMax_segm, wind_speed, wind_direc, -1);
        System.out.println("VREL: " + vRel + " Km/h");
        double v = conv(vRel);
        System.out.println("V: " + v + " m/s");

    }

    public Vehicle Dummy01() {
        Vehicle v = new Vehicle();
        v.setName("Dummy01");
        v.setMotor("combustion");
        v.setFuel("gasoline");
        v.setMass("2400 kg");
        v.setDrag_coef(0.38);
        v.setFrontal_area(2.4);
        v.setRrc(0.01);
        v.setWheel_size(0.7);
        Energy e = new Energy();

        e.setMin_rpm(1000);
        e.setMax_rpm(5500);
        e.setFinal_drive_ratio(2.6);

        Gear g1 = new Gear(1, 3.5);
        Gear g2 = new Gear(2, 2.5);
        Gear g3 = new Gear(3, 1.9);
        Gear g4 = new Gear(4, 1.2);
        Gear g5 = new Gear(5, 0.8);
        ArrayList<Gear> lG = new ArrayList();
        lG.add(g1);
        lG.add(g2);
        lG.add(g3);
        lG.add(g4);
        lG.add(g5);

        e.setlGear(lG);
        ArrayList<Throttle> lT = new ArrayList();
        ArrayList<Regime> lReg = new ArrayList();
        lReg.add(new Regime(105, 1000, 2499, 8.2));
        lReg.add(new Regime(115, 2500, 3999, 6.2));
        lReg.add(new Regime(100, 4000, 5500, 10.2));

        Throttle t1 = new Throttle();
        t1.setId(25);
        t1.setlRegime(lReg);
        lT.add(t1);
        e.setlThrottle(lT);
        v.setEnergy(e);
        return v;

    }

    @Test
    public void testMotorForce() {
        double G = 3.6;             //final_drive_ratio_
        double r = 0.30; //metros
        double gk = 0.9;
        double torque = 130;

        double f = ((torque * G * gk) / r);
        System.out.println("Motor Force: " + f);
    }

    @Test
    public void testResistanceForces() {
        double rrc = 0.010;
        double r = 0.30; //metros
        double m = 1820;
        double frontal_Area = 1.800;
        double air_drag = 0.30;
        double ang = 0.030;
        double v = 42.270;

        double eP = m * 9.8 * Math.sin(ang);
        System.out.println("Gravitational Force: " + eP);
        
        double roll = rrc * m * 9.8 + (1 / 2);
        System.out.println("Rolling resistance: "+roll);
        
        double air = 0.5 *air_drag * frontal_Area * 1.255 * Math.pow(v, 2);
        System.out.println("Air resistance: "+air);
        
        double f = roll + air + eP;
        System.out.println("Braking Force: " + f);
    }

//    public Section n0n2() {
//        Section s = new Section("E01", 1, "n0", "n2", "regular road", "bidrectional", 0, "5 m/s", 20);
//        ArrayList<Segment> lSeg = new ArrayList();
//        Segment s01 = new Segment(1, 100, "3.0%", "3.2 Km", 250, "90 km/h", "0 Km/h");
//        Segment s02 = new Segment(2, 196, "-1.5%", "6.4 Km", 200, "90 km/h", "0 Km/h");
//        lSeg.add(s01);
//        lSeg.add(s02);
//        s.setlSegments(lSeg);
//        return s;
//    }
}
