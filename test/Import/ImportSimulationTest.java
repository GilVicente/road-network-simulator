/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Import;

import Model.Simulation;
import Model.TrafficPattern;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.ParserConfigurationException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.xml.sax.SAXException;

/**
 *
 * @author Luís Rocha
 */
public class ImportSimulationTest {

    public ImportSimulationTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of ImportSimulation method, of class ImportSimulation.
     */
    @Test
    public void testImportSimulation() throws ParserConfigurationException, SAXException, IOException {
        System.out.println("\nImportSimulation");
        String pathFile = "TestSet02_Simulation.xml";
        ImportSimulation instance = new ImportSimulation(pathFile);
        instance.ImportSimulation();
        ArrayList<Simulation> a = instance.getlSimulation();
        for (Simulation s : a) {
            System.out.print("\nID:" + s.getId() + "\n"
                    + "Description:" + s.getDescription());
        }
        System.out.print("\n");

    }

    /**
     * Test of ImportTrafficPattern method, of class ImportSimulation.
     */
    @Test
    public void testImportAll() throws ParserConfigurationException, SAXException, IOException {
        System.out.println("\nImportTrafficPattern");
        String pathFile = "TestSet02_Simulation.xml";
        ImportSimulation instance = new ImportSimulation(pathFile);
        instance.importAll();
        ArrayList<Simulation> lSimulation = instance.getlSimulation();
        for (Simulation v : lSimulation) {
            ArrayList<TrafficPattern> a = v.getlPattern();
            for (TrafficPattern t : a) {
                System.out.print("\nBegin:" + t.getBegin() + "\n"
                        + "End:" + t.getEnd() + "\n"
                        + "Arrival Rate:" + t.getArrival_rate() + "\n" + 
                        "Vehicle:" + t.getlVeh() + "\n");
            }
            System.out.print("\n");
        }

    }

}
