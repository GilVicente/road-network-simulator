/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Import;

import Vehicle.Energy;
import Vehicle.Gear;
import Vehicle.Regime;
import Vehicle.Throttle;
import Vehicle.Vehicle;
import Vehicle.Velocity;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.ParserConfigurationException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.xml.sax.SAXException;

/**
 *
 * @author Luís Rocha
 */
public class ImportVehiclesTest {

    public ImportVehiclesTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of ImportVehicle method, of class ImportVehicles.
     */
    @Test
    public void testImportVehicle() throws ParserConfigurationException, SAXException, IOException {
        System.out.println("\nImportVehicle");
        String pathFile = "TestSet02_Vehicles.xml";
        Vehicle v = new Vehicle();
        ImportVehicles instance = new ImportVehicles(pathFile);
        instance.ImportVehicle();
        ArrayList<Vehicle> a = instance.getlVehicles();
        for (Vehicle vehichle : a) {
            System.out.print("\nKey:" + vehichle.getName() + "\n"
                    + "Description:" + vehichle.getDescription() + "\n"
                    + "Type:" + vehichle.getType() + "\n"
                    + "Fuel:" + vehichle.getFuel() + "\n"
                    + "Load:" + vehichle.getLoad() + "\n"
                    + "Mass:" + vehichle.getMass() + "\n"
                    + "Motor:" + vehichle.getMotor());
        }
        System.out.print("\n");
    }

    /**
     * Test of ImportEnergy method, of class ImportVehicles.
     */
    @Test
    public void testImportEnergy() throws ParserConfigurationException, SAXException, IOException {
        System.out.println("\nImportEnergy");
        String pathFile = "TestSet02_Vehicles.xml";
        ImportVehicles instance = new ImportVehicles(pathFile);
        instance.importAll();
        ArrayList<Vehicle> lVehicles = instance.getlVehicles();
        for (Vehicle v : lVehicles) {
            Energy energy = v.getEnergy();
            System.out.print("Max RPM:" + energy.getMax_rpm() + "\n"
                    + "MinRPM" + energy.getMin_rpm() + "\n"
                    + "Final drive ratio:" + energy.getFinal_drive_ratio() + "\n"
            );
        }
        System.out.print("\n");
    }

    /**
     * Test of ImportVelocity method, of class ImportVehicles.
     */
    @Test
    public void testImportVelocity() throws ParserConfigurationException, SAXException, IOException {
        System.out.println("\nImportVelocity");
        String pathFile = "TestSet02_Vehicles.xml";
        ImportVehicles instance = new ImportVehicles(pathFile);
        instance.importAll();
        ArrayList<Vehicle> lVehicles = instance.getlVehicles();
        for (Vehicle v : lVehicles) {
            ArrayList<Velocity> a = v.getlVelocity();
            for (Velocity c : a) {
                System.out.print("Limit:" + c.getLimit() + "\n"
                        + "Segment type:" + c.getSegment_type());
            }
            System.out.print("\n");
        }
    }

    /**
     * Test of ImportGear method, of class ImportVehicles.
     */
    @Test
    public void testImportGear() throws ParserConfigurationException, SAXException, IOException {
        System.out.println("\nImportGear");
        String pathFile = "TestSet02_Vehicles.xml";
        ImportVehicles instance = new ImportVehicles(pathFile);
        instance.importAll();
        ArrayList<Vehicle> lVehicles = instance.getlVehicles();
        for (Vehicle v : lVehicles) {
            ArrayList<Gear> a = v.getEnergy().getlGear();
            for (Gear gear : a) {
                System.out.print("Gear ID key:" + gear.getId() + "\n"
                        + "Gear Ratio:" + gear.getRatio() + "\n");
            }
            System.out.print("\n");
        }

    }

    /**
     * Test of ImportThrottle method, of class ImportVehicles.
     */
    @Test
    public void testImportThrottle() throws ParserConfigurationException, SAXException, IOException {
        System.out.println("\nImportThrottle");
        String pathFile = "TestSet02_Vehicles.xml";
        ImportVehicles instance = new ImportVehicles(pathFile);
        instance.importAll();
        ArrayList<Vehicle> lVehicles = instance.getlVehicles();
        for (Vehicle v : lVehicles) {
            ArrayList<Throttle> a = v.getEnergy().getlThrottle();
            for (Throttle t : a) {
                System.out.print("\nID key:" + t.getId());
            }
        }

    }

    /**
     * Test of ImportRegime method, of class ImportVehicles.
     */
    @Test
    public void testImportRegime() throws ParserConfigurationException, SAXException, IOException {
        System.out.println("ImportRegime");
        String pathFile = "TestSet02_Vehicles.xml";
        ImportVehicles instance = new ImportVehicles(pathFile);
        instance.importAll();
        ArrayList<Vehicle> lVehicles = instance.getlVehicles();
        for (Vehicle v : lVehicles) {
            ArrayList<Throttle> a = v.getEnergy().getlThrottle();
            for (Throttle a1 : a) {
                System.out.println("T ID:"+a1.getId());
                ArrayList<Regime> b = a1.getlRegime();
                for (Regime t : b) {
                    System.out.print("\nTorque: " + t.getTorque()
                            + "\nRPM High: " + t.getRpm_high()
                            + "\nRPM Low: " + t.getRpm_low() + "\nSFC: " + t.getSFC());
                }
                System.out.print("\n");
            }

        }

    }

}
