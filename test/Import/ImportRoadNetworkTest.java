/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Import;

import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.ParserConfigurationException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.xml.sax.SAXException;
import road.Junction;
import road.RoadNetwork;
import road.Section;
import road.Segment;

/**
 *
 * @author Windows 8
 */
public class ImportRoadNetworkTest {

    public ImportRoadNetworkTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of importJunction method, of class ImportRoadNetwork.
     */
    @Test
    public void testImportJunction() throws ParserConfigurationException, SAXException, IOException {
        System.out.println("\nimportJunction");
        String pathFile = "import.xml";
        RoadNetwork roadNetwork = new RoadNetwork();
        ImportRoadNetwork instance = new ImportRoadNetwork(roadNetwork, pathFile);
        instance.importJunction();
        ArrayList<Junction> a = roadNetwork.getlNodes();
        for (Junction j : a) {
            System.out.print("Junction key:" + j.getKey() + "\n");
        }
            System.out.print("\n");
    }

    /**
     * Test of importRoadSection method, of class ImportRoadNetwork.
     */
    @Test
    public void testImportRoadSection() throws ParserConfigurationException, SAXException, IOException {
        System.out.println("\nImportRoadSection");
        String pathFile = "import.xml";
        RoadNetwork roadNetwork = new RoadNetwork();
        ImportRoadNetwork instance = new ImportRoadNetwork(roadNetwork, pathFile);
        instance.importRoadSection();
        ArrayList<Section> a = roadNetwork.getlSections();
        for (Section s : a) {
            System.out.print("\nBeginnig Node:" + s.getBeginning_node() + "\nEnding Node:" + s.getEnding_node()
                    + "\nID road:" + s.getId_road() + "\nType:" + s.getTypology()
                    + "\nDirection" + s.getDirection() + "\nToll:" + s.getToll() + "\nWind speed:"
                    + s.getWind_speed() + "\nWind Angle " + +s.getWind_angle());

        }
        System.out.print("\n");
    }

    /**
     * Test of importSegment method, of class ImportRoadNetwork.
     */
    @Test
    public void testImportSegment() throws ParserConfigurationException, SAXException, IOException {
        System.out.println("\nimportSegment");
        String pathFile = "import.xml";
        RoadNetwork roadNetwork = new RoadNetwork();
        ImportRoadNetwork instance = new ImportRoadNetwork(roadNetwork, pathFile);
        instance.importSegment();
        ArrayList<Segment> a = roadNetwork.getlSegments();
        System.out.print("\n");
        for (Segment s : a) {
            System.out.print("\nSegment ID:" + s.getId_segment() + "\nHeight" + s.getHeight() + "\nSlope: "
                    + s.getSlope() + "\nLenght: " + s.getLenght() + "\nMax. Vel: " + s.getMaxVel() + "\nMin. Vel:" + s.getMinVel());

        }
        System.out.print("\n");
    }

}
