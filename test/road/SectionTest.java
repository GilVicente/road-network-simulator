/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package road;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Daniela Ferreira
 */
public class SectionTest {
    
    public SectionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getBeginning_node method, of class Section.
     */
    @Test
    public void testGetBeginning_node() {
        System.out.println("getBeginning_node");
        Section instance = new Section();
        String expResult = "node";
        instance.setBeginning_node("node");
        String result = instance.getBeginning_node();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setBeginning_node method, of class Section.
     */
    @Test
    public void testSetBeginning_node() {
        System.out.println("setBeginning_node");
        String beginning_node = "node";
        Section instance = new Section();
        instance.setBeginning_node(beginning_node);
        
    }

    /**
     * Test of getEnding_node method, of class Section.
     */
    @Test
    public void testGetEnding_node() {
        System.out.println("getEnding_node");
        Section instance = new Section();
        String expResult = "final_node";
        instance.setEnding_node("final_node");
        String result = instance.getEnding_node();
        assertEquals(expResult, result);
    }

    /**
     * Test of setEnding_node method, of class Section.
     */
    @Test
    public void testSetEnding_node() {
        System.out.println("setEnding_node");
        String ending_node = "final_node";
        Section instance = new Section();
        instance.setEnding_node(ending_node);

    }

    /**
     * Test of getTypology method, of class Section.
     */
    @Test
    public void testGetTypology() {
        System.out.println("getTypology");
        Section instance = new Section();
        String expResult = "typology";
        instance.setTypology("typology");
        String result = instance.getTypology();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTypology method, of class Section.
     */
    @Test
    public void testSetTypology() {
        System.out.println("setTypology");
        String typology = "typulogy";
        Section instance = new Section();
        instance.setTypology(typology);
    }

    /**
     * Test of getDirection method, of class Section.
     */
    @Test
    public void testGetDirection() {
        System.out.println("getDirection");
        Section instance = new Section();
        String expResult = "direction";
        instance.setDirection("direction");
        String result = instance.getDirection();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setDirection method, of class Section.
     */
    @Test
    public void testSetDirection() {
        System.out.println("setDirection");
        String direction = "direction";
        Section instance = new Section();
        instance.setDirection(direction);
        
    }

    /**
     * Test of getToll method, of class Section.
     */
    @Test
    public void testGetToll() {
        System.out.println("getToll");
        Section instance = new Section();
        double expResult = 0.0;
        instance.getToll();
        double result = instance.getToll();
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of setToll method, of class Section.
     */
    @Test
    public void testSetToll() {
        System.out.println("setToll");
        double toll = 0.0;
        Section instance = new Section();
        instance.setToll(toll);
       
    }

    /**
     * Test of getWind_speed method, of class Section.
     */
    @Test
    public void testGetWind_speed() {
        System.out.println("getWind_speed");
        Section instance = new Section();
        String expResult = "speed";
        instance.setWind_speed("speed");
        String result = instance.getWind_speed();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setWind_speed method, of class Section.
     */
    @Test
    public void testSetWind_speed() {
        System.out.println("setWind_speed");
        String wind_speed = "speed";
        Section instance = new Section();
        instance.setWind_speed(wind_speed);
        
    }

    /**
     * Test of getWind_angle method, of class Section.
     */
    @Test
    public void testGetWind_angle() {
        System.out.println("getWind_angle");
        Section instance = new Section();
        double expResult = 0.0;
        instance.setWind_angle(0.0);
        double result = instance.getWind_angle();
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of setWind_angle method, of class Section.
     */
    @Test
    public void testSetWind_angle() {
        System.out.println("setWind_angle");
        double wind_angle = 0.0;
        Section instance = new Section();
        instance.setWind_angle(wind_angle);
        
    }

    /**
     * Test of getIndex method, of class Section.
     */
    @Test
    public void testGetIndex() {
        System.out.println("getIndex");
        Section instance = new Section();
        int expResult = 0;
        instance.setIndex(0);
        int result = instance.getIndex();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setIndex method, of class Section.
     */
    @Test
    public void testSetIndex() {
        System.out.println("setIndex");
        int index = 0;
        Section instance = new Section();
        instance.setIndex(index);
        
    }

    /**
     * Test of getId_road method, of class Section.
     */
    @Test
    public void testGetId_road() {
        System.out.println("getId_road");
        Section instance = new Section();
        String expResult = "idRoad";
        instance.setId_road("idRoad");
        String result = instance.getId_road();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setId_road method, of class Section.
     */
    @Test
    public void testSetId_road() {
        System.out.println("setId_road");
        String id_road = "idRoad";
        Section instance = new Section();
        instance.setId_road(id_road);
       
    }

    /**
     * Test of getId_section method, of class Section.
     */
    @Test
    public void testGetId_section() {
        System.out.println("getId_section");
        Section instance = new Section();
        instance.getId_section();
        int expResult = 0;
        int result = instance.getId_section();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setId_section method, of class Section.
     */
    @Test
    public void testSetId_section() {
        System.out.println("setId_section");
        int id_section = 0;
        Section instance = new Section();
        instance.setId_section(id_section);
        
    }

    /**
     * Test of getlSegments method, of class Section.
     */
    @Test
    public void testGetlSegments() {
        System.out.println("getlSegments");
        Section instance = new Section();
        ArrayList<Segment> expResult = null;
        ArrayList<Segment> result = instance.getlSegments();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setlSegments method, of class Section.
     */
    @Test
    public void testSetlSegments() {
        System.out.println("setlSegments");
        ArrayList<Segment> lSegments = null;
        Section instance = new Section();
        instance.setlSegments(lSegments);
        
    }

    /**
     * Test of addSegment method, of class Section.
     */
    @Test
    public void testAddSegment() {
        System.out.println("addSegment");
        Segment s = null;
        Section instance = new Section();
        instance.addSegment(s);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRoad_name method, of class Section.
     */
    @Test
    public void testGetRoad_name() {
        System.out.println("getRoad_name");
        Section instance = new Section();
        String expResult = "";
        String result = instance.getRoad_name();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setRoad_name method, of class Section.
     */
    @Test
    public void testSetRoad_name() {
        System.out.println("setRoad_name");
        String road_name = "";
        Section instance = new Section();
        instance.setRoad_name(road_name);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
