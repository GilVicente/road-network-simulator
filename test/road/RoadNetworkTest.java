/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package road;

import graphbase.Graph;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Daniela Ferreira
 */
public class RoadNetworkTest {
    
    public RoadNetworkTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
 

    /**
     * Test of setlNodes method, of class RoadNetwork.
     */
    @Test
    public void testSetlNodes() {
        System.out.println("setlNodes");
        ArrayList<Junction> lNodes = null;
        RoadNetwork instance = new RoadNetwork();
        instance.setlNodes(lNodes);
       
    }

    /**
     * Test of getlSections method, of class RoadNetwork.
     */
    @Test
    public void testGetlSections() {
        System.out.println("getlSections");
        RoadNetwork instance = new RoadNetwork();
        ArrayList<Section> expResult = null;
        ArrayList<Section> result = instance.getlSections();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setlSections method, of class RoadNetwork.
     */
    @Test
    public void testSetlSections() {
        System.out.println("setlSections");
        ArrayList<Section> lSections = null;
        RoadNetwork instance = new RoadNetwork();
        instance.setlSections(lSections);
        
    }

    /**
     * Test of getlRoads method, of class RoadNetwork.
     */
    @Test
    public void testGetlRoads() {
        System.out.println("getlRoads");
        RoadNetwork instance = new RoadNetwork();
        ArrayList<Road> expResult = null;
        ArrayList<Road> result = instance.getlRoads();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setlRoads method, of class RoadNetwork.
     */
    @Test
    public void testSetlRoads() {
        System.out.println("setlRoads");
        ArrayList<Road> lRoads = new ArrayList();
        RoadNetwork instance = new RoadNetwork();
        instance.setlRoads(lRoads);
      
    }

    /**
     * Test of getlSegments method, of class RoadNetwork.
     */
    @Test
    public void testGetlSegments() {
        System.out.println("getlSegments");
        RoadNetwork instance = new RoadNetwork();
        ArrayList<Segment> expResult = null;
        ArrayList<Segment> result = instance.getlSegments();
        assertEquals(expResult, result);
      
    }

    /**
     * Test of setlSegments method, of class RoadNetwork.
     */
    @Test
    public void testSetlSegments() {
        System.out.println("setlSegments");
        ArrayList<Segment> lSegments = new ArrayList();
        RoadNetwork instance = new RoadNetwork();
        instance.setlSegments(lSegments);
        
    }

    /**
     * Test of routeEdges method, of class RoadNetwork.
     */
    @Test
    public void testRouteEdges() {
        System.out.println("routeEdges");
        ArrayList<Junction> lNodes = new ArrayList();
        RoadNetwork instance = new RoadNetwork();
        ArrayList<Section> expResult = new ArrayList();
        ArrayList<Section> result = instance.routeEdges(lNodes);
        assertEquals(expResult, result);
        
    }
    
}
