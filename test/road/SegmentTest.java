/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package road;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Daniela Ferreira
 */
public class SegmentTest {

    public SegmentTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getId_segment method, of class Segment.
     */
    @Test
    public void testGetId_segment() {
        System.out.println("getId_segment");
        Segment instance = new Segment();
        int expResult = 0;
        int result = instance.getId_segment();
        assertEquals(expResult, result);

    }

    /**
     * Test of setId_segment method, of class Segment.
     */
    @Test
    public void testSetId_segment() {
        System.out.println("setId_segment");
        int id_segment = 0;
        Segment instance = new Segment();
        instance.setId_segment(id_segment);

    }

    /**
     * Test of getHeight method, of class Segment.
     */
    @Test
    public void testGetHeight() {
        System.out.println("getHeight");
        Segment instance = new Segment();
        double expResult = 0.0;
        double result = instance.getHeight();
        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of setHeight method, of class Segment.
     */
    @Test
    public void testSetHeight() {
        System.out.println("setHeight");
        double height = 0.0;
        Segment instance = new Segment();
        instance.setHeight(height);

    }

    /**
     * Test of getSlope method, of class Segment.
     */
    @Test
    public void testGetSlope() {
        System.out.println("getSlope");
        Segment instance = new Segment();
        String expResult = "slope";
        instance.setSlope("slope");
        String result = instance.getSlope();
        assertEquals(expResult, result);

    }

    /**
     * Test of setSlope method, of class Segment.
     */
    @Test
    public void testSetSlope() {
        System.out.println("setSlope");
        String slope = "";
        Segment instance = new Segment();
        instance.setSlope(slope);

    }

    /**
     * Test of getLenght method, of class Segment.
     */
    @Test
    public void testGetLenght() {
        System.out.println("getLenght");
        Segment instance = new Segment();
        String expResult = "lenght";
        instance.setLenght("lenght");
        String result = instance.getLenght();
        assertEquals(expResult, result);

    }

    /**
     * Test of setLenght method, of class Segment.
     */
    @Test
    public void testSetLenght() {
        System.out.println("setLenght");
        String lenght = "";
        Segment instance = new Segment();
        instance.setLenght(lenght);

    }



    /**
     * Test of getMax_vehicles method, of class Segment.
     */
    @Test
    public void testGetMax_vehicles() {
        System.out.println("getMax_vehicles");
        Segment instance = new Segment();
        int expResult = 0;
        int result = instance.getMax_vehicles();
        assertEquals(expResult, result);

    }

    /**
     * Test of setMax_vehicles method, of class Segment.
     */
    @Test
    public void testSetMax_vehicles() {
        System.out.println("setMax_vehicles");
        int max_vehicles = 0;
        Segment instance = new Segment();
        instance.setMax_vehicles(max_vehicles);

    }

    /**
     * Test of getMaxVel method, of class Segment.
     */
    @Test
    public void testGetMaxVel() {
        System.out.println("getMaxVel");
        Segment instance = new Segment();
        String expResult = "MaxVel";
        instance.setMaxVel("MaxVel");
        String result = instance.getMaxVel();
        assertEquals(expResult, result);

    }

    /**
     * Test of setMaxVel method, of class Segment.
     */
    @Test
    public void testSetMaxVel() {
        System.out.println("setMaxVel");
        String maxVel = "";
        Segment instance = new Segment();
        instance.setMaxVel(maxVel);

    }

    /**
     * Test of getMinVel method, of class Segment.
     */
    @Test
    public void testGetMinVel() {
        System.out.println("getMinVel");
        Segment instance = new Segment();
        String expResult = "minVel";
        instance.setMinVel("minVel");
        String result = instance.getMinVel();
        assertEquals(expResult, result);

    }

    /**
     * Test of setMinVel method, of class Segment.
     */
    @Test
    public void testSetMinVel() {
        System.out.println("setMinVel");
        String minVel = "";
        Segment instance = new Segment();
        instance.setMinVel(minVel);

    }

}
