/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package road;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Daniela Ferreira
 */
public class RoadTest {
    
    public RoadTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    /**
     * Test of getName method, of class Road.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Road instance = new Road();
        String expResult = "name";
        instance.setName("name");
        String result = instance.getName();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setName method, of class Road.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "name";
        Road instance = new Road();
        instance.setName(name);
       
    }

    /**
     * Test of getlSections method, of class Road.
     */
    @Test
    public void testGetlSections() {
        System.out.println("getlSections");
        Road instance = new Road();
        ArrayList<Section> expResult = new ArrayList();
        ArrayList<Section> result = instance.getlSections();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setlSections method, of class Road.
     */
    @Test
    public void testSetlSections() {
        System.out.println("setlSections");
        ArrayList<Section> lSections = new ArrayList();
        Road instance = new Road();
        instance.setlSections(lSections);

    }
    
}
