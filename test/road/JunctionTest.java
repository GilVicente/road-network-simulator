/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package road;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Daniela Ferreira
 */
public class JunctionTest {
    
    public JunctionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getKey method, of class Junction.
     */
    @Test
    public void testGetKey() {
        System.out.println("getKey");
        Junction instance = new Junction();
        instance.setKey("key");
        String expResult = "key";
        String result = instance.getKey();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setKey method, of class Junction.
     */
    @Test
    public void testSetKey() {
        System.out.println("setKey");
        String key = "key";
        Junction instance = new Junction();
        instance.setKey(key);
       
    }
    
}
