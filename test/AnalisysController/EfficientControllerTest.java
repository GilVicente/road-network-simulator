/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AnalisysController;

import Vehicle.Vehicle;
import java.util.ArrayList;
import java.util.HashMap;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import road.Junction;
import road.RoadNetwork;
import road.RoadNetworkController;
import road.Section;

/**
 *
 * @author Daniela Ferreira
 */
public class EfficientControllerTest {
    
    public EfficientControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of mostEfficientPath method, of class EfficientController.
     */
    @Test
    public void testMostEfficientPath() {
        System.out.println("mostEfficientPath");
        String orig = "n01";
        String dest = "n02";
        EfficientController instance = new EfficientController();
        ArrayList<Junction> expResult = new ArrayList<>();
        ArrayList<Junction> result = instance.mostEfficientPath(orig, dest);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of mostEfficientPathSection method, of class EfficientController.
     */
    @Test
    public void testMostEfficientPathSection() {
        System.out.println("mostEfficientPathSection");
        Vehicle v = new Vehicle();
        ArrayList<Junction> fp = new ArrayList<>();
        EfficientController instance = new EfficientController();
        ArrayList<Section> expResult = new ArrayList<>();
        ArrayList<Section> result = instance.mostEfficientPathSection(v, fp);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getPathConsumption method, of class EfficientController.
     */
    @Test
    public void testGetPathConsumption() {
        System.out.println("getPathConsumption");
        ArrayList<Junction> lNodes = new ArrayList<>();
        Vehicle v = new Vehicle();
        EfficientController instance = new EfficientController();
        double expResult = 0.0;
        double result = instance.getPathConsumption(lNodes, v);
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of routeEdgesByConsumption method, of class EfficientController.
     */
    @Test
    public void testRouteEdgesByConsumption() {
        System.out.println("routeEdgesByConsumption");
        ArrayList<Junction> lNodes = new ArrayList<>();
        Vehicle v = new Vehicle();
        EfficientController instance = new EfficientController();
        ArrayList<Section> expResult = new ArrayList<>();
        ArrayList<Section> result = instance.routeEdgesByConsumption(lNodes, v);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of sectionConsumption method, of class EfficientController.
     */
    @Test
    public void testSectionConsumption() {
        System.out.println("sectionConsumption");
        Section s = new Section();
        Vehicle v = new Vehicle();
        EfficientController instance = new EfficientController();
        double expResult = 0.0;
        double result = instance.sectionConsumption(s, v);
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of getMinConsumptionSection method, of class EfficientController.
     */
    @Test
    public void testGetMinConsumptionSection() {
        System.out.println("getMinConsumptionSection");
        ArrayList<Section> ls = new ArrayList<>();
        Vehicle v = new Vehicle();
        EfficientController instance = new EfficientController();
        Section expResult = new Section();
        Section result = instance.getMinConsumptionSection(ls, v);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getRn method, of class EfficientController.
     */
    @Test
    public void testGetRn() {
        System.out.println("getRn");
        EfficientController instance = new EfficientController();
        RoadNetwork roadNetwork = null;
        instance.setRn(roadNetwork);
        RoadNetwork expResult = new RoadNetwork();
        RoadNetwork result = instance.getRn();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setRn method, of class EfficientController.
     */
    @Test
    public void testSetRn() {
        System.out.println("setRn");
        RoadNetwork rn = new RoadNetwork();
        EfficientController instance = new EfficientController();
        instance.setRn(rn);
        
    }

    /**
     * Test of getV method, of class EfficientController.
     */
    @Test
    public void testGetV() {
        System.out.println("getV");
        EfficientController instance = new EfficientController();
        Vehicle vehicle = null;
        instance.setV(vehicle);
        Vehicle expResult = new Vehicle();
        Vehicle result = instance.getV();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setV method, of class EfficientController.
     */
    @Test
    public void testSetV() {
        System.out.println("setV");
        Vehicle v = new Vehicle();
        EfficientController instance = new EfficientController();
        instance.setV(v);
        
    }
    
}
