/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vehicle;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Daniela Ferreira
 */
public class EnergyTest {
    
    public EnergyTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    

    /**
     * Test of getMin_rpm method, of class Energy.
     */
    @Test
    public void testGetMin_rpm() {
        System.out.println("getMin_rpm");
        Energy instance = new Energy();
        int expResult = 0;
        instance.setMin_rpm(expResult);
        int result = instance.getMin_rpm();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setMin_rpm method, of class Energy.
     */
    @Test
    public void testSetMin_rpm() {
        System.out.println("setMin_rpm");
        int min_rpm = 0;
        Energy instance = new Energy();
        instance.setMin_rpm(min_rpm);
        
    }

    /**
     * Test of getMax_rpm method, of class Energy.
     */
    @Test
    public void testGetMax_rpm() {
        System.out.println("getMax_rpm");
        Energy instance = new Energy();
        int expResult = 0;
        instance.setMax_rpm(expResult);
        int result = instance.getMax_rpm();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setMax_rpm method, of class Energy.
     */
    @Test
    public void testSetMax_rpm() {
        System.out.println("setMax_rpm");
        int max_rpm = 0;
        Energy instance = new Energy();
        instance.setMax_rpm(max_rpm);
        
    }

    /**
     * Test of getFinal_drive_ratio method, of class Energy.
     */
    @Test
    public void testGetFinal_drive_ratio() {
        System.out.println("getFinal_drive_ratio");
        Energy instance = new Energy();
        double expResult = 0.0;
        instance.setFinal_drive_ratio(expResult);
        double result = instance.getFinal_drive_ratio();
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of setFinal_drive_ratio method, of class Energy.
     */
    @Test
    public void testSetFinal_drive_ratio() {
        System.out.println("setFinal_drive_ratio");
        double final_drive_ratio = 0.0;
        Energy instance = new Energy();
        instance.setFinal_drive_ratio(final_drive_ratio);
        
    }

    /**
     * Test of getlGear method, of class Energy.
     */
    @Test
    public void testGetlGear() {
        System.out.println("getlGear");
        Energy instance = new Energy();
        ArrayList<Gear> expResult = new ArrayList();
        ArrayList<Gear> result = instance.getlGear();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setlGear method, of class Energy.
     */
    @Test
    public void testSetlGear() {
        System.out.println("setlGear");
        ArrayList<Gear> lGear = new ArrayList();
        Energy instance = new Energy();
        instance.setlGear(lGear);
       
    }

    /**
     * Test of getlThrottle method, of class Energy.
     */
    @Test
    public void testGetlThrottle() {
        System.out.println("getlThrottle");
        Energy instance = new Energy();
        ArrayList<Throttle> expResult =new ArrayList<>();
        ArrayList<Throttle> result = instance.getlThrottle();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setlThrottle method, of class Energy.
     */
    @Test
    public void testSetlThrottle() {
        System.out.println("setlThrottle");
        ArrayList<Throttle> lThrottle = new ArrayList<> ();
        Energy instance = new Energy();
        instance.setlThrottle(lThrottle);
       
    }
    
}
