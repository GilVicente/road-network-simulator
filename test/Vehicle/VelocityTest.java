/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vehicle;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Daniela Ferreira
 */
public class VelocityTest {
    
    public VelocityTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getSegment_type method, of class Velocity.
     */
    @Test
    public void testGetSegment_type() {
        System.out.println("getSegment_type");
        Velocity instance = new Velocity();
        String expResult = "type";
        instance.setSegment_type("type");
        String result = instance.getSegment_type();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setSegment_type method, of class Velocity.
     */
    @Test
    public void testSetSegment_type() {
        System.out.println("setSegment_type");
        String segment_type = "type";
        Velocity instance = new Velocity();
        instance.setSegment_type(segment_type);
       
    }

    /**
     * Test of getLimit method, of class Velocity.
     */
    @Test
    public void testGetLimit() {
        System.out.println("getLimit");
        Velocity instance = new Velocity();
        double expResult = 0.0;
        instance.setLimit(expResult);
        double result = instance.getLimit();
        assertEquals(expResult, result, 0.0);
    
    }

    /**
     * Test of setLimit method, of class Velocity.
     */
    @Test
    public void testSetLimit() {
        System.out.println("setLimit");
        double limit = 0.0;
        Velocity instance = new Velocity();
        instance.setLimit(limit);
        
    }
    
}
