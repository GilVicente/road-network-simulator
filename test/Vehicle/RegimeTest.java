/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vehicle;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Daniela Ferreira
 */
public class RegimeTest {

    public RegimeTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getTorque method, of class Regime.
     */
    @Test
    public void testGetTorque() {
        System.out.println("getTorque");
        Regime instance = new Regime();
        instance.setTorque(1);
        int expResult = 1;
        int result = instance.getTorque();
        assertEquals(expResult, result);

    }

    /**
     * Test of setTorque method, of class Regime.
     */
    @Test
    public void testSetTorque() {
        System.out.println("setTorque");
        int torque = 1;
        Regime instance = new Regime();
        instance.setTorque(torque);

    }

    /**
     * Test of getRpm_low method, of class Regime.
     */
    @Test
    public void testGetRpm_low() {
        System.out.println("getRpm_low");
        Regime instance = new Regime();
        instance.setRpm_low(2);
        int expResult = 2;
        int result = instance.getRpm_low();
        assertEquals(expResult, result);

    }

    /**
     * Test of setRpm_low method, of class Regime.
     */
    @Test
    public void testSetRpm_low() {
        System.out.println("setRpm_low");
        int rpm_low = 2;
        Regime instance = new Regime();
        instance.setRpm_low(rpm_low);

    }

    /**
     * Test of getRpm_high method, of class Regime.
     */
    @Test
    public void testGetRpm_high() {
        System.out.println("getRpm_high");
        Regime instance = new Regime();
        instance.setRpm_high(3);
        int expResult = 3;
        int result = instance.getRpm_high();
        assertEquals(expResult, result);

    }

    /**
     * Test of setRpm_high method, of class Regime.
     */
    @Test
    public void testSetRpm_high() {
        System.out.println("setRpm_high");
        int rpm_high = 3;
        Regime instance = new Regime();
        instance.setRpm_high(rpm_high);

    }

    /**
     * Test of getSFC method, of class Regime.
     */
    @Test
    public void testGetSFC() {
        System.out.println("getSFC");
        Regime instance = new Regime();
        instance.setSFC(2.1);
        double expResult = 2.1;
        double result = instance.getSFC();
        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of setSFC method, of class Regime.
     */
    @Test
    public void testSetSFC() {
        System.out.println("setSFC");
        double SFC = 2.1;
        Regime instance = new Regime();
        instance.setSFC(SFC);
    }

}
