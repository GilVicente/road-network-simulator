/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vehicle;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Daniela Ferreira
 */
public class VehicleTest {
    
    public VehicleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getId method, of class Vehicle.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        Vehicle instance = new Vehicle();
        int expResult = 0;
        instance.setId(expResult);
        int result = instance.getId();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setId method, of class Vehicle.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        int id = 0;
        Vehicle instance = new Vehicle();
        instance.setId(id);
        
    }

    /**
     * Test of getName method, of class Vehicle.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Vehicle instance = new Vehicle();
        String expResult = "name";
        instance.setName(expResult);
        String result = instance.getName();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setName method, of class Vehicle.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "name";
        Vehicle instance = new Vehicle();
        instance.setName(name);
        
    }

    /**
     * Test of getType method, of class Vehicle.
     */
    @Test
    public void testGetType() {
        System.out.println("getType");
        Vehicle instance = new Vehicle();
        String expResult = "type";
        instance.setType(expResult);
        String result = instance.getType();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setType method, of class Vehicle.
     */
    @Test
    public void testSetType() {
        System.out.println("setType");
        String type = "type";
        Vehicle instance = new Vehicle();
        instance.setType(type);
        
    }

    /**
     * Test of getFuel method, of class Vehicle.
     */
    @Test
    public void testGetFuel() {
        System.out.println("getFuel");
        Vehicle instance = new Vehicle();
        String expResult = "fuel";
        instance.setFuel(expResult);
        String result = instance.getFuel();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFuel method, of class Vehicle.
     */
    @Test
    public void testSetFuel() {
        System.out.println("setFuel");
        String fuel = "fuel";
        Vehicle instance = new Vehicle();
        instance.setFuel(fuel);
    }

    /**
     * Test of getMotor method, of class Vehicle.
     */
    @Test
    public void testGetMotor() {
        System.out.println("getMotor");
        Vehicle instance = new Vehicle();
        String expResult = "motor";
        instance.setMotor(expResult);
        String result = instance.getMotor();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setMotor method, of class Vehicle.
     */
    @Test
    public void testSetMotor() {
        System.out.println("setMotor");
        String motor = "motor";
        Vehicle instance = new Vehicle();
        instance.setMotor(motor);
        }

    /**
     * Test of getMass method, of class Vehicle.
     */
    @Test
    public void testGetMass() {
        System.out.println("getMass");
        Vehicle instance = new Vehicle();
        String expResult = "mass";
        instance.setMass(expResult);
        String result = instance.getMass();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setMass method, of class Vehicle.
     */
    @Test
    public void testSetMass() {
        System.out.println("setMass");
        String mass = "mass";
        Vehicle instance = new Vehicle();
        instance.setMass(mass);
    }

    /**
     * Test of getDrag_coef method, of class Vehicle.
     */
    @Test
    public void testGetDrag_coef() {
        System.out.println("getDrag_coef");
        Vehicle instance = new Vehicle();
        double expResult = 0.0;
        instance.setDrag_coef(expResult);
        double result = instance.getDrag_coef();
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of setDrag_coef method, of class Vehicle.
     */
    @Test
    public void testSetDrag_coef() {
        System.out.println("setDrag_coef");
        double drag_coef = 0.0;
        Vehicle instance = new Vehicle();
        instance.setDrag_coef(drag_coef);
        
    }

    /**
     * Test of getLoad method, of class Vehicle.
     */
    @Test
    public void testGetLoad() {
        System.out.println("getLoad");
        Vehicle instance = new Vehicle();
        String expResult = "load";
        instance.setLoad(expResult);
        String result = instance.getLoad();
        assertEquals(expResult, result);
      
    }

    /**
     * Test of setLoad method, of class Vehicle.
     */
    @Test
    public void testSetLoad() {
        System.out.println("setLoad");
        String load = "load";
        Vehicle instance = new Vehicle();
        instance.setLoad(load);
       
    }

    /**
     * Test of getRrc method, of class Vehicle.
     */
    @Test
    public void testGetRrc() {
        System.out.println("getRrc");
        Vehicle instance = new Vehicle();
        double expResult = 0.0;
        instance.setRrc(expResult);
        double result = instance.getRrc();
        assertEquals(expResult, result, 0.0);
      
    }

    /**
     * Test of setRrc method, of class Vehicle.
     */
    @Test
    public void testSetRrc() {
        System.out.println("setRrc");
        double rrc = 0.0;
        Vehicle instance = new Vehicle();
        instance.setRrc(rrc);
        
    }

    /**
     * Test of getWheel_size method, of class Vehicle.
     */
    @Test
    public void testGetWheel_size() {
        System.out.println("getWheel_size");
        Vehicle instance = new Vehicle();
        double expResult = 0.0;
        instance.setWheel_size(expResult);
        double result = instance.getWheel_size();
        assertEquals(expResult, result, 0.0);
       
    }

    /**
     * Test of setWheel_size method, of class Vehicle.
     */
    @Test
    public void testSetWheel_size() {
        System.out.println("setWheel_size");
        double wheel_size = 0.0;
        Vehicle instance = new Vehicle();
        instance.setWheel_size(wheel_size);
        
    }
   

    /**
     * Test of getDescription method, of class Vehicle.
     */
    @Test
    public void testGetDescription() {
        System.out.println("getDescription");
        Vehicle instance = new Vehicle();
        String expResult = "desc";
        instance.setDescription(expResult);
        String result = instance.getDescription();
        assertEquals(expResult, result);
      
    }

    /**
     * Test of setDescription method, of class Vehicle.
     */
    @Test
    public void testSetDescription() {
        System.out.println("setDescription");
        String description = "desc";
        Vehicle instance = new Vehicle();
        instance.setDescription(description);
        
    }


    /**
     * Test of addVelocity method, of class Vehicle.
     */
    @Test
    public void testAddVelocity() {
        System.out.println("addVelocity");
        Velocity g = new Velocity();
        Vehicle instance = new Vehicle();
        instance.addVelocity(g);
        
    }

    /**
     * Test of getlVelocity method, of class Vehicle.
     */
    @Test
    public void testGetlVelocity() {
        System.out.println("getlVelocity");
        Vehicle instance = new Vehicle();
        ArrayList<Velocity> expResult = new ArrayList();
        ArrayList<Velocity> result = instance.getlVelocity();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setlGear method, of class Vehicle.
     */
    @Test
    public void testSetlGear() {
        System.out.println("setlGear");
        ArrayList<Velocity> lVelocity = new ArrayList();
        Vehicle instance = new Vehicle();
        instance.setlGear(lVelocity);
        
    }

    /**
     * Test of getVelocityByTypology method, of class Vehicle.
     */
    @Test
    public void testGetVelocityByTypology() {
        System.out.println("getVelocityByTypology");
        String t = "typology";
        Vehicle instance = new Vehicle();
        double expResult = 0.0;
        double result = instance.getVelocityByTypology(t);
        assertEquals(expResult, result, 0.0);
        
    }
    
}
