/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Project;
import java.util.ArrayList;

/**
 *
 * @author Daniela Ferreira
 */
public class ChangeProjectController {
    private Project m_project;
    private ArrayList<Project> lProjects;
    
     public Project getProject(String name) {
        for (Project p : getlProjects()) {
            if (p.getName().equalsIgnoreCase(name)) {
                return p;
            }
        }
        return null;
    }
     public ArrayList<Project> getlProjects() {
        return lProjects;
    }
    
    public Project setDados(String name, String description){
        this.m_project.setName(name);
        this.m_project.setDescription(description);
        
        return m_project;
    }
    
}
