/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Project;
import java.util.ArrayList;

/**
 *
 * @author Daniela Ferreira
 */
public class SelectProjectController {
    
    private ArrayList<Project> m_Lproject;
    private Project activeProject;
    private boolean isActive = false;
    
    
    public SelectProjectController(System system){
        m_Lproject = new ArrayList();
    }
    
        
    public Project getProject(String name){
        for(Project proj : getlProjects()){
            if(proj.getName().equalsIgnoreCase(name)){
                return proj;
            }
        }
        return null;
    }
    
    public void setActiveProj(Project activeProj) {
        this.activeProject = activeProj;
    }
    
     public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }
     
     public boolean isIsActive() {
        return isActive;
    }
    
      public ArrayList<Project> getlProjects() {
        return m_Lproject;
    }
      
      public void setProject (ArrayList<Project> m_Lproject ){
          this.m_Lproject=m_Lproject;
      }
}
