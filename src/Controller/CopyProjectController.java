/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Project;
import java.util.ArrayList;

/**
 *
 * @author Daniela Ferreira
 */
public class CopyProjectController {
    
    private Project activeProject;
    private boolean isActive = false;
    private ArrayList <Project> m_Lproject;
    private Project m_project;
    
    
    public CopyProjectController(){
        m_Lproject= new ArrayList();
    }
    
    
    public Project getProject(String name){
        for(Project proj : getlProjects()){
            if( proj.getName().equalsIgnoreCase(name)){
                return proj;
            }
        }
        return null;
    }
    
    public ArrayList<Project> getlProjects(){
        return m_Lproject;
    }
    
    public void setActiveProject (Project activeProj){
        this.activeProject=activeProj;
        
    }
  public boolean isActive(){
      return isActive;
  }
    
    public void setIsActive ( boolean isActive){
        this.isActive= isActive;
    }
}
