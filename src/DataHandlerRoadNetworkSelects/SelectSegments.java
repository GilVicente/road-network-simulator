/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataHandlerRoadNetworkSelects;

import Model.DataHandler;
import Model.Project;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import road.Junction;
import road.Section;
import road.Segment;

/**
 *
 * @author Yahkiller
 */
public class SelectSegments {

    public void getAllSegments(DataHandler dh, Project ActProject, Section sec) throws SQLException {

        Statement stmt = dh.getConnection().createStatement();

        String query = "SELECT * FROM Segment where SECTIONIDSECTION ="
                 + sec.getId_section();

        try {
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                int IDSEGMENT = rs.getInt(1);
                int SEGMENTINDEX = rs.getInt(2);
                int INITIALHEIGHT = rs.getInt(3);
                String SLOPE = rs.getString(4);
                String LENGTH = rs.getString(5);
                String MAXIMUMVELOCITY = rs.getString(6);
                String MINIMUMVELOCITY = rs.getString(7);
                int SECTIONIDSECTION = rs.getInt(8);
                int MAXIMUMNRVEHICLES = rs.getInt(9);

                Segment seg = new Segment(SECTIONIDSECTION, SEGMENTINDEX, INITIALHEIGHT, SLOPE, LENGTH, MAXIMUMNRVEHICLES, MAXIMUMVELOCITY, MINIMUMVELOCITY);
                ActProject.getRn().getSection(SECTIONIDSECTION).addSegment(seg);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

}
