/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataHandlerRoadNetworkSelects;

import Model.DataHandler;
import Model.Project;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import road.Section;

/**
 *
 * @author Paulo Silva
 */
public class SelectSections {

    public void getAllSections(DataHandler dh, Project ActProject) throws SQLException {

        Statement stmt = dh.getConnection().createStatement();

        String query = "SELECT * FROM Section where ProjectIDProject ="
                + ActProject.getBdID();

        try {
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                int IDSECTION = rs.getInt(1);
                String BEGINNINGNODE = rs.getString(2);
                String ENDINGNODE = rs.getString(3);
                String DIRECTION = rs.getString(4);
                double TOLL = rs.getFloat(5);
                double ANGLEWIND = rs.getFloat(6);
                String SPEEDWIND = rs.getString(7);
                int PROJECTIDPROJECT = rs.getInt(8);
                String TYPOLOGY = rs.getString(9);
                String ROADNAME = rs.getString(10);
                
                Section s = new Section(ROADNAME, IDSECTION, BEGINNINGNODE, ENDINGNODE, TYPOLOGY, DIRECTION, TOLL, SPEEDWIND,ANGLEWIND);
                ActProject.getRn().addSection(s);
                
                SelectSegments ss = new SelectSegments();
                ss.getAllSegments(dh, ActProject, s);
                
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}

