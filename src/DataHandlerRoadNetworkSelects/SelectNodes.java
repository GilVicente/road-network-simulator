/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataHandlerRoadNetworkSelects;

import Model.DataHandler;
import Model.Project;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import road.Junction;

/**
 *
 * @author Yahkiller
 */
public class SelectNodes {
    
    public void getAllNodes(DataHandler dh, Project ActProject) throws SQLException {
        
        Statement stmt = dh.getConnection().createStatement();
        
        String query = "SELECT * FROM Node";
        try {
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                int IDNODE = rs.getInt(1);
                String desc = rs.getString(2);
                int IDPROJECT = rs.getInt(3);
                
                Junction n = new Junction(desc);
                ActProject.getRn().addNode(n);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public ArrayList<Junction> getArrayNodes(DataHandler dh, Project ActProject) throws SQLException {
        
        Statement stmt = dh.getConnection().createStatement();
        ArrayList<Junction> r = new ArrayList();
        String query = "SELECT * FROM Node";
        try {
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                int IDNODE = rs.getInt(1);
                String desc = rs.getString(2);
                int IDPROJECT = rs.getInt(3);
                
                Junction n = new Junction(desc);
                r.add(n);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return r;
    }
}
