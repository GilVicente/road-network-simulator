package Export;

import AnalisysUI.BestPathResultsUI;
import AnalisysUI.BestPathUI;
import AnalisysUI.Path;
import Model.Project;
import Utils.MyListModel;
import Vehicle.Vehicle;
import com.hp.gagawa.java.Document;
import com.hp.gagawa.java.DocumentType;
import com.hp.gagawa.java.elements.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;
import java.util.Locale;
import road.RoadNetwork;
import road.RoadNetworkController;

/**
 *
 * @author Luís Rocha
 */
public class HTMLGenerator {

  
    private File file;
    private RoadNetworkController r;
    private Path path;
    private Vehicle v;

    private RoadNetwork RN;
    Formatter fout;

    public HTMLGenerator() {

    }

    public HTMLGenerator(BestPathUI bUI, Path path, RoadNetworkController rc, Vehicle v, String name) throws FileNotFoundException {
        setR(r);
        this.path = path;
        this.v = v;
        file = new File(name);
        fout = new Formatter(file);
    }

    public boolean gerarHTML() throws FileNotFoundException {
        criaHeader();
        criaResultados();
        fout.close();
        return true;
    }

    public RoadNetworkController getR() {
        return r;
    }

    public void setR(RoadNetworkController r) {
        this.r = r;
    }


    private void criaHeader() {

        fout.
                format("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
        fout.format("<html>");
        fout.format("<head>");
        fout.
                format("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
        fout.format("<title>" + "Network	static	analysis" + "</title></head>");
        fout.
                format("<body style=\"margin:2;padding:0;background-color:#E8E8E8;height;100%%\">");

        fout.format("<center>");
        fout.format("<h1><b>" + "Network static	analysis" + "</b></h1>");
        fout.format("</center>");
    }

    private void criaResultados() {
        fout.format("<p>");
        fout.format("<p>");
        fout.format("<p>");
        fout.format("<p><b>" + "Algorithm" + ":</b> %s</p>", path.getAlgorithm());

        fout.format("<p><b>" + "Best Path" + ":</b> %s </p>", path.getStrPath());

       // String[] vec = MyListModel.listaString(Utils.Utils.mapToString(r.otherPaths(path.getAlgorithm(), path.getOrig(), path.getDest(), v)));

//        fout.format("<p><b>" + "Other Paths\":</b>");
//
//        for (int i = 0; i < vec.length; i++) {
//            fout.format("</b>%s</p>", vec[i]);
//        }

        fout.format("<p><b>" + "Toll Costs" + ":</b> %.2f €</p>", path.getCost());

        fout.format("<p><b>" + "Energy Consumption" + ":</b> %.2f L</p>", path.getEnergy());
        fout.
                format("<p><b>" + "Path Length" + ":</b> %.2f Km</p>", path.getLenght());

        fout.
                format("<p><b>" + "Travelling Time" + ":</b> %.2f Min.</p>", path.getTime());

    }

}
