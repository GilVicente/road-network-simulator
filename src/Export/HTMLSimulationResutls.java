/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Export;

import AnalisysUI.BestPathUI;
import AnalisysUI.Path;
import Model.Project;
import Model.Simulation;
import Model.SimulationRun;
import SimulationUI.RunSimulatorController;
import Vehicle.Time;
import Vehicle.Vehicle;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Formatter;
import road.RoadNetwork;
import road.RoadNetworkController;

/**
 *
 * @author Luís Rocha
 */
public class HTMLSimulationResutls {

    private File file;
    private RunSimulatorController r;
    private String alg;
    Formatter fout;

    public HTMLSimulationResutls() {

    }

    public HTMLSimulationResutls(RunSimulatorController r, String name,
            String alg) throws FileNotFoundException {
        this.r = r;
        this.alg = alg;
        file = new File(name);
        fout = new Formatter(file);
    }

    public boolean gerarHTML() throws FileNotFoundException {
        criaHeader();
        criaResultados();
        fout.close();
        return true;
    }

    private void criaHeader() {

        fout.
                format("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
        fout.format("<html>");
        fout.format("<head>");
        fout.
                format("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
        fout.format("<title>" + "Simulation Results" + "</title></head>");
        fout.
                format("<body style=\"margin:2;padding:0;background-color:#E8E8E8;height;100%%\">");

        fout.format("<center>");
        fout.format("<h1><b>" + "Simulation Results" + "</b></h1>");
        fout.format("</center>");
    }

    private void criaResultados() {
        fout.format("<p>");
        fout.format("<p>");
        fout.format("<p>");
        fout.format("<p><b>" + "Algorithm" + ":</b> %s</p>", alg);

        ArrayList<Vehicle> v = r.getFinalVehicles();
        for (Vehicle v1 : v) {
            ArrayList<Time> tIn = v1.getInTime();
            ArrayList<Time> tOut = v1.getOutTime();
            fout.format("<p><b>" + "Car Name:" + ":</b> %s </p>", v1.getName());
            for (Time tI : tIn) {
                for (Time tO : tOut) {
                    fout.format("<p><b>" + "In Time" + "</b>");
                    fout.format("<p><b>" + "Section :" + ":</b> %i </p>", tI.getS().getId_section());
                    fout.format("<p><b>" + "Segment" + ":</b> %i L</p>", tI.getS().getId_segment());
                    fout.format("<p><b>" + "Time" + ":</b> %i Km</p>", tI.getTime());
                    fout.format("<p><b>" + "Out Time" + "</b>");
                    fout.format("<p><b>" + "Section :" + ":</b> %i </p>", tO.getS().getId_section());
                    fout.format("<p><b>" + "Segment" + ":</b> %i L</p>", tO.getS().getId_segment());
                    fout.format("<p><b>" + "Time" + ":</b> %i Km</p>", tO.getTime());

                }
            }

        }

    }

}
