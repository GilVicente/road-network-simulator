package Export;

import AnalisysUI.BestPathUI;
import AnalisysUI.Path;
import Vehicle.Vehicle;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Formatter;
import road.RoadNetwork;
import road.RoadNetworkController;

/**
 *
 * @author Luís Rocha
 */
public class HMTLMultipleGenarator {

    private File file;
    private RoadNetworkController r;
    private ArrayList<Path> lPath;
    private ArrayList<Vehicle> v;

    private RoadNetwork RN;
    Formatter fout;

    public HMTLMultipleGenarator() {

    }

    public HMTLMultipleGenarator(BestPathUI bUI, ArrayList<Path> lPath, RoadNetworkController rc, ArrayList<Vehicle> v, String name) throws FileNotFoundException {
        setR(r);
        this.lPath = lPath;
        this.v = v;
        file = new File(name);
        fout = new Formatter(file);
    }

    public boolean gerarHTML() throws FileNotFoundException {
        criaHeader();

        criaResultados();

        fout.close();
        return true;
    }

    public RoadNetworkController getR() {
        return r;
    }

    public void setR(RoadNetworkController r) {
        this.r = r;
    }

    private void criaHeader() {

        fout.
                format("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
        fout.format("<html>");
        fout.format("<head>");
        fout.
                format("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
        fout.format("<title>" + "Network	static	analysis" + "</title></head>");
        fout.
                format("<body style=\"margin:2;padding:0;background-color:#E8E8E8;height;100%%\">");

        fout.format("<center>");
        fout.format("<h1><b>" + "Network static	analysis" + "</b></h1>");
        fout.format("</center>");
    }

    private void criaResultados() {
        fout.format("<p>");
        fout.format("<p>");
        fout.format("<p>");
        for (Path v2 : lPath) {
            fout.format("<br>");
            fout.format("<p><b>" + "Vehicle Name :</b> %s</p>", v2.getV().getName());

            fout.format("<p><b>" + "Algorithm" + ":</b> %s</p>", v2.getAlgorithm());

            fout.format("<p><b>" + "Best Path" + ":</b> %s </p>", v2.getStrPath());
            fout.format("<p><b>" + "Toll Costs" + ":</b> %.2f €</p>", v2.getCost());

            fout.format("<p><b>" + "Energy Consumption" + ":</b> %.2f L</p>", v2.getEnergy());
            fout.
                    format("<p><b>" + "Path Length" + ":</b> %.2f Km</p>", v2.getLenght());
            fout.
                    format("<p><b>" + "Travelling Time" + ":</b> %.2f Min.</p>", v2.getTime());
            fout.format("<br>");
        }
    }

    // String[] vec = MyListModel.listaString(Utils.Utils.mapToString(r.otherPaths(path.getAlgorithm(), path.getOrig(), path.getDest(), v)));
//        fout.format("<p><b>" + "Other Paths\":</b>");
//
//        for (int i = 0; i < vec.length; i++) {
//            fout.format("</b>%s</p>", vec[i]);
//        }
}
