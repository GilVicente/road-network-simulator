/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AnalisysController;

import Graphs.FastestGraph;
import Utils.Utils;
import Vehicle.Vehicle;
import java.util.ArrayList;
import java.util.HashMap;
import road.Junction;
import road.RoadNetwork;
import road.RoadNetworkController;
import road.Section;
import road.Segment;

/**
 *
 * @author Yahkiller
 */
public class FastestController {

    private RoadNetworkController rc;
    private RoadNetwork rn;

    public FastestController(RoadNetworkController rc) {
        this.rc = rc;
        this.rn = rc.getRn();
    }

    public ArrayList<Junction> fastestPath(String orig, String dest, Vehicle v) {
        FastestGraph fg = new FastestGraph(rn, v, true);
        ArrayList<Junction> fp = fg.getShortestPath(rn.getNode(orig), rn.getNode(dest));
        return fp;
    }

    public ArrayList<Section> fastestPathSections(Vehicle v, ArrayList<Junction> fp) {
        FastestGraph fg = new FastestGraph(rn, v, true);
        ArrayList<Section> lSections = routeEdgesByTime(fp, v);
        return lSections;
    }

    public HashMap<ArrayList<Junction>, Double> getAllFastestPaths(String orig, String dest, Vehicle v) {
        FastestGraph fg = new FastestGraph(rn, v, true);
        HashMap<ArrayList<Junction>, Double> map = fg.getAllPaths(rn.getNode(orig), rn.getNode(dest));
        return map;
    }

    public Double getPathFastTime(ArrayList<Junction> lNodes, Vehicle v) {
        FastestGraph fg = new FastestGraph(rn, v, true);
        double time = 0.0;
        ArrayList<Section> lSections = routeEdgesByTime(lNodes, v);
        for (Section s : lSections) {
            time += getSectionTime(s, v);
        }
        return time;
    }

    //Rout Edges
    public ArrayList<Section> routeEdgesByTime(ArrayList<Junction> lNodes, Vehicle v) {
        ArrayList<Section> lSecs = new ArrayList<>();
        for (int i = 0; i < lNodes.size() - 1; i++) {

            ArrayList<Section> ls = rn.getSections(lNodes.get(i).getKey(), lNodes.get(i + 1).getKey());
            Section s = getMinTimeSection(ls, v);
            lSecs.add(s);
        }
        return lSecs;
    }

    public double getSectionTime(Section s, Vehicle v) {
        FastestGraph fg = new FastestGraph(rn, v, true);
        return fg.getSectionTime(s, v);
    }

    public double getSegmentTime(Section s, Segment seg, Vehicle v) {
        FastestGraph fg = new FastestGraph(rn, v, true);
        return fg.getSegmentTime(s,seg, v);
    }

    public Section getMinTimeSection(ArrayList<Section> ls, Vehicle v) {
        HashMap<Section, Double> map = new HashMap();
        for (Section s : ls) {
            map.put(s, getSectionTime(s, v));
        }
        return Utils.getMinSection(map);
    }
}
