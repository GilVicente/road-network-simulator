/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AnalisysController;

import Graphs.TollGraph;
import java.util.ArrayList;
import java.util.HashMap;
import road.Junction;
import road.RoadNetwork;
import road.RoadNetworkController;

/**
 *
 * @author Yahkiller
 */
public class TollCostController {

    private RoadNetworkController rc;
    private RoadNetwork rn;

    public TollCostController(RoadNetworkController rc) {
        this.rc = rc;
        this.rn = rc.getRn();
    }

    public ArrayList<Junction> cheapestTollPath(String orig, String dest) {
        TollGraph tg = new TollGraph(rn, true);
        ArrayList<Junction> cp = tg.getShortestPathTollCost(rn.getNode(orig), rn.getNode(dest));
        return cp;
    }

    public Double getPathTollCost(ArrayList<Junction> lNodes) {
        TollGraph tg = new TollGraph(rn, true);
        double cp = tg.getPathTollCost(lNodes);
        return cp;
    }

    public HashMap<ArrayList<Junction>, Double> getAllPathsCost(String orig, String dest) {
        TollGraph tg = new TollGraph(rn, true);
        HashMap<ArrayList<Junction>, Double> map = tg.getAllPaths(rn.getNode(orig), rn.getNode(dest));
        return map;
    }

}
