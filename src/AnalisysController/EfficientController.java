/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AnalisysController;

import Graphs.ConsumptionGraph;
import Graphs.Data1;
import Graphs.FastestGraph;
import Physics.CarPhysics;
import Utils.Utils;
import Vehicle.Vehicle;
import java.util.ArrayList;
import java.util.HashMap;
import road.Junction;
import road.RoadNetwork;
import road.RoadNetworkController;
import road.Section;
import road.Segment;

/**
 *
 * @author Yahkiller
 */
public class EfficientController {

    private RoadNetworkController rc;
    private RoadNetwork rn;
    private Vehicle v;

    public EfficientController(){
        
    }
    public EfficientController(RoadNetworkController rc, Vehicle v) {
        this.rc = rc;
        this.rn = rc.getRn();
        this.v = v;
    }

    public ArrayList<Junction> mostEfficientPath(String orig, String dest) {
        //printPath
        ConsumptionGraph eg = new ConsumptionGraph(this, true);
        ArrayList<Junction> ep = eg.getLessConsumptionPath(getRn().getNode(orig), getRn().getNode(dest));
        return ep;
    }

    public ArrayList<Section> mostEfficientPathSection(Vehicle v, ArrayList<Junction> fp) {
        FastestGraph fg = new FastestGraph(rn, v, true);
        ArrayList<Section> lSections = routeEdgesByConsumption(fp, v);
        return lSections;
    }

    public HashMap<ArrayList<Junction>, Double> getAllEfficientPaths(String orig, String dest) {
        ConsumptionGraph eg = new ConsumptionGraph(this, true);
        HashMap<ArrayList<Junction>, Double> map = eg.getAllPaths(getRn().getNode(orig), getRn().getNode(dest));
        return map;
    }

    public double getPathConsumption(ArrayList<Junction> lNodes, Vehicle v) {
        double en = 0.0;
        ConsumptionGraph eg = new ConsumptionGraph(this, true);
        return eg.getPathConsumption(lNodes);
    }

    public ArrayList<Section> routeEdgesByConsumption(ArrayList<Junction> lNodes, Vehicle v) {
        ArrayList<Section> lSecs = new ArrayList<>();
        for (int i = 0; i < lNodes.size() - 1; i++) {

            ArrayList<Section> ls = getRn().getSections(lNodes.get(i).getKey(), lNodes.get(i + 1).getKey());
            Section s = getMinConsumptionSection(ls, v);
            lSecs.add(s);
        }
        return lSecs;
    }

    public double sectionConsumption(Section s, Vehicle v) {
        double en = 0.0;
        int throt = 0;
        ConsumptionGraph eg = new ConsumptionGraph(this, true);
        Data1 d = eg.getSectionConsumption(s, v, v.getEnergy().GetMaxGear(), throt);
        return d.getEnergy();
    }

    public Section getMinConsumptionSection(ArrayList<Section> ls, Vehicle v) {
        HashMap<Section, Double> map = new HashMap();
        for (Section s : ls) {
            map.put(s, sectionConsumption(s, v));
        }
        return Utils.getMinSection(map);
    }

    /**
     * @return the rc
     */
    public RoadNetworkController getRc() {
        return rc;
    }

    /**
     * @param rc the rc to set
     */
    public void setRc(RoadNetworkController rc) {
        this.rc = rc;
    }

    /**
     * @return the rn
     */
    public RoadNetwork getRn() {
        return rn;
    }

    /**
     * @param rn the rn to set
     */
    public void setRn(RoadNetwork rn) {
        this.rn = rn;
    }

    /**
     * @return the v
     */
    public Vehicle getV() {
        return v;
    }

    /**
     * @param v the v to set
     */
    public void setV(Vehicle v) {
        this.v = v;
    }

}
