/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AnalisysController;

import Graphs.LengthGraph;
import Utils.Utils;
import java.util.ArrayList;
import java.util.HashMap;
import road.Junction;
import road.RoadNetwork;
import road.RoadNetworkController;
import road.Section;

/**
 *
 * @author Yahkiller
 */
public class ShortestController {

    private RoadNetworkController rc;
    private RoadNetwork rn;

    public ShortestController(RoadNetworkController rc) {
        this.rc = rc;
        this.rn = rc.getRn();
    }

    public ArrayList<Junction> shortestPath(String orig, String dest) {
        LengthGraph lg = new LengthGraph(rn, true);
        ArrayList<Junction> sp = lg.getShortestPath(rn.getNode(orig), rn.getNode(dest));

        return sp;
    }

    public HashMap<ArrayList<Junction>, Double> getAllPathsLength(String orig, String dest) {
        LengthGraph lg = new LengthGraph(rn, true);
        HashMap<ArrayList<Junction>, Double> map = lg.getAllPaths(rn.getNode(orig), rn.getNode(dest));
        return map;
    }

    public double getPathLength(ArrayList<Junction> lNodes) {
        LengthGraph lg = new LengthGraph(rn, true);
        double pl = lg.getPathLength(lNodes);
        return pl;
    }

}
