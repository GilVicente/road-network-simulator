/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AnalisysController;

import Graphs.ConsumptionGraph;
import Graphs.FastestGraph;
import Graphs.TheoConsumptionGraph;
import Physics.CarPhysics;
import Utils.Utils;
import Vehicle.Vehicle;
import java.util.ArrayList;
import java.util.HashMap;
import road.Junction;
import road.RoadNetwork;
import road.RoadNetworkController;
import road.Section;
import road.Segment;

/**
 *
 * @author Yahkiller
 */
public class TheoEfficientController {

    private RoadNetworkController rc;
    private RoadNetwork rn;
    private Vehicle v;

    public TheoEfficientController(RoadNetworkController rc, Vehicle v) {
        this.rc = rc;
        this.rn = rc.getRn();
        this.v = v;
    }

    public ArrayList<Junction> mostEfficientPath(String orig, String dest) {
        //printPath
        TheoConsumptionGraph eg = new TheoConsumptionGraph(this, true);
        ArrayList<Junction> ep = eg.getLessConsumptionPath(getRn().getNode(orig), getRn().getNode(dest));
        return ep;
    }

    public ArrayList<Section> mostEfficientPathSection(Vehicle v, ArrayList<Junction> fp) {
        FastestGraph fg = new FastestGraph(rn, v, true);
        ArrayList<Section> lSections = routeEdgesByConsumption(fp, v);
        return lSections;
    }

    public HashMap<ArrayList<Junction>, Double> getAllEfficientPaths(String orig, String dest) {
        TheoConsumptionGraph eg = new TheoConsumptionGraph(this, true);
        HashMap<ArrayList<Junction>, Double> map = eg.getAllPaths(getRn().getNode(orig), getRn().getNode(dest));
        return map;
    }

    public double getPathConsumption(ArrayList<Junction> lNodes, Vehicle v) {
        TheoConsumptionGraph eg = new TheoConsumptionGraph(this, true);
        return eg.getPathConsumption(lNodes);
    }

    public ArrayList<Section> routeEdgesByConsumption(ArrayList<Junction> lNodes, Vehicle v) {
        ArrayList<Section> lSecs = new ArrayList<>();
        for (int i = 0; i < lNodes.size() - 1; i++) {

            ArrayList<Section> ls = getRn().getSections(lNodes.get(i).getKey(), lNodes.get(i + 1).getKey());
            Section s = getMinConsumptionSection(ls, v);
            lSecs.add(s);
        }
        return lSecs;
    }

    public double sectionConsumption(Section s, Vehicle v) {
        double en = 0.0;
//        int g = 5;
//
//        for (Segment seg : s.getlSegments()) {
//
//            if (Utils.withoutPercent(seg.getSlope()) != 0) {
//                double f = CarPhysics.slopeForce(getV(), s, seg, 25, g);
//                while (f <= 0 && g > 0) {
//                    g--;
//                    f = CarPhysics.slopeForce(getV(), s, seg, 25, g);
//                }
//
//                // double w = CarPhysics.calculateWork(f, (Utils.fromKmToDouble(seg.getLenght()) * 1000));
//                double P = CarPhysics.calculatePower(getV(), seg, 25, g);
//                double res = CarPhysics.calculateFuelGrams(P, getV(), seg, g, 25);
//                res = CarPhysics.fromGramsToGas(res);
//                en += res;
//            } else {
//                double f = CarPhysics.planeForce(getV(), s, seg, 25, g);
//                while (f <= 0 && g > 0) {
//                    g--;
//                    f = CarPhysics.planeForce(getV(), s, seg, 25, g);
//                }
//
//                // double w = CarPhysics.calculateWork(f, (Utils.fromKmToDouble(seg.getLenght()) * 1000));
//                double P = CarPhysics.calculatePower(getV(), seg, 25, g);
//                double res = CarPhysics.calculateFuelGrams(P, getV(), seg, g, 25);
//                res = CarPhysics.fromGramsToGas(res);
//                en += res;
//            }
//        }
        return en;
    }

    public Section getMinConsumptionSection(ArrayList<Section> ls, Vehicle v) {
        HashMap<Section, Double> map = new HashMap();
        for (Section s : ls) {
            map.put(s, sectionConsumption(s, v));
        }
        return Utils.getMinSection(map);
    }

    /**
     * @return the rc
     */
    public RoadNetworkController getRc() {
        return rc;
    }

    /**
     * @param rc the rc to set
     */
    public void setRc(RoadNetworkController rc) {
        this.rc = rc;
    }

    /**
     * @return the rn
     */
    public RoadNetwork getRn() {
        return rn;
    }

    /**
     * @param rn the rn to set
     */
    public void setRn(RoadNetwork rn) {
        this.rn = rn;
    }

    /**
     * @return the v
     */
    public Vehicle getV() {
        return v;
    }

    /**
     * @param v the v to set
     */
    public void setV(Vehicle v) {
        this.v = v;
    }

}
