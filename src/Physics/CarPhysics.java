package Physics;

import Graphs.dataWorkSFC;
import Vehicle.Regime;
import Vehicle.Throttle;
import Vehicle.Vehicle;
import Vehicle.Velocity;
import java.text.DecimalFormat;
import java.util.ArrayList;
import road.Section;
import road.Segment;

/**
 *
 * @author Yahkiller
 */
public class CarPhysics {

    private static double g = 9.8;
    private static double air_density = 1.255;

    public static dataWorkSFC calculateForce(boolean isPlane, Vehicle vehicle, Section sec, Segment seg, int throt, int gear) {
        dataWorkSFC d;
        if (isPlane) {
            d = planeForce(vehicle, sec, seg, throt, gear);
        } else {
            d = slopeForce(vehicle, sec, seg, throt, gear);
        }
        return d;
    }

    public static dataWorkSFC planeForce(Vehicle vehicle, Section sec, Segment seg, int throttle_id, int gear) {
        double G = vehicle.getEnergy().getFinal_drive_ratio();
        double r = vehicle.getWheel_size() / 2; //metros

        double vMax_segm = calculateMaxVel(vehicle, sec, seg);
        double gk = vehicle.getEnergy().getGear(gear).getRatio();

        double rpm = calculateRPM(G, gk, r, vMax_segm);

        if (throttle_id > 2) {
            System.out.println("X");
        }
        double f = calculateCarForce(vehicle, sec, seg, throttle_id, gear) - calculatePlaneTotalResistanceForces(vehicle, sec, seg);
        dataWorkSFC d = new dataWorkSFC(f, rpm);
        return d;
    }

    public static dataWorkSFC slopeForce(Vehicle vehicle, Section sec, Segment seg, int throttle_id, int gear) {
        double G = vehicle.getEnergy().getFinal_drive_ratio();             //final_drive_ratio_
        double r = vehicle.getWheel_size() / 2; //metros
        double gk = vehicle.getEnergy().getGear(gear).getRatio();

        double vMax_segm = calculateMaxVel(vehicle, sec, seg);
        double rpm = calculateRPM(G, gk, r, vMax_segm);

        if (throttle_id > 2) {
            System.out.println("X");
        }

        double f = calculateCarForce(vehicle, sec, seg, throttle_id, gear) - calculateSlopeTotalResistanceForces(vehicle, sec, seg);
        dataWorkSFC d = new dataWorkSFC(f, rpm);
        return d;
    }

    public static double calculateCarForce(Vehicle vehicle, Section sec, Segment seg, int throt, int gear) {
        double G = vehicle.getEnergy().getFinal_drive_ratio();             //final_drive_ratio_
        double r = vehicle.getWheel_size() / 2; //metros
        double gk = vehicle.getEnergy().getGear(gear).getRatio();
        double wind_speed = Utils.Utils.fromKmToDouble(sec.getWind_speed());
        double wind_direc = sec.getWind_angle();
        int sentido = getSentido(sec);

        double vMax_segm = calculateMaxVel(vehicle, sec, seg);
        double vRel = relVelocity(vMax_segm, wind_speed, wind_direc, sentido);
        double v = conv(vRel);

        double rpm = calculateRPM(G, gk, r, vMax_segm);
        double torque = calculateTorque(rpm, vehicle, throt);

        if (torque == 0.0) {
            vMax_segm = calculateVRpmVehicle(vehicle, gear);
            if (vMax_segm > calculateMaxVel(vehicle, sec, seg)) {
                vMax_segm = calculateMaxVel(vehicle, sec, seg);
            }
            vRel = relVelocity(vMax_segm, wind_speed, wind_direc, sentido);
            v = conv(vRel);
            rpm = calculateRPM(G, gk, r, vMax_segm);
            torque = calculateTorque(rpm, vehicle, throt);
        }

        double f = ((torque * G * gk) / r);
//        System.out.println(" CAR FORCE"+f);
        return f;
    }

    public static double calculatePlaneTotalResistanceForces(Vehicle vehicle, Section sec, Segment seg) {
        double rrc = vehicle.getRrc();
        double m = Utils.Utils.fromKmToDouble(vehicle.getMass());
        double frontal_Area = vehicle.getFrontal_area();
        double air_drag = vehicle.getDrag_coef();
        double vMax_segm = calculateMaxVel(vehicle, sec, seg);
        int sentido = getSentido(sec);
        double wind_speed = Utils.Utils.fromKmToDouble(sec.getWind_speed());
        double wind_direc = sec.getWind_angle();
        double vRel = relVelocity(vMax_segm, wind_speed, wind_direc, sentido);
        double v = conv(vRel);

        double roll = rrc * m * g;
//        System.out.println("Rolling resistance: "+roll);

        double air = 0.5 * air_drag * frontal_Area * air_density * Math.pow(v, 2);
//        System.out.println("Air resistance: "+air);

        double f = roll + air;
//        System.out.println("Braking Force: " + f);
        return f;

    }

    public static double calculateSlopeTotalResistanceForces(Vehicle vehicle, Section sec, Segment seg) {
        double rrc = vehicle.getRrc();
        double r = vehicle.getWheel_size() / 2; //metros
        double m = Utils.Utils.fromKmToDouble(vehicle.getMass());
        double frontal_Area = vehicle.getFrontal_area();
        double air_drag = vehicle.getDrag_coef();
        double ang = toRadians(Utils.Utils.withoutPercent(seg.getSlope()));
        double vMax_segm = calculateMaxVel(vehicle, sec, seg);
        int sentido = getSentido(sec);
        double wind_speed = Utils.Utils.fromKmToDouble(sec.getWind_speed());
        double wind_direc = sec.getWind_angle();
        double vRel = relVelocity(vMax_segm, wind_speed, wind_direc, sentido);
        double v = conv(vRel);

        double eP = m * g * Math.sin(ang);
//        System.out.println("Gravitational Force: " + eP);

        double roll = rrc * m * g * Math.cos(ang);
//        System.out.println("Rolling resistance: " + roll);

        double air = 0.5 * air_drag * frontal_Area * air_density * Math.pow(v, 2);
//        System.out.println("Air Resistance: "+air);

        double f = roll + air + eP;
//        System.out.println("Braking Force: " + f);
        return f;
    }

    public static double relVelocity(double vMax_segm, double wind_speed, double wind_direc, int sentido) {
        double vRel = 0.0;
        vRel = vMax_segm - (wind_speed * Math.cos(wind_direc) * sentido);
        if (vRel > 0 && vRel < 30 && (sentido * Math.cos(wind_direc) > 0)) {
            vRel = 30;
        } else if (vRel < 0) {
            vRel = vRel * -1;
        }
        return vRel;
    }

    public static double calculateRPM(double G, double gk, double r, double v) {
        double rpm = (60 * G * gk * conv(v)) / (2 * Math.PI * r);
        return rpm;
    }

    public static double calculateRPM(Vehicle vehicle, int gear) {
        double G = vehicle.getEnergy().getFinal_drive_ratio();
        double gk = vehicle.getEnergy().getGear(gear).getRatio();
        double r = vehicle.getWheel_size() / 2; //metros
        double v = 0.0;

        double rpm = (60 * G * gk * conv(v)) / (2 * Math.PI * r);
        return rpm;
    }

    public static double calculateVRpmVehicle(Vehicle vehicle, int gear) {
        double G = vehicle.getEnergy().getFinal_drive_ratio();
        double gk = vehicle.getEnergy().getGear(gear).getRatio();
        double r = vehicle.getWheel_size() / 2; //metros
        double v = 0.0;
        double rpm = vehicle.getEnergy().getMax_rpm();

        v = (2 * Math.PI * r * rpm) / (60 * G * gk);
        return v;
    }

    public static double calculateTorque(double rpm, Vehicle v, int id) {
        double torque = 0.0;
        if (id > 2) {
            System.out.println("SS");
        }
        Throttle t = v.getEnergy().getlThrottle().get(id);
        if (t == null) {
            System.out.println("S2");
        }
        ArrayList<Regime> lRegime = t.getlRegime();
        for (Regime r : lRegime) {
            if (rpm >= r.getRpm_low() && rpm <= r.getRpm_high()) {
                torque = r.getTorque();
            }
        }
        return torque;
    }

    public static double calculateSFC(double rpm, Vehicle v, int id) {
        double SFC = 0.0;
        if (id > 2) {
            System.out.println("SS");
        }
        Throttle t = v.getEnergy().getlThrottle().get(id);
        if (t == null) {
            System.out.println("S");
        }
        ArrayList<Regime> lRegime = t.getlRegime();
        for (Regime r : lRegime) {
            if (rpm >= r.getRpm_low() && rpm <= r.getRpm_high()) {
                SFC = r.getSFC();
            }
        }
        return SFC;
    }

    public static double conv(double v) {
        return v * 0.28;
    }

    public static double calculateWork(double f, double d) {
        return f * d;
    }

    public static double calculatePower(Vehicle v, Segment seg, int throt, int g) {
        double G = v.getEnergy().getFinal_drive_ratio();
        double gk = v.getEnergy().getGear(g).getRatio();
        double r = v.getWheel_size() / 2; //metros
        double vMax_segm = Utils.Utils.fromKmToDouble(seg.getMaxVel());

        double rpm = calculateRPM(G, gk, r, vMax_segm);
        double torque = calculateTorque(rpm, v, throt);
        double P = 2 * Math.PI * torque * (rpm / 60);
        return P; //W ou  J/s
    }

    public static double calculateFuelGrams(double P, Vehicle v, Segment s, int gear, int throttle_id) {
        double G = v.getEnergy().getFinal_drive_ratio();
        double gk = v.getEnergy().getGear(gear).getRatio();
        double r = v.getWheel_size() / 2; //metros
        double vMax_segm = Utils.Utils.fromKmToDouble(s.getMaxVel());

        double rpm = calculateRPM(G, gk, r, vMax_segm);
        double SFC = calculateSFC(rpm, v, throttle_id);

        double res = P * SFC;
        return res;
    }

    public static double fromGramsToGas(double g) {
        double res = g / 748.2; //1l de gasolina = 748.2g
        return res;
    }

    public static double cons(double f) {
        double c = 44400;
        return f / c;
    }

    public static double toRadians(double inc) {
        double x = inc / 100;
        double rad = Math.asin(x);
        return rad;
    }

    public static double calculateMaxVel(Vehicle vehicle, Section sec, Segment seg) {
        double vMax_segm = Utils.Utils.fromKmToDouble(seg.getMaxVel());;
        if (!vehicle.getlVelocity().isEmpty()) {
            ArrayList<Velocity> lVel = new ArrayList();
            for (Velocity v : lVel) {
                if (v.getSegment_type().equalsIgnoreCase(sec.getTypology())) {
                    vMax_segm = v.getLimit();
                }
            }
        }
        return vMax_segm;
    }

    public static int getSentido(Section s) {
        int sentido = 1;
        if (Utils.Utils.withoutBeginningChar(s.getBeginning_node()) > Utils.Utils.withoutBeginningChar(s.getEnding_node())) {
            sentido = -1;
        }
        return sentido;
    }

}
