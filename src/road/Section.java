/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package road;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author Yahkiller
 */
public class Section {

    private String road_name;
    private int id_section;
    private String beginning_node;
    private String ending_node;
    private String typology;
    private String direction;
    private double toll;
    private String wind_speed;
    private double wind_angle;
    private ArrayList<Segment> lSegments = new ArrayList();
   

    public Section(String road, int index, String beginning_node, String ending_node, String typology, String direction, double toll, String wind_speed, double wind_angle) {
        this.road_name = road;
        this.id_section = index;
        this.beginning_node = beginning_node;
        this.ending_node = ending_node;
        this.typology = typology;
        this.direction = direction;
        this.toll = toll;
        this.wind_angle = wind_angle;
        this.wind_speed = wind_speed;

    }

    public Section() {

    }

    /**
     * @return the beginning_node
     */
    public String getBeginning_node() {
        return beginning_node;
    }

    /**
     * @param beginning_node the beginning_node to set
     */
    public void setBeginning_node(String beginning_node) {
        this.beginning_node = beginning_node;
    }

    /**
     * @return the ending_node
     */
    public String getEnding_node() {
        return ending_node;
    }

    /**
     * @param ending_node the ending_node to set
     */
    public void setEnding_node(String ending_node) {
        this.ending_node = ending_node;
    }

    /**
     * @return the typology
     */
    public String getTypology() {
        return typology;
    }

    /**
     * @param typology the typology to set
     */
    public void setTypology(String typology) {
        this.typology = typology;
    }

    /**
     * @return the direction
     */
    public String getDirection() {
        return direction;
    }

    /**
     * @param direction the direction to set
     */
    public void setDirection(String direction) {
        this.direction = direction;
    }

    /**
     * @return the toll
     */
    public double getToll() {
        return toll;
    }

    /**
     * @param toll the toll to set
     */
    public void setToll(double toll) {
        this.toll = toll;
    }

    /**
     * @return the wind_speed
     */
    public String getWind_speed() {
        return wind_speed;
    }

    /**
     * @param wind_speed the wind_speed to set
     */
    public void setWind_speed(String wind_speed) {
        this.wind_speed = wind_speed;
    }

    /**
     * @return the wind_angle
     */
    public double getWind_angle() {
        return wind_angle;
    }

    /**
     * @param wind_angle the wind_angle to set
     */
    public void setWind_angle(double wind_angle) {
        this.wind_angle = wind_angle;
    }

    /**
     * @return the index
     */
    public int getIndex() {
        return getId_section();
    }

    /**
     * @param index the index to set
     */
    public void setIndex(int index) {
        this.setId_section(index);
    }

    /**
     * @return the id_road
     */
    public String getId_road() {
        return road_name;
    }

    /**
     * @param id_road the id_road to set
     */
    public void setId_road(String id_road) {
        this.road_name = id_road;
    }

    /**
     * @return the id_section
     */
    public int getId_section() {
        return id_section;
    }

    /**
     * @param id_section the id_section to set
     */
    public void setId_section(int id_section) {
        this.id_section = id_section;
    }

    /**
     * @return the lSegments
     */
    public ArrayList<Segment> getlSegments() {
        return lSegments;
    }

    /**
     * @param lSegments the lSegments to set
     */
    public void setlSegments(ArrayList<Segment> lSegments) {
        this.lSegments = lSegments;
    }

    /**
     * @param lSegments the lSegments to set
     */
    public void addSegment(Segment s) {
        this.lSegments.add(s);
    }

    public String getRoad_name() {
        return road_name;
    }

    public void setRoad_name(String road_name) {
        this.road_name = road_name;
    }
}
