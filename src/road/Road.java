/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package road;

import java.util.ArrayList;

/**
 *
 * @author Yahkiller
 */
public class Road {

    private String name;
    private ArrayList<Section> lSections;

    public Road (){
        
    }
    
    public Road(String name) {
        this.name = name;
    }

    public void addSection(Section s) {
        this.lSections.add(s);
    }

    public void removeSection(Section rem) {
        int i = 0;
        int index = 0;
        for (Section s : lSections) {
            if (s.getIndex() == rem.getIndex()) {
                this.lSections.remove(i);
            }
            i++;
        }

    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the lSections
     */
    public ArrayList<Section> getlSections() {
        return lSections;
    }

    /**
     * @param lSections the lSections to set
     */
    public void setlSections(ArrayList<Section> lSections) {
        this.lSections = lSections;
    }


}
