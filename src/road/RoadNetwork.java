/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package road;

import graphbase.Graph;
import graphbase.GraphAlgorithms;
import graphbase.Vertex;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.List;
import Utils.Utils;
import Vehicle.Vehicle;
import java.util.HashMap;

/**
 *
 * @author Yahkiller
 */
public class RoadNetwork {

    private Graph<String, Integer> roadNetwork;
    private ArrayList<Junction> lNodes;
    private ArrayList<Section> lSections;
    private ArrayList<Road> lRoads;
    private ArrayList<Segment> lSegments;

    public RoadNetwork() {
        lSections = new ArrayList();
        lSegments = new ArrayList();
        lNodes = new ArrayList();
    }

    public RoadNetwork(boolean isDirected, ArrayList<Junction> lNodes, ArrayList<Road> lRoads, ArrayList<Section> lSections, ArrayList<Segment> lSegments) {
        this.roadNetwork = new Graph(isDirected);
        this.lNodes = lNodes;
        this.lRoads = lRoads;
        this.lSections = lSections;

    }

    /**
     * @return the roadNetwork
     */
    public Graph<String, Integer> getRoadNetwork() {
        return roadNetwork;
    }

    public Junction getNode(String key) {
        for (Junction n : getlNodes()) {
            if (n.getKey().equalsIgnoreCase(key)) {
                return n;
            }
        }
        return null;
    }

    public boolean hasNode(String key) {
        boolean flag = false;
        for (Junction n : getlNodes()) {
            if (n.getKey().equalsIgnoreCase(key)) {
                flag = true;
            }
        }
        return flag;
    }

    public Road getRoad(String id_road) {
        for (Road r : getlRoads()) {
            if (r.getName().equalsIgnoreCase(id_road)) {
                return r;
            }
        }
        return null;
    }

    public ArrayList<Section> getRoadSections(String id_road) {
        ArrayList<Section> sections = new ArrayList();
        for (Section s : getlSections()) {
            if (s.getId_road().equalsIgnoreCase(id_road)) {
                sections.add(s);
            }
        }

        return sections;
    }

    public Section getSection(int id_section) {
        for (Section s : getlSections()) {
            if (s.getId_section() == id_section) {
                return s;
            }

        }
        return null;
    }
//ARRAY

    public Section getSection(String a, String b) {
        for (Section s : getlSections()) {
            if (s.getDirection().equalsIgnoreCase("Bidirectional")) {
                if ((s.getBeginning_node().equalsIgnoreCase(a)
                        && (s.getEnding_node().equalsIgnoreCase(b)))
                        || (s.getBeginning_node().equalsIgnoreCase(b)
                        && (s.getEnding_node().equalsIgnoreCase(a)))) {
                    return s;
                }
            } else if (s.getDirection().equalsIgnoreCase("Direct")) {
                if ((s.getBeginning_node().equalsIgnoreCase(a)
                        && (s.getEnding_node().equalsIgnoreCase(b)))) {
                    return s;
                }
            } else {
                if ((s.getBeginning_node().equalsIgnoreCase(b)
                        && (s.getEnding_node().equalsIgnoreCase(a)))) {
                    return s;
                }
            }

        }
        return null;
    }

    public ArrayList<Section> getSections(String a, String b) {
        ArrayList<Section> lSections = new ArrayList();
        for (Section s : getlSections()) {
            if (s.getDirection().equalsIgnoreCase("Bidirectional")) {
                if ((s.getBeginning_node().equalsIgnoreCase(a)
                        && (s.getEnding_node().equalsIgnoreCase(b)))
                        || (s.getBeginning_node().equalsIgnoreCase(b)
                        && (s.getEnding_node().equalsIgnoreCase(a)))) {
                    lSections.add(s);
                }
            } else if (s.getDirection().equalsIgnoreCase("Direct")) {
                if ((s.getBeginning_node().equalsIgnoreCase(a)
                        && (s.getEnding_node().equalsIgnoreCase(b)))) {
                    lSections.add(s);
                }
            } else {
                if ((s.getBeginning_node().equalsIgnoreCase(b)
                        && (s.getEnding_node().equalsIgnoreCase(a)))) {
                    lSections.add(s);
                }
            }

        }
        return lSections;
    }

    public Section getSectionsByRoad(String a, String b, String idRoad, ArrayList<Section> ls) {

        for (Section l : ls) {
            if (l.getId_road().equalsIgnoreCase(idRoad)
                    && l.getBeginning_node().equalsIgnoreCase(a)
                    && l.getEnding_node().equalsIgnoreCase(b)) {
                return l;

            }
        }

        return null;
    }

    public Segment getSegment(int id_segment) {
        for (Segment s : getlSegments()) {
            if (s.getId_segment() == id_segment) {
                return s;
            }

        }
        return null;
    }

    public ArrayList<Segment> getOrderedSegments(Section s) {
        ArrayList<Segment> segments = s.getlSegments();
        Collections.sort(segments, new CustomComparator());
        return segments;

    }

    public double getSectionLength(Section s) {
        Double length = 0.0;
        ArrayList<Segment> segments = getOrderedSegments(s);
        for (Segment ss : segments) {
            length += Utils.fromKmToDouble(ss.getLenght());
        }
        return length;
    }

    /**
     * @return the lNodes
     */
    public ArrayList<Junction> getlNodes() {
        return lNodes;
    }

    /**
     * @param lNodes the lNodes to set
     */
    public void setlNodes(ArrayList<Junction> lNodes) {
        this.lNodes = lNodes;
    }

    /**
     * @return the lSections
     */
    public ArrayList<Section> getlSections() {
        return lSections;
    }

    /**
     * @param lSections the lSections to set
     */
    public void setlSections(ArrayList<Section> lSections) {
        this.lSections = lSections;
    }

    /**
     * @return the lRoads
     */
    public ArrayList<Road> getlRoads() {
        return lRoads;
    }

    /**
     * @param lRoads the lRoads to set
     */
    public void setlRoads(ArrayList<Road> lRoads) {
        this.lRoads = lRoads;
    }

    /**
     * @return the lSegments
     */
    public ArrayList<Segment> getlSegments() {
        return lSegments;
    }

    /**
     * @param lSegments the lSegments to set
     */
    public void setlSegments(ArrayList<Segment> lSegments) {
        this.lSegments = lSegments;
    }

    public class CustomComparator implements Comparator<Segment> {

        @Override
        public int compare(Segment t, Segment t1) {
            return t.getId_segment() - t1.getId_segment();
        }
    }

    public ArrayList<Section> routeEdges(ArrayList<Junction> lNodes) {
        ArrayList<Section> lSecs = new ArrayList<>();
        for (int i = 0; i < lNodes.size() - 1; i++) {
            Section s = this.getSection(lNodes.get(i).getKey(), lNodes.get(i + 1).getKey());
            lSecs.add(s);
        }
        return lSecs;
    }

    public void addSection(Section s) {
        this.lSections.add(s);
    }

    public void addNode(Junction n) {
        this.lNodes.add(n);
    }

    
}
