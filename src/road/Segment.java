/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package road;

import Vehicle.Vehicle;
import java.util.ArrayList;

/**
 *
 * @author Yahkiller
 */
public class Segment {

    private int id_section;
    private int id_segment;
    private double height;
    private String slope;
    private String lenght;
    private int max_vehicles;
    private String maxVel;
    private String minVel;
    private ArrayList<Vehicle> due = new ArrayList();

    public Segment() {
    }

    public Segment(int id_section, int index, double height, String slope, String lenght, int max_vehicles, String maxVel, String minVel) {
        this.id_section = id_section;
        this.id_segment = index;
        this.height = height;
        this.slope = slope;
        this.lenght = lenght;

        this.max_vehicles = max_vehicles;
        this.maxVel = maxVel;
        this.minVel = minVel;
    }

    /**
     * @return the index
     */
    public int getId_segment() {
        return id_segment;
    }

    /**
     * @param id_segment the index to set
     */
    public void setId_segment(int id_segment) {
        this.id_segment = id_segment;
    }

    /**
     * @return the height
     */
    public double getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(double height) {
        this.height = height;
    }

    /**
     * @return the slope
     */
    public String getSlope() {
        return slope;
    }

    /**
     * @param slope the slope to set
     */
    public void setSlope(String slope) {
        this.slope = slope;
    }

    /**
     * @return the lenght
     */
    public String getLenght() {
        return lenght;
    }

    /**
     * @param lenght the lenght to set
     */
    public void setLenght(String lenght) {
        this.lenght = lenght;
    }

    /**
     * @return the max_vehicles
     */
    public int getMax_vehicles() {
        return max_vehicles;
    }

    /**
     * @param max_vehicles the max_vehicles to set
     */
    public void setMax_vehicles(int max_vehicles) {
        this.max_vehicles = max_vehicles;
    }

    /**
     * @return the maxVel
     */
    public String getMaxVel() {
        return maxVel;
    }

    /**
     * @param maxVel the maxVel to set
     */
    public void setMaxVel(String maxVel) {
        this.maxVel = maxVel;
    }

    /**
     * @return the minVel
     */
    public String getMinVel() {
        return minVel;
    }

    /**
     * @param minVel the minVel to set
     */
    public void setMinVel(String minVel) {
        this.minVel = minVel;
    }

    public void setVehicleOut(Vehicle v) {
        for (Vehicle t : due) {
            if (t.getId() == v.getId()) {
                v.setIsOut(true);
            }
        }
    }

    public boolean getVehicleStatus(Vehicle v) {
        boolean bool = false;
        for (Vehicle t : due) {
            if (t.getId() == v.getId()) {
                bool = v.isOut();
            }
        }
        return bool;
    }

    public void addVehicle(Vehicle v) {
        this.due.add(v);
    }

    public void removeVehicle(Vehicle v) {
        this.due.remove(v);
    }

    public ArrayList<Vehicle> getDue() {
        return due;
    }

    public void setDue(ArrayList<Vehicle> due) {
        this.due = due;
    }

    /**
     * @return the id_section
     */
    public int getId_section() {
        return id_section;
    }

    /**
     * @param id_section the id_section to set
     */
    public void setId_section(int id_section) {
        this.id_section = id_section;
    }

}
