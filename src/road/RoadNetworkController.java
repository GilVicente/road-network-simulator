/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package road;

import AnalisysController.EfficientController;
import AnalisysController.FastestController;
import AnalisysController.ShortestController;
import AnalisysController.TheoEfficientController;
import AnalisysController.TollCostController;

import Vehicle.Vehicle;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import java.util.HashMap;

/**
 *
 * @author Yahkiller
 */
public class RoadNetworkController {

    private RoadNetwork rn;

    public RoadNetworkController() {

    }

    public RoadNetworkController(boolean isDirected, RoadNetwork roadN) throws IOException {
        rn = roadN;
    }

    //Varios tipos de best path: por tempo, consumo de energia, custo das portagens
    public ArrayList<Junction> bestPath(String algorithm, String orig, String dest, Vehicle v) {

        if (algorithm.equalsIgnoreCase("Fastest")) {
            FastestController fc = new FastestController(this);
            return fc.fastestPath(orig, dest, v);
        } else if (algorithm.equalsIgnoreCase("Most Efficient")) {
            EfficientController ec = new EfficientController(this, v);
            return ec.mostEfficientPath(orig, dest);
        } else if (algorithm.equalsIgnoreCase("Cheapest")) {
            TollCostController tc = new TollCostController(this);
            return tc.cheapestTollPath(orig, dest);
        } else if (algorithm.equalsIgnoreCase("Shortest")) {
            ShortestController sc = new ShortestController(this);
            return sc.shortestPath(orig, dest);
        } else if (algorithm.equalsIgnoreCase("Theoretical")) {
            TheoEfficientController sc = new TheoEfficientController(this, v);
            return sc.mostEfficientPath(orig, dest);
        }
        return null;
    }

    public String bestPathValue(String algorithm, String orig, String dest, Vehicle v) {
        DecimalFormat df = new DecimalFormat("#.##");
        if (algorithm.equalsIgnoreCase("Fastest")) {
            FastestController fc = new FastestController(this);
            return df.format(fc.getPathFastTime(fc.fastestPath(orig, dest, v), v)) + " min";
        } else if (algorithm.equalsIgnoreCase("Most Efficient")) {
            EfficientController ec = new EfficientController(this, v);
            return df.format(ec.getPathConsumption(ec.mostEfficientPath(orig, dest), v)) + " Kj";
        } else if (algorithm.equalsIgnoreCase("Cheapest")) {
            TollCostController tc = new TollCostController(this);
            return df.format(tc.getPathTollCost(tc.cheapestTollPath(orig, dest))) + " €";
        } else if (algorithm.equalsIgnoreCase("Shortest")) {
            ShortestController sc = new ShortestController(this);
            return df.format(sc.getPathLength(sc.shortestPath(orig, dest))) + " Km";
        } else if (algorithm.equalsIgnoreCase("Theoretical")) {
            TheoEfficientController sc = new TheoEfficientController(this, v);
            return df.format(sc.getPathConsumption(sc.mostEfficientPath(orig, dest), v)) + " Kj";
        }
        return null;
    }

    public ArrayList<Section> bestPathSections(String algorithm, String orig, String dest, Vehicle v) {

        if (algorithm.equalsIgnoreCase("Fastest")) {
            FastestController fc = new FastestController(this);
            return fc.fastestPathSections(v, fc.fastestPath(orig, dest, v));
        } else if (algorithm.equalsIgnoreCase("Most Efficient")) {
            EfficientController ec = new EfficientController(this, v);
            return ec.mostEfficientPathSection(v, ec.mostEfficientPath(orig, dest));
        } else if (algorithm.equalsIgnoreCase("Cheapest")) {
            TollCostController tc = new TollCostController(this);
            return null;
        } else if (algorithm.equalsIgnoreCase("Shortest")) {
            ShortestController sc = new ShortestController(this);
            return null;
        } else if (algorithm.equalsIgnoreCase("Theoretical")) {
            TheoEfficientController sc = new TheoEfficientController(this, v);
            return sc.mostEfficientPathSection(v, sc.mostEfficientPath(orig, dest));
        }
        return null;
    }

    //Varios tipos de best path: por tempo, consumo de energia, custo das portagens
    public HashMap<ArrayList<Junction>, Double> otherPaths(String algorithm, String orig, String dest, Vehicle v) {

        if (algorithm.equalsIgnoreCase("Fastest")) {
            FastestController fc = new FastestController(this);
            return fc.getAllFastestPaths(orig, dest, v);
        } else if (algorithm.equalsIgnoreCase("Most Efficient")) {
            EfficientController ec = new EfficientController(this, v);
            return ec.getAllEfficientPaths(orig, dest);
        } else if (algorithm.equalsIgnoreCase("Cheapest")) {
            TollCostController tc = new TollCostController(this);
            return tc.getAllPathsCost(orig, dest);
        } else if (algorithm.equalsIgnoreCase("Shortest")) {
            ShortestController sc = new ShortestController(this);
            return sc.getAllPathsLength(orig, dest);
        } else if (algorithm.equalsIgnoreCase("Theoretical")) {
            TheoEfficientController sc = new TheoEfficientController(this, v);
            return sc.getAllEfficientPaths(orig, dest);
        }
        return null;
    }

    /**
     * @return the rn
     */
    public RoadNetwork getRn() {
        return rn;
    }

    /**
     * @param rn the rn to set
     */
    public void setRn(RoadNetwork rn) {
        this.rn = rn;
    }

}
