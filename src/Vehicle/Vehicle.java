/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vehicle;

import java.util.ArrayList;
import road.Section;
import road.Segment;

/**
 *
 * @author Yahkiller
 */
public class Vehicle {

    private int id;
    private String name;
    private String description;
    private double frontal_area;
    private String type;
    private String motor;
    private String fuel;
    private String mass;
    private String load;

    private double drag_coef;
    private double rrc;
    private double wheel_size;
    private Energy e;
    private Throttle t;
    private ArrayList<Velocity> lVelocity = new ArrayList();
    private boolean isOut = false;
    private ArrayList<Regime> lRegime = new ArrayList();
    private ArrayList<Time> inTime = new ArrayList();
    private ArrayList<Time> outTime = new ArrayList();

    public Vehicle() {

    }

    public Vehicle(int id, String name, String description, String type, String motor, String fuel,
            String mass, String load, double rrc,
            double wheel_size, double drag_coef, double frontal_area, Energy e,
            ArrayList<Velocity> lVelocity, ArrayList<Regime> lRegime) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.type = type;
        this.motor = motor;
        this.fuel = fuel;
        this.frontal_area = frontal_area;
        this.mass = mass;
        this.load = load;
        this.rrc = rrc;
        this.wheel_size = wheel_size;
        this.drag_coef = drag_coef;
        this.e = e;

        this.lVelocity = lVelocity;
        this.lRegime = lRegime;

    }

    public Vehicle(int IDVEHICLE, String name, String description, String type,
            String MOTORIZATION, String FUEL, String MASS, String LOAD, double RRC,
            double WHEELSIZE, double DRAGCOEFFICIENT, double FRONTALAREA) {
        this.id = IDVEHICLE;
        this.name = name;
        this.description = description;
        this.type = type;
        this.motor = MOTORIZATION;
        this.fuel = FUEL;
        this.mass = MASS;
        this.load = LOAD;
        this.rrc = RRC;
        this.wheel_size = WHEELSIZE;
        this.drag_coef = DRAGCOEFFICIENT;
        this.frontal_area = FRONTALAREA;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    /**
     * @return the motor
     */
    public String getMotor() {
        return motor;
    }

    /**
     * @param motor the motor to set
     */
    public void setMotor(String motor) {
        this.motor = motor;
    }

    /**
     * @return the mass
     */
    public String getMass() {
        return mass;
    }

    /**
     * @param mass the mass to set
     */
    public void setMass(String mass) {
        this.mass = mass;
    }

    /**
     * @return the drag_coef
     */
    public double getDrag_coef() {
        return drag_coef;
    }

    /**
     * @param drag_coef the drag_coef to set
     */
    public void setDrag_coef(double drag_coef) {
        this.drag_coef = drag_coef;
    }

    public String getLoad() {
        return load;
    }

    public void setLoad(String load) {
        this.load = load;
    }

    public double getRrc() {
        return rrc;
    }

    public void setRrc(double rrc) {
        this.rrc = rrc;
    }

    public double getWheel_size() {
        return wheel_size;
    }

    public void setWheel_size(double wheel_size) {
        this.wheel_size = wheel_size;
    }

    public Energy getEnergy() {
        return e;
    }

    public void setEnergy(Energy e) {
        this.e = e;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void addGear(Gear g) {
        this.e.getlGear().add(g);

    }

    public void addThrottle(Throttle t) {
        this.e.getlThrottle().add(t);

    }

    public void addRegime(Regime t) {
        this.t.getlRegime().add(t);

    }

    public void addVelocity(Velocity g) {
        this.getlVelocity().add(g);

    }

    public ArrayList<Velocity> getlVelocity() {
        return lVelocity;
    }

    public void setlGear(ArrayList<Velocity> lVelocity) {
        this.lVelocity = lVelocity;
    }

    public double getVelocityByTypology(String t) {
        for (Velocity v : lVelocity) {
            if (v.getSegment_type() != null) {
                if (v.getSegment_type().equalsIgnoreCase(t)) {
                    return v.getLimit();
                }
            }
        }
        return 100.0;

    }

    public double getFrontal_area() {
        return frontal_area;
    }

    public void setFrontal_area(double frontal_area) {
        this.frontal_area = frontal_area;
    }

    /**
     * @return the lTime
     */
    public ArrayList<Time> getInTime() {
        return inTime;
    }

    /**
     * @param lTime the lTime to set
     */
    public void setInTime(ArrayList<Time> lTime) {
        this.inTime = lTime;
    }

    public double getInTime(Segment s) {
        for (Time t : this.inTime) {
            if (t.getS() == s) {
                return t.getTime();
            }
        }
        return 0.0;
    }

    public void addInTime(Time time) {
        this.inTime.add(time);
    }

    public void addOutTime(Time time) {
        this.outTime.add(time);
    }

    public boolean hasSegInTime(Time time) {
        boolean bool = false;
        for (Time t : this.inTime) {
            // if (t.getS().getId_segment() == seg.getId_segment() && t.getS().getLenght() == seg.getLenght() && t.getS().getHeight() == seg.getHeight()) {
            if (t.getTime() == time.getTime() && t.getS().getId_section() == time.getS().getId_section() && t.getS().getId_segment() == time.getS().getId_segment()) {
                bool = true;
            }
        }
        return bool;
    }

    public boolean hasSegOutTime(Time time) {
        boolean bool = false;
        for (Time t : this.outTime) {
            //           if (t.getS().getId_segment() == seg.getId_segment() && t.getS().getLenght() == seg.getLenght() && t.getS().getHeight() == seg.getHeight()) {
            if (t.getTime() == time.getTime() && t.getS().getId_section() == time.getS().getId_section() && t.getS().getId_segment() == time.getS().getId_segment()) {
                bool = true;
            }
        }
        return bool;
    }

    /**
     * @return the outTime
     */
    public ArrayList<Time> getOutTime() {
        return outTime;
    }

    /**
     * @param outTime the outTime to set
     */
    public void setOutTime(ArrayList<Time> outTime) {
        this.outTime = outTime;
    }

    /**
     * @return the isOut
     */
    public boolean isOut() {
        return isOut;
    }

    /**
     * @param isOut the isOut to set
     */
    public void setIsOut(boolean isOut) {
        this.isOut = isOut;
    }
}
