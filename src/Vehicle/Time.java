/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vehicle;

import road.Section;
import road.Segment;

/**
 *
 * @author Yahkiller
 */
public class Time {

    private double time;
    private Segment s;

    public Time() {

    }

    public Time(double time, Segment s) {
        this.time = time;
        this.s = s;
    }

    /**
     * @return the out
     */
    public double getTime() {
        return time;
    }

    /**
     * @param out the out to set
     */
    public void setTime(double out) {
        this.time = out;
    }

    /**
     * @return the s
     */
    public Segment getS() {
        return s;
    }

    /**
     * @param s the s to set
     */
    public void setS(Segment s) {
        this.s = s;
    }
}
