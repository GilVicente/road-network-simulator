/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vehicle;

import java.util.ArrayList;

/**
 *
 * @author Luís Rocha
 */
public class Throttle {

    private int id;
    private ArrayList<Regime> lRegime = new ArrayList();

    public Throttle() {
    }

    public Throttle(int id, ArrayList<Regime> lRegime) {
        this.lRegime = lRegime;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the lRegime
     */
    public ArrayList<Regime> getlRegime() {
        return lRegime;
    }

    /**
     * @param lRegime the lRegime to set
     */
    public void setlRegime(ArrayList<Regime> lRegime) {
        this.lRegime = lRegime;
    }
    
    public void addRegime(Regime r){
        this.lRegime.add(r);
    }
}
