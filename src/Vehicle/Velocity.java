package Vehicle;

/**
 *
 * @author Luís Rocha
 */
public class Velocity {

    private String segment_type;
    private double limit;

    public Velocity() {

    }

    public Velocity(String segment_Type, double limit) {
        this.limit = limit;
        this.segment_type = segment_type;
    }

    public String getSegment_type() {
        return segment_type;
    }

    public void setSegment_type(String segment_type) {
        this.segment_type = segment_type;
    }

    public double getLimit() {
        return limit;
    }

    public void setLimit(double limit) {
        this.limit = limit;
    }
}
