/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vehicle;

/**
 *
 * @author Luís Rocha
 */
public class Regime {

    private int torque;
    private int rpm_low;
    private int rpm_high;
    private double SFC;

    public Regime() {
    }

    public Regime(int torque, int rpm_low, int rpm_high, double SFC) {
        this.torque = torque;
        this.rpm_low = rpm_low;
        this.rpm_high = rpm_high;
        this.SFC = SFC;

    }

    public Regime(int torque, int rpm_low, int rpm_high) {
        this.torque = torque;
        this.rpm_low = rpm_low;
        this.rpm_high = rpm_high;

    }

    public int getTorque() {
        return torque;
    }

    public void setTorque(int torque) {
        this.torque = torque;
    }

    public int getRpm_low() {
        return rpm_low;
    }

    public void setRpm_low(int rpm_low) {
        this.rpm_low = rpm_low;
    }

    public int getRpm_high() {
        return rpm_high;
    }

    public void setRpm_high(int rpm_high) {
        this.rpm_high = rpm_high;
    }

    public double getSFC() {
        return SFC;
    }

    public void setSFC(double SFC) {
        this.SFC = SFC;
    }

}
