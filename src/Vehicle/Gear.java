package Vehicle;

/**
 *
 * @author Luís Rocha
 */
public class Gear {

    private int id;
    private double ratio;

    public Gear() {
    }

    public Gear(int id, double ratio) {
        this.id = id;
        this.ratio = ratio;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getRatio() {
        return ratio;
    }

    public void setRatio(double ratio) {
        this.ratio = ratio;
    }

}
