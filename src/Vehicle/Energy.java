package Vehicle;

import java.util.ArrayList;

/**
 *
 * @author Luís Rocha
 */
public class Energy {

    private int min_rpm;
    private int max_rpm;
    private double final_drive_ratio;
    private ArrayList<Gear> lGear = new ArrayList();
    private ArrayList<Throttle> lThrottle = new ArrayList();
    private double energy_regeneration_ratio;

    public Energy() {
    }

    public Energy(int min_rpm, int max_rpm, double final_drive_ratio,
            ArrayList<Gear> lGear, ArrayList<Throttle> lThrottle) {
        this.min_rpm = min_rpm;
        this.max_rpm = max_rpm;
        this.final_drive_ratio = final_drive_ratio;
        this.lGear = lGear;
        this.lThrottle = lThrottle;
    }

    public Energy(int torque, int rpm, double consumption, int min_rpm, int max_rpm,
            double final_drive_ratio, double energy_regeneration_ratio,
            ArrayList<Gear> lGear, ArrayList<Throttle> lThrottle) {

        this.min_rpm = min_rpm;
        this.max_rpm = max_rpm;
        this.final_drive_ratio = final_drive_ratio;
        this.energy_regeneration_ratio = energy_regeneration_ratio;
        this.lGear = lGear;
        this.lThrottle = lThrottle;
    }

    public int getMin_rpm() {
        return min_rpm;
    }

    public void setMin_rpm(int min_rpm) {
        this.min_rpm = min_rpm;
    }

    public int getMax_rpm() {
        return max_rpm;
    }

    public void setMax_rpm(int max_rpm) {
        this.max_rpm = max_rpm;
    }

    public double getFinal_drive_ratio() {
        return final_drive_ratio;
    }

    public void setFinal_drive_ratio(double final_drive_ratio) {
        this.final_drive_ratio = final_drive_ratio;
    }

    public ArrayList<Gear> getlGear() {
        return lGear;
    }

    public void setlGear(ArrayList<Gear> lGear) {
        this.lGear = lGear;
    }

    public ArrayList<Throttle> getlThrottle() {
        return lThrottle;
    }

    public void setlThrottle(ArrayList<Throttle> lThrottle) {
        this.lThrottle = lThrottle;
    }

    public Throttle getThrottle(int id) {
        for (Throttle t : lThrottle) {
            if (t.getId() == id) {
                return t;
            }
        }
        return null;
    }

    public Gear getGear(int id) {
        for (Gear g : lGear) {
            if (g.getId() == id) {
                return g;
            }
        }
        return null;
    }

    public int GetMaxGear() {
        int gear = 1;

        for (Gear g : lGear) {
            if (g.getId() > gear) {
                gear = g.getId();
            }
        }
        return gear;
    }

    public double getEnergy_regeneration_ratio() {
        return energy_regeneration_ratio;
    }

    public void setEnergy_regeneration_ratio(double energy_regeneration_ratio) {
        this.energy_regeneration_ratio = energy_regeneration_ratio;
    }

}
