/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BaseDados;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import road.Section;
import road.Segment;

/**
 *
 * @author Utilizador
 */
public class Selects {

    private Connection Con;

    public Selects(Connection Con) {
        this.Con = Con;
    }

    Statement stmt = null;

    public ArrayList<Segment> SelectSectionSegment(Section sec) throws SQLException {

        ArrayList<Segment> lSegts = new ArrayList();

        System.out.println("Creating statement...");
        stmt = Con.createStatement();

        String sql = "select idsegment, segmentIndex, initialheight, slope, length, maximumvelocity,"
                + " minimumvelocity, sectionidsection, maximumnrvehicles from section st,"
                + " segment sg where st.IDSECTION = sg.SECTIONIDSECTION and st.IDSECTION = "+sec.getIndex()+";";
        ResultSet rs = stmt.executeQuery(sql);
        //STEP 5: Extract data from result set
        while (rs.next()) {
            //Retrieve by column name
            Segment s = new Segment();
            s.setId_segment(rs.getInt("segmentIndex"));
            s.setHeight(rs.getDouble("inicialHeight"));
            lSegts.add(s);
        }

        return lSegts;
    }
}
