/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import road.Junction;
import road.Section;

/**
 *
 * @author Yahkiller
 */
public class Utils {

    //VER O Q FAZER NO CASO DE HAVER VARIOS... TALVEC FAZER OUTRO MAP PA DISTANCIAS
    public static ArrayList<Junction> getMinPath(HashMap<ArrayList<Junction>, Double> map) {
        Map.Entry<ArrayList<Junction>, Double> min = null;
        for (Map.Entry<ArrayList<Junction>, Double> entry : map.entrySet()) {
            if (min == null || min.getValue() > entry.getValue()) {
                min = entry;
            }
        }
        return min.getKey();
    }

    public static Section getMinSection(HashMap<Section, Double> map) {
        Map.Entry<Section, Double> min = null;
        for (Map.Entry<Section, Double> entry : map.entrySet()) {
            if (min == null || min.getValue() > entry.getValue()) {
                min = entry;
            }
        }
        return min.getKey();
    }

    public static String pathToString(ArrayList<Junction> lNodes) {

        String path = "";
        for (Junction j : lNodes) {
            path += j.getKey() + " ";
        }
        return path;
    }

    public static ArrayList<String> mapToString(HashMap<ArrayList<Junction>, Double> map) {
        ArrayList<String> s = new ArrayList<String>();
        DecimalFormat df = new DecimalFormat("#.##");

        for (ArrayList<Junction> p : map.keySet()) {
            String path = Utils.pathToString(p);
            path += ": " + df.format(map.get(p));
            s.add(path);
        }
        return s;
    }

    public static ArrayList<String> mapToStringCons(HashMap<ArrayList<Junction>, Double> map) {
        ArrayList<String> s = new ArrayList<String>();
        DecimalFormat df = new DecimalFormat("#.##");

        for (ArrayList<Junction> p : map.keySet()) {
            String path = Utils.pathToString(p);
            path += ": " + df.format(map.get(p)) + "l";
            s.add(path);
        }
        return s;
    }

    public static double fromKmToDouble(String km) {
        String[] parts = km.split(" ");
        return Double.parseDouble(parts[0]);
    }

    public static double intoDegrees(String slope) {
        slope = slope.substring(0, slope.length() - 1);
        double perc = Double.parseDouble(slope);
        double ang = Math.atan(perc / 100);
        return ang;
    }

    public static double withoutPercent(String slope) {
        slope = slope.substring(0, slope.length() - 1);
        double perc = Double.parseDouble(slope);
        return perc;
    }

    public static double fromPerMinute(String min) {
        String[] parts = min.split(" ");
        return Double.parseDouble(parts[0]);
    }

    public static int withoutBeginningChar(String str) {
        str = str.substring(1, str.length());
        int res = Integer.parseInt(str);
        return res;
    }

    public static double converterJtoKWh(double j) {
        double kwh = 0.0;
        kwh = j * 2.7777777778 * Math.pow(10, -7);
        return kwh;

    }

}
