package Utils;

import Model.Project;
import Model.Simulation;
import Model.SimulationRun;
import Vehicle.Vehicle;
import java.util.*;
import javax.swing.*;
import road.Section;

/**
 *
 * @author Yahkiller
 */
public class MyListModel extends AbstractListModel {

    private List<String> data = new LinkedList<>();

    public MyListModel(String[] vec) {

        for (int i = 0; i < (vec.length); i++) {
            data.add(vec[i]);
        }

    }

    @Override
    public int getSize() {
        return getData().size();
    }

    @Override
    public Object getElementAt(int index) {
        return getData().get(index);
    }

    public void addElement(String x) {
        getData().add(x);
        fireContentsChanged(this, 0, getData().size());
    }

    /**
     * @return the data
     */
    public List<String> getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(List<String> data) {
        this.data = data;
    }

    public boolean hasData(String st) {
        return this.data.contains(st);
    }

    public static String[] listaStringProject(List<Project> lp) {

        String[] vec = new String[lp.size()];
        for (int i = 0; i < lp.size(); i++) {
            vec[i] = lp.get(i).getName();
        }

        return vec;
    }

    public static String[] listaStringRuns(List<SimulationRun> lp) {

        String[] vec = new String[lp.size()];
        for (int i = 0; i < lp.size(); i++) {
            vec[i] = lp.get(i).getName();
        }

        return vec;
    }

    public static String[] listaStringSimulation(List<Simulation> lp) {

        String[] vec = new String[lp.size()];
        for (int i = 0; i < lp.size(); i++) {
            vec[i] = lp.get(i).getId();
        }

        return vec;
    }

    public static String[] listaString(ArrayList<String> ls) {
        String[] vec = new String[ls.size()];
        for (int i = 0; i < ls.size(); i++) {
            vec[i] = ls.get(i);
        }

        return vec;
    }

    public static String[] listaVeiculos(ArrayList<Vehicle> vs) {
        String[] vec = new String[vs.size()];
        for (int i = 0; i < vs.size(); i++) {
            vec[i] = vs.get(i).getName();
        }

        return vec;
    }

    public static String[] listaSections(ArrayList<Section> vs) {
        String[] vec = new String[vs.size()];
        for (int i = 0; i < vs.size(); i++) {
            vec[i] = vs.get(i).getId_road();
        }

        return vec;
    }

}
