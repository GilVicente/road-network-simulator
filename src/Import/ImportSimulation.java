package Import;

import Model.Simulation;
import Model.TrafficPattern;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Luís Rocha
 */
public class ImportSimulation {

    private DocumentBuilderFactory dbFactory;
    private DocumentBuilder dBuilder;
    private Document doc;

    private ArrayList<Simulation> lSimulation;

    public ImportSimulation(String pathFile) throws ParserConfigurationException, SAXException, IOException {
        lSimulation = new ArrayList();
        dbFactory = DocumentBuilderFactory.newInstance();
        dBuilder = dbFactory.newDocumentBuilder();
        doc = dBuilder.parse(new File(pathFile));

    }

    public void importAll() {
        ImportSimulation();
        ImportTrafficPattern();
    }

    public void ImportSimulation() {
        NodeList nList = doc.getElementsByTagName("Simulation");

        for (int i = 0; i < nList.getLength(); i++) {
            Node node = nList.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elm = (Element) node;
                Simulation s = new Simulation();
                s.setId(elm.getAttribute("id"));
                s.setDescription(elm.getAttribute("description"));

                lSimulation.add(s);
            }
        }
    }

    public ArrayList<TrafficPattern> ImportTrafficPattern() {
        ArrayList<TrafficPattern> tp = new ArrayList();
        NodeList nList = doc.getElementsByTagName("traffic_pattern");

        for (int i = 0; i < nList.getLength(); i++) {
            Node node = nList.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elm = (Element) node;

                Element elm2 = (Element) elm.getParentNode().getParentNode();
                String parent = elm2.getAttribute("id");

                TrafficPattern t = new TrafficPattern();
                t.setBegin(elm.getAttribute("begin"));
                t.setEnd(elm.getAttribute("end"));
                t.setArrival_rate(elm.getElementsByTagName("arrival_rate").item(0).getTextContent());
                t.setlVeh(elm.getElementsByTagName("vehicle").item(0).getTextContent());

                tp.add(t);
            }
        }
        return tp;
    }

    public ArrayList<Simulation> getlSimulation() {
        return lSimulation;
    }

    public void setlSimulation(ArrayList<Simulation> lSimulation) {
        this.lSimulation = lSimulation;
    }

    public void addTrafficPattern(String parent, TrafficPattern t) {
        for (Simulation s : lSimulation) {
            if (s.getId().equalsIgnoreCase(parent)) {
                s.addTrafficPattern(t);
            }
        }
    }
}
