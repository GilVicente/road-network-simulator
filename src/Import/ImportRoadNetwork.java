package Import;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.UserDataHandler;
import org.xml.sax.SAXException;
import road.RoadNetwork;
import road.Junction;
import road.Road;
import road.Section;
import road.Segment;

/**
 *
 * @author Luís Rocha
 */
public class ImportRoadNetwork {
    
    private DocumentBuilderFactory dbFactory;
    private DocumentBuilder dBuilder;
    private Document doc;
    private RoadNetwork roadNetwork;
    private int nSection = 0;
    
    public ImportRoadNetwork(RoadNetwork roadNetwork, String pathFile) throws ParserConfigurationException, SAXException, IOException {
        this.roadNetwork = roadNetwork;
        dbFactory = DocumentBuilderFactory.newInstance();
        dBuilder = dbFactory.newDocumentBuilder();
        doc = dBuilder.parse(new File(pathFile));
        doc.getDocumentElement().normalize();
        importAll(this.roadNetwork);
    }
    
    public void importAll(RoadNetwork roadNetwork) {
        importJunction();
        importRoadSection();
        importSegment();
    }
    
    public void importJunction() {
        ArrayList<Junction> lJunction = new ArrayList();
        NodeList nList = doc.getElementsByTagName("node");
        for (int i = 0; i < nList.getLength(); i++) {
            Node node = nList.item(i);
            
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elm = (Element) node;
                Junction n = new Junction();
                n.setKey(elm.getAttribute("id"));
                lJunction.add(n);
            }
        }
        roadNetwork.setlNodes(lJunction);
    }
    
    public void importRoadSection() {
        ArrayList<Section> lSections = new ArrayList();
        NodeList nList = doc.getElementsByTagName("road_section");
        
        for (int i = 0; i < nList.getLength(); i++) {
            Node node = nList.item(i);
            
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elm = (Element) node;
                Section ma = new Section();
                ma.setIndex(nSection++);
                ma.setBeginning_node(elm.getAttribute("begin"));
                ma.setEnding_node(elm.getAttribute("end"));
                ma.setId_road(elm.getElementsByTagName("road").item(0).getTextContent());
                ma.setTypology(elm.getElementsByTagName("typology").item(0).getTextContent());
                ma.setDirection(elm.getElementsByTagName("direction").item(0).getTextContent());
                ma.setToll(Double.parseDouble(elm.getElementsByTagName("toll").item(0).getTextContent()));
                ma.setWind_angle(Double.parseDouble(elm.getElementsByTagName("wind_direction").item(0).getTextContent()));
                ma.setWind_speed((elm.getElementsByTagName("wind_speed").item(0).getTextContent()));
                lSections.add(ma);
            }
        }
        roadNetwork.setlSections(lSections);
    }
    
    public void importSegment() {
        
        ArrayList<Segment> lSegments = new ArrayList();
        NodeList nList = doc.getElementsByTagName("segment");
        
        for (int i = 0; i < nList.getLength(); i++) {
            Node node = nList.item(i);
            
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elm = (Element) node;
                Segment ma = new Segment();
                
                Element elm2 = (Element) elm.getParentNode().getParentNode();
                String a = elm2.getAttribute("begin");
                String b = elm2.getAttribute("end");
                String id = elm2.getElementsByTagName("road").item(0).getTextContent();
                int id_section = roadNetwork.getSectionsByRoad(a, b, id, this.roadNetwork.getlSections()).getId_section();
                ma.setId_segment(Integer.parseInt(elm.getAttribute("id")));
                ma.setHeight(Double.parseDouble(elm.getElementsByTagName("height").item(0).getTextContent()));
                ma.setSlope(elm.getElementsByTagName("slope").item(0).getTextContent());
                ma.setLenght(elm.getElementsByTagName("length").item(0).getTextContent());
                ma.setId_section(id_section);
                //           ma.setRolling_res_coef(Double.parseDouble(elm.getElementsByTagName("rrc").item(0).getTextContent()));
                ma.setMaxVel((elm.getElementsByTagName("max_velocity").item(0).getTextContent()));
                ma.setMinVel((elm.getElementsByTagName("min_velocity").item(0).getTextContent()));
                ma.setMax_vehicles(Integer.parseInt(elm.getElementsByTagName("number_vehicles").item(0).getTextContent()));
                
                roadNetwork.getSectionsByRoad(a, b, id, this.roadNetwork.getlSections()).addSegment(ma);
                
            }
        }
    }
    
    public RoadNetwork getRoadNetwork() {
        return this.roadNetwork;
    }
    
}
