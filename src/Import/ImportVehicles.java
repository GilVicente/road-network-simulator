package Import;

import Vehicle.Energy;
import Vehicle.Gear;
import Vehicle.Regime;
import Vehicle.Throttle;
import Vehicle.Vehicle;
import Vehicle.Velocity;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Luís Rocha
 */
public class ImportVehicles {

    private DocumentBuilderFactory dbFactory;
    private DocumentBuilder dBuilder;
    private Document doc;

    private int nSection = 0;
    private ArrayList<Vehicle> lVehicles;

    public ImportVehicles(String pathFile) throws ParserConfigurationException, SAXException, IOException {
        lVehicles = new ArrayList();
        dbFactory = DocumentBuilderFactory.newInstance();
        dBuilder = dbFactory.newDocumentBuilder();
        doc = dBuilder.parse(new File(pathFile));

    }

    public void importAll() {
        ImportVehicle();
        ImportVelocity();
        ImportEnergy();
        ImportGear();
        ImportThrottle();
        ImportRegime();
    }

    public void ImportVehicle() {
        NodeList nList = doc.getElementsByTagName("vehicle");
        int count = 1;
        for (int i = 0; i < nList.getLength(); i++) {
            Node node = nList.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elm = (Element) node;
                Vehicle v = new Vehicle();
                if (lVehicles.contains(elm.getAttribute("name"))) {
                    v.setName(elm.getAttribute("name") + "(" + count + ")");
                    v.setDescription(elm.getAttribute("description"));
                    v.setType(elm.getElementsByTagName("type").item(0).getTextContent());
                    v.setFrontal_area(Double.parseDouble(elm.getElementsByTagName("frontal_area").item(0).getTextContent()));
                    v.setMotor(elm.getElementsByTagName("motorization").item(0).getTextContent());
                    v.setFuel(elm.getElementsByTagName("fuel").item(0).getTextContent());
                    v.setMass((elm.getElementsByTagName("mass").item(0).getTextContent()));
                    v.setLoad((elm.getElementsByTagName("load").item(0).getTextContent()));
                    v.setRrc(Double.parseDouble(elm.getElementsByTagName("rrc").item(0).getTextContent()));
                    v.setWheel_size(Double.parseDouble(elm.getElementsByTagName("wheel_size").item(0).getTextContent()));
                    v.setDrag_coef(Double.parseDouble(elm.getElementsByTagName("drag").item(0).getTextContent()));
                    lVehicles.add(v);
                    count++;
                } else {
                    v.setName(elm.getAttribute("name"));
                    v.setDescription(elm.getAttribute("description"));
                    v.setType(elm.getElementsByTagName("type").item(0).getTextContent());
                    v.setFrontal_area(Double.parseDouble(elm.getElementsByTagName("frontal_area").item(0).getTextContent()));
                    v.setMotor(elm.getElementsByTagName("motorization").item(0).getTextContent());
                    v.setFuel(elm.getElementsByTagName("fuel").item(0).getTextContent());
                    v.setMass((elm.getElementsByTagName("mass").item(0).getTextContent()));
                    v.setLoad((elm.getElementsByTagName("load").item(0).getTextContent()));
                    v.setRrc(Double.parseDouble(elm.getElementsByTagName("rrc").item(0).getTextContent()));
                    v.setWheel_size(Double.parseDouble(elm.getElementsByTagName("wheel_size").item(0).getTextContent()));
                    v.setDrag_coef(Double.parseDouble(elm.getElementsByTagName("drag").item(0).getTextContent()));
                    lVehicles.add(v);
                }

            }
        }
    }

    public void insertVehicle(Vehicle v) {
        String type = v.getType();
        int idTipo = 0;
        if ("car".equalsIgnoreCase(type)) {
            idTipo = 1;
        } else if ("truck".equalsIgnoreCase(type)) {
            idTipo = 2;
        } else if ("byke".equalsIgnoreCase(type)) {
            idTipo = 3;
        }

        String fuelType = v.getFuel();
        int idFuelTipo = 0;
        if ("gasoline".equalsIgnoreCase(fuelType)) {
            idFuelTipo = 1;
        } else if ("diesel".equalsIgnoreCase(fuelType)) {
            idFuelTipo = 2;
        } else if ("Electricity".equalsIgnoreCase(fuelType)) {
            idFuelTipo = 3;
        } else if ("Hydrogen".equalsIgnoreCase(fuelType)) {
            idFuelTipo = 4;
        }

//        int valor = "select MAX v.IDvehicle from vehicle ";
//
//        String insert = "INSERT INTO VEHICLE VALUES (" + IDvehicle + ", " + v.getName()", "+electric + ", " + idTipo + ", " + idFuelTipo + ",  " + v.getMass() + ", " + v.getLoad() + ", " + v.getDrag_coef() + ", " + v.getRrc() + ", " + v.getWheel_size() + ";)";
    }

    public void ImportGear() {
        NodeList nList = doc.getElementsByTagName("gear");

        for (int i = 0; i < nList.getLength(); i++) {
            Node node = nList.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elm = (Element) node;

                Element elm2 = (Element) elm.getParentNode().getParentNode().getParentNode();
                String parent = elm2.getAttribute("name");

                Gear g = new Gear();
                g.setId(Integer.parseInt(elm.getAttribute("id")));
                g.setRatio(Double.parseDouble(elm.getElementsByTagName("ratio").item(0).getTextContent()));
                addVehichleGear(parent, g);
            }
        }
    }

    public void ImportThrottle() {
        NodeList nList = doc.getElementsByTagName("throttle");

        for (int i = 0; i < nList.getLength(); i++) {
            Node node = nList.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elm = (Element) node;

                Element elm2 = (Element) elm.getParentNode().getParentNode().getParentNode();
                String parent = elm2.getAttribute("name");

                int id = Integer.parseInt(elm.getAttribute("id"));

                Throttle t = new Throttle();
                t.setId(id);
                addVehichleThrottle(parent, t);

            }
        }
    }

    public void ImportRegime() {
        NodeList nList = doc.getElementsByTagName("regime");

        for (int i = 0; i < nList.getLength(); i++) {
            Node node = nList.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elm = (Element) node;

                Element elm2 = (Element) elm.getParentNode();
                int id = Integer.parseInt(elm2.getAttribute("id"));

                Element elm3 = (Element) elm.getParentNode().getParentNode().getParentNode().getParentNode();
                String parent = elm3.getAttribute("name");

                Regime r = new Regime();
                if (elm3.getElementsByTagName("motorization").item(0).getTextContent().equalsIgnoreCase("combustion")) {
                    r.setTorque(Integer.parseInt(elm.getElementsByTagName("torque").item(0).getTextContent()));
                    r.setRpm_high(Integer.parseInt(elm.getElementsByTagName("rpm_high").item(0).getTextContent()));
                    r.setRpm_low(Integer.parseInt(elm.getElementsByTagName("rpm_low").item(0).getTextContent()));
                    r.setSFC(Double.parseDouble(elm.getElementsByTagName("SFC").item(0).getTextContent()));
                    addVehichleRegime(parent, r, id);
                } else if (elm3.getElementsByTagName("motorization").item(0).getTextContent().equalsIgnoreCase("electric")) {
                    r.setTorque(Integer.parseInt(elm.getElementsByTagName("torque").item(0).getTextContent()));
                    r.setRpm_high(Integer.parseInt(elm.getElementsByTagName("rpm_high").item(0).getTextContent()));
                    r.setRpm_low(Integer.parseInt(elm.getElementsByTagName("rpm_low").item(0).getTextContent()));
                    addVehichleRegime(parent, r, id);
                }
            }
        }
    }

    public void ImportEnergy() {

        NodeList nList = doc.getElementsByTagName("energy");
        for (int i = 0; i < nList.getLength(); i++) {
            Node node = nList.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elm = (Element) node;
                Element elm2 = (Element) elm.getParentNode();
                String parent = elm2.getAttribute("name");

                Energy e = new Energy();
                if (elm2.getElementsByTagName("motorization").item(0).getTextContent().equalsIgnoreCase("combustion")) {
                    e.setMin_rpm(Integer.parseInt(elm.getElementsByTagName("min_rpm").item(0).getTextContent()));
                    e.setMax_rpm(Integer.parseInt(elm.getElementsByTagName("max_rpm").item(0).getTextContent()));
                    e.setFinal_drive_ratio(Double.parseDouble(elm.getElementsByTagName("final_drive_ratio").item(0).getTextContent()));
                    addVehichleEnergy(parent, e);
                } else if (elm2.getElementsByTagName("motorization").item(0).getTextContent().equalsIgnoreCase("electric")) {
                    e.setMin_rpm(Integer.parseInt(elm.getElementsByTagName("min_rpm").item(0).getTextContent()));
                    e.setMax_rpm(Integer.parseInt(elm.getElementsByTagName("max_rpm").item(0).getTextContent()));
                    e.setFinal_drive_ratio(Double.parseDouble(elm.getElementsByTagName("final_drive_ratio").item(0).getTextContent()));
                    e.setEnergy_regeneration_ratio((Double.parseDouble(elm.getElementsByTagName("energy_regeneration_ratio").item(0).getTextContent())));
                    addVehichleEnergy(parent, e);
                }
            }
        }
    }

    public void ImportVelocity() {
        NodeList nList = doc.getElementsByTagName("velocity_limit");

        for (int i = 0; i < nList.getLength(); i++) {
            Node node = nList.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elm = (Element) node;

                Element elm2 = (Element) elm.getParentNode().getParentNode();
                String parent = elm2.getAttribute("name");

                Velocity e = new Velocity();
                e.setLimit(Double.parseDouble(elm.getElementsByTagName("limit").item(0).getTextContent()));
                e.setSegment_type((elm.getElementsByTagName("segment_type").item(0).getTextContent()));
                addVehichleVelocity(parent, e);

            }
        }
    }

    public void addVehichleGear(String vehichleName, Gear g) {
        for (Vehicle s : lVehicles) {
            if (s.getName().equalsIgnoreCase(vehichleName)) {
                s.addGear(g);
            }
        }
    }

    public void addVehichleEnergy(String vehichleName, Energy e) {
        for (Vehicle s : lVehicles) {
            if (s.getName().equalsIgnoreCase(vehichleName)) {
                s.setEnergy(e);
            }
        }
    }

    public void addVehichleVelocity(String vehichleName, Velocity v) {
        for (Vehicle s : lVehicles) {
            if (s.getName().equalsIgnoreCase(vehichleName)) {
                s.addVelocity(v);
            }
        }
    }

    public void addVehichleRegime(String vehichleName, Regime r, int id) {
        for (Vehicle s : lVehicles) {
            if (s.getName().equalsIgnoreCase(vehichleName)) {
                for (Throttle t : s.getEnergy().getlThrottle()) {
                    if (t.getId() == id) {
                        t.addRegime(r);
                    }
                }

            }
        }
    }

    public void addVehichleThrottle(String vehichleName, Throttle t) {
        for (Vehicle s : lVehicles) {
            if (s.getName().equalsIgnoreCase(vehichleName)) {
                s.addThrottle(t);
            }
        }
    }

    public boolean hasVehicleThrottle(String vehichleName, int id) {
        for (Vehicle s : lVehicles) {
            if (s.getName().equalsIgnoreCase(vehichleName)) {
                for (Throttle temp : s.getEnergy().getlThrottle()) {
                    if (id == temp.getId()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public Throttle getThrottle(int id, String vehichleName) {
        for (Vehicle s : lVehicles) {
            if (s.getName().equalsIgnoreCase(vehichleName)) {
                for (Throttle temp : s.getEnergy().getlThrottle()) {
                    if (temp.getId() == temp.getId()) {
                        return temp;
                    }
                }
            }
        }
        return null;
    }

    public ArrayList<Vehicle> getlVehicles() {
        return lVehicles;
    }

    public void setlVehicles(ArrayList<Vehicle> lVehicles) {
        this.lVehicles = lVehicles;
    }

}
