/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProjectUI;

import Import.ImportRoadNetwork;
import Import.ImportVehicles;
import Model.Project;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import road.RoadNetwork;
import road.Section;

/**
 *
 * @author Luís Rocha
 */
public class AddRoad extends javax.swing.JFrame {

    private File rod_dir;
    private Project created;
    private MainUI main;

    /**
     * Creates new form AddRoad
     */
    public AddRoad(MainUI main) {
        this.main = main;
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 3, dim.height / 4);
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Vehicles = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        Add = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        Vehicles.setText("Choose Import File");
        Vehicles.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                VehiclesActionPerformed(evt);
            }
        });

        jLabel5.setText("Roads");

        Add.setText("Add");
        Add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AddActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Add Roads");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(Add)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(Vehicles))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Vehicles)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Add)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void VehiclesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_VehiclesActionPerformed
        JFileChooser fc = new JFileChooser();

        fc.setCurrentDirectory(new File("."));
        FileNameExtensionFilter filter = new FileNameExtensionFilter("XML file", "xml");
        fc.addChoosableFileFilter(filter);
        fc.setFileFilter(filter);
        fc.setAcceptAllFileFilterUsed(false);
        fc.showOpenDialog(fc);
        fc.setVisible(true);
        rod_dir = fc.getSelectedFile();
    }//GEN-LAST:event_VehiclesActionPerformed

    private void AddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AddActionPerformed
        if (rod_dir == null) {
            final JPanel panel = new JPanel();
            JOptionPane.showMessageDialog(panel, "Road Network file not selected!", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            try {
                RoadNetwork n = new RoadNetwork();
                final JPanel panel = new JPanel();

                ImportRoadNetwork vc = new ImportRoadNetwork(
                        n, rod_dir.getAbsolutePath());
                vc.importAll(n);
                n = vc.getRoadNetwork();
                ArrayList<Section> m = n.getlSections();

               for(int i = 0; i < m.size(); i++){
                   Section sec = m.get(i);
                    if (main.getActiveProj().addOtherSection(sec) == false) {
                        JOptionPane.showMessageDialog(panel, "There aren't "
                                + "different roads to add", "Error", JOptionPane.ERROR_MESSAGE);

                    }
                }
                JOptionPane.showMessageDialog(panel, "Road Added", "Message", JOptionPane.INFORMATION_MESSAGE);
            } catch (ParserConfigurationException ex) {
                Logger.getLogger(CreateProjectUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SAXException ex) {
                Logger.getLogger(CreateProjectUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(CreateProjectUI.class.getName()).log(Level.SEVERE, null, ex);
            }

        }


    }//GEN-LAST:event_AddActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Add;
    private javax.swing.JButton Vehicles;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel5;
    // End of variables declaration//GEN-END:variables
}
