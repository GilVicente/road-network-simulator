/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;

/**
 *
 * @author Yahkiller
 */
public class ProjectController {

    private ArrayList<Project> lProjects;

    public ProjectController() {
        lProjects = new ArrayList();
    }

    public void addProject(Project p) {
        getlProjects().add(p);
    }

    public Project getProject(String name) {
        for (Project p : getlProjects()) {
            if (p.getName().equalsIgnoreCase(name)) {
                return p;
            }
        }
        return null;
    }


    /**
     * @return the lProjects
     */
    public ArrayList<Project> getlProjects() {
        return lProjects;
    }

    /**
     * @param lProjects the lProjects to set
     */
    public void setlProjects(ArrayList<Project> lProjects) {
        this.lProjects = lProjects;
    }

    public void remove(int index) {
        this.lProjects.remove(index);
    }

    public boolean hasProject(String teste) {
        boolean flag = false;
        for (Project p : lProjects) {
            if (teste.equalsIgnoreCase(p.getName())) {
                flag = true;
            }
        }
        return flag;
    }

    public boolean hasOtherProject(Project activ, String teste) {
        boolean flag = false;
        for (Project p : lProjects) {
            if (teste.equalsIgnoreCase(p.getName())&&!p.equals(activ)) {
                flag = true;
            }
        }
        return flag;
    }
}
