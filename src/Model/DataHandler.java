package Model;

import Import.ImportRoadNetwork;
import SimulationUI.RunSimulatorController;
import Vehicle.Energy;
import Vehicle.Gear;
import Vehicle.Regime;
import Vehicle.Throttle;
import Vehicle.Time;
import Vehicle.Vehicle;
import Vehicle.Velocity;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javax.swing.text.html.HTML.Attribute.ID;
import javax.xml.parsers.ParserConfigurationException;
import oracle.jdbc.pool.OracleDataSource;
import org.xml.sax.SAXException;
import road.Junction;
import road.RoadNetwork;
import road.Section;
import road.Segment;

/**
 *
 * @author Utilizador
 */
public class DataHandler {

    /**
     * O URL da BD.
     */
    private String jdbcUrl;

    /**
     * O nome de utilizador da BD.
     */
    private String username;

    /**
     * A password de utilizador da BD.
     */
    private String password;

    /**
     * A ligação à BD.
     */
    private Connection connection;

    OracleDataSource ds;
    ArrayList<Regime> g = new ArrayList();
    ArrayList<Velocity> v = new ArrayList();

    /**
     * Constrói uma instância de "DataHandler" recebendo, por parâmetro, o URL
     * da BD e as credenciais do utilizador.
     *
     * @param jdbcUrl o URL da BD.
     * @param username o nome do utilizador.
     * @param password a password do utilizador.
     */
    public DataHandler(String jdbcUrl, String username, String password) {
        this.jdbcUrl = jdbcUrl;
        this.username = username;
        this.password = password;
        connection = null;

    }

    /**
     * Estabelece a ligação à BD.
     */
    public void openConnection() throws SQLException {
        OracleDataSource ds = new OracleDataSource();
        ds.setURL(jdbcUrl);
        connection = ds.getConnection(username, password);
    }

    /**
     * @return the connection
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * Fecha os objetos "Connection", e retorna uma mensagem de erro se alguma
     * dessas operações não for bem sucedida. Caso contrário retorna uma
     * "string" vazia.
     */
    public String closeAll() {
        StringBuilder message = new StringBuilder("");

        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException ex) {
                message.append(ex.getMessage());
                message.append("\n");
            }
            //connection = null;
        }
        return message.toString();
    }

    /**
     * Save Projects in database
     *
     * @param name
     * @param description
     * @param rn
     * @param project
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public int saveProject(String name, String description, RoadNetwork rn, Project project) throws SQLException, ParserConfigurationException, SAXException, IOException {
        int IDProject = 0;
        String getID = "SELECT MAX(P.IDPROJECT)" + "FROM PROJECT P";
        try {
            Statement statement = connection.createStatement();
            ResultSet IDe = statement.executeQuery(getID);
            IDe.next();
            int nID = IDe.getInt(1);
            if (nID != 0) {
                IDProject = nID + 1;
            } else {
                IDProject = 1;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        String insertTableSQL = "INSERT INTO PROJECT"
                + "(IDPROJECT,NAME,DESCRIPTION)" + "VALUES" + "("
                + IDProject + ",'" + name + "','" + description + "')";

        try {

            Statement statement = connection.createStatement();
            statement.executeUpdate(insertTableSQL);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        saveNetwork(rn, IDProject);

        //saveVehicles(project);
        saveVehicles(project, IDProject);
        return IDProject;
    }

    /**
     * save vehicles in database
     *
     * @param project
     * @param IDProject
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public void saveVehicles(Project project, int IDProject) throws SQLException, ParserConfigurationException, SAXException, IOException {

        ArrayList<Vehicle> lVehicles = project.getListVehicles();

        for (Vehicle v : lVehicles) {
            //SaveVehicle -- 4
            saveVehicle(IDProject, v);
        }
    }

    /**
     * saveNetwork in database
     *
     * @param rn
     * @param IDProject
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public void saveNetwork(RoadNetwork rn, int IDProject) throws SQLException, ParserConfigurationException, SAXException, IOException {

        ArrayList<Junction> lNodes = rn.getlNodes();
        ArrayList<Section> lSections = rn.getlSections();

        //SaveNodes     -- 1
        for (Junction n : lNodes) {
            saveNode(n, IDProject);
        }

        //SaveSections  -- 2
        for (Section s : lSections) {
            saveSection(s, IDProject);

        }

    }

    public void saveNode(Junction node, int IDProject) {
        int IDNode = 0;
        String getID = "SELECT MAX(N.IDnode) FROM Node N";
        try {
            Statement statement = connection.createStatement();
            ResultSet IDe = statement.executeQuery(getID);
            IDe.next();
            int nID = IDe.getInt(1);

            if (nID != 0) {
                IDNode = nID + 1;
            } else {
                IDNode = 1;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        String insertTableSQL = "INSERT INTO NODE"
                + "(IDNODE,DESCRICAO,PROJECTIDPROJECT)" + "VALUES" + "("
                + IDNode + ",'" + node.getKey() + "'," + IDProject + ")";

        try {

            Statement statement = connection.createStatement();
            statement.executeUpdate(insertTableSQL);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * save Segments in database
     *
     * @param segmentIndex
     * @param initialHeight
     * @param slope
     * @param length
     * @param maximunVelocity
     * @param minimumVelocity
     * @param IDSection
     * @param maximunNrVehicles
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public void saveSegment(int segmentIndex, int initialHeight,
            String slope, String length, String maximunVelocity,
            String minimumVelocity, int IDSection,
            int maximunNrVehicles) throws SQLException, ParserConfigurationException, SAXException, IOException {
        int IDSegment = 0;
        String getID = "SELECT MAX(S.IDSEGMENT) FROM Segment S";
        try {

            Statement statement = connection.createStatement();
            ResultSet IDe = statement.executeQuery(getID);
            IDe.next();
            int nID = IDe.getInt(1);

            if (nID != 0) {
                IDSegment = nID + 1;
            } else {
                IDSegment = 1;

                if (nID != 0) {
                    IDSegment = nID + 1;
                } else {
                    IDSegment = 1;
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        String insertTableSQL = "INSERT INTO SEGMENT"
                + "(IDSEGMENT, SEGMENTINDEX, INITIALHEIGHT, SLOPE, LENGTH,"
                + " MAXIMUMVELOCITY, MINIMUMVELOCITY,SECTIONIDSECTION, MAXIMUMNRVEHICLES)"
                + "VALUES (" + IDSegment + "," + segmentIndex + "," + initialHeight
                + ",'" + slope + "','" + length + "','" + maximunVelocity + "','" + minimumVelocity
                + "'," + IDSection + "," + maximunNrVehicles + ")";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(insertTableSQL);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * save Section in database
     *
     * @param s
     * @param IDProject
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public void saveSection(Section s, int IDProject) throws SQLException, ParserConfigurationException, SAXException, IOException {

        int IDSection = 0;
        String getID = "SELECT MAX(S.IDSection)" + "FROM Section S";
        try {
            Statement statement = connection.createStatement();
            ResultSet IDe = statement.executeQuery(getID);
            IDe.next();
            int nID = IDe.getInt(1);

            if (nID != 0) {
                IDSection = nID + 1;
            } else {
                IDSection = 1;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        String insertTableSQL = "INSERT INTO SECTION"
                + "(IDSECTION, BEGINNINGNODE, ENDINGNODE, DIRECTION, TOLL, "
                + "ANGLEWIND,SPEEDWIND, PROJECTIDPROJECT,TYPOLOGY,ROADNAME)"
                + "VALUES"
                + "(" + IDSection + ",'" + s.getBeginning_node() + "','" + s.getEnding_node()
                + "','" + s.getDirection() + "'," + (int) s.getToll() + "," + (int) s.getWind_angle()
                + ",'" + s.getWind_speed() + "'," + IDProject + ",'" + s.getTypology() + "','" + s.getRoad_name() + "')";

        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(insertTableSQL);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        //SaveSegments  -- 3
        ArrayList<Segment> lSegments = s.getlSegments();
        for (Segment seg : lSegments) {
            saveSegment(seg.getId_segment(), (int) seg.getHeight(), seg.getSlope(), seg.getLenght(), seg.getMaxVel(), seg.getMinVel(), IDSection, seg.getMax_vehicles());
        }
    }

    /**
     * save network in database
     *
     * @param rn
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public void saveNetwork(RoadNetwork rn) throws SQLException, ParserConfigurationException, SAXException, IOException {

        ArrayList<Junction> lNodes = rn.getlNodes();
        ArrayList<Section> lSections = rn.getlSections();
        ArrayList<Segment> lSegments = rn.getlSegments();

        System.out.println("\n" + lNodes.size());

    }

    /**
     * save road in database
     *
     * @param IDRoad
     * @param name
     * @param projectIDProject
     * @throws SQLException
     */
    public void saveRoad(String IDRoad, String name, int projectIDProject) throws SQLException {

        OracleDataSource ds = new OracleDataSource();
        ds.setURL(jdbcUrl);
        connection = ds.getConnection(username, password);

        String insertTableSQL = "INSERT INTO ROAD"
                + "(IDROAD, NAME, PROJECTIDPROJECT)" + "VALUES" + "('+" + IDRoad + "','" + name + "','" + projectIDProject + "')";
    }

    /**
     * save vehicle type in database
     *
     * @param designation
     * @throws SQLException
     */
    public void saveTypeVehicle(String designation) throws SQLException {
        int IDType = 0;
        String getID = "SELECT MAX(T.IDType)" + "FROM TYPEVEHICLE T";
        try {
            Statement statement = connection.createStatement();
            ResultSet IDe = statement.executeQuery(getID);
            IDe.next();
            int nID = IDe.getInt(1);
            System.out.println("saveTypeVehicle:" + nID);

            if (nID != 0) {
                IDType = nID + 1;
            } else {
                IDType = 1;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        String insertTableSQL = "INSERT INTO TYPEVEHICLE"
                + "(IDTYPE, DESIGNATION)" + "VALUES"
                + "('+" + IDType + "','" + designation + "')";

        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(insertTableSQL);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * save velocity limit in database
     *
     * @param v
     * @param vehicleID
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public void saveVelLimit(Vehicle v, int vehicleID) throws ParserConfigurationException, SAXException, IOException {
        int VelLimitID = 0;
        ArrayList<Velocity> lVelocity = v.getlVelocity();
        for (Velocity vel : lVelocity) {

            String getID = "SELECT MAX(S.VelLimitID)" + "FROM VelLimit S";
            try {
                Statement statement = connection.createStatement();
                ResultSet IDe = statement.executeQuery(getID);
                IDe.next();
                int nID = IDe.getInt(1);

                if (nID != 0) {
                    VelLimitID = nID + 1;
                } else {
                    VelLimitID = 1;
                }
                String insertTableSQL = "INSERT into VELLIMiT "
                        + "(VELLIMITID, SEGMENTTYPE, LIMIT ,VEHICLEIDVEHICLE) VALUES("
                        + VelLimitID + ",'" + vel.getSegment_type()
                        + "'," + (float) vel.getLimit() + "," + vehicleID + ")";
                Statement statement2 = connection.createStatement();
                statement2.executeUpdate(insertTableSQL);
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     * save energy in database
     *
     * @param minRpm
     * @param maxRpm
     * @param finalDriveRatio
     * @param vehicleIDvehicle
     * @param v
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public void saveEnergy(int minRpm, int maxRpm,
            float finalDriveRatio, int vehicleIDvehicle, Vehicle v) throws SQLException, ParserConfigurationException, SAXException, IOException {
        int EnergyID = 0;
        String getID = "SELECT MAX(S.EnergyID) FROM Energy S";
        try {
            Statement statement = connection.createStatement();
            ResultSet IDe = statement.executeQuery(getID);
            IDe.next();
            int nID = IDe.getInt(1);

            if (nID != 0) {
                EnergyID = nID + 1;
            } else {
                EnergyID = 1;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        String insertTableSQL = "INSERT INTO energy"
                + "(EnergyID,minRpm,maxRpm,finalDriveRatio, vehicleIDvehicle)" + "VALUES"
                + "(" + EnergyID + "," + minRpm + "," + maxRpm + "," + finalDriveRatio
                + "," + vehicleIDvehicle + ")";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(insertTableSQL);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        //saveGear -- 7
        saveGear(EnergyID, v);
        saveThrottle(v, EnergyID);
    }

    /**
     * Save Gear in database
     *
     * @param energyenergyID
     * @param v
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public void saveGear(int energyenergyID, Vehicle v) throws SQLException, ParserConfigurationException, SAXException, IOException {
        ArrayList<Gear> lGear = v.getEnergy().getlGear();
        for (Gear g : lGear) {
            String insertTableSQL = "INSERT INTO Gear"
                    + "(GearID,ratio,energyenergyID) VALUES "
                    + "(" + g.getId() + "," + g.getRatio() + "," + energyenergyID
                    + ")";
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(insertTableSQL);
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     * Save Throttle in database
     *
     * @param v
     * @param energyID
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public void saveThrottle(Vehicle v, int energyID) throws SQLException, ParserConfigurationException, SAXException, IOException {
        for (Throttle t : v.getEnergy().getlThrottle()) {
            String insertTableSQL = "INSERT INTO Throttle"
                    + "(throttleID,energyenergyID)" + "VALUES"
                    + "(" + t.getId() + "," + energyID
                    + ")";
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(insertTableSQL);

            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            saveRegime(v, t.getId(), energyID);
        }

        //saveNetwork(rn);
    }

    /**
     * Save regime in database
     *
     * @param v
     * @param throttleID
     * @param energyID
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public void saveRegime(Vehicle v, int throttleID, int energyID) throws SQLException, ParserConfigurationException, SAXException, IOException {
        int RegimeID = 0;

        ArrayList<Regime> lRegime = v.getEnergy().getThrottle(throttleID).getlRegime();
        for (Regime r : lRegime) {
            String getID = "SELECT MAX(R.RegimeID)" + "FROM Regime R";
            try {
                Statement statement = connection.createStatement();
                ResultSet IDe = statement.executeQuery(getID);
                IDe.next();
                int nID = IDe.getInt(1);

                if (nID != 0) {
                    RegimeID = nID + 1;
                } else {
                    RegimeID = 1;
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }

            String insertTableSQL = "INSERT INTO Regime"
                    + "(RegimeID,torque,rpm_low,rpm_high,sfc,throttlethrottleID, throttleenergyenergyID)" + "VALUES"
                    + "(" + RegimeID + "," + r.getTorque() + "," + r.getRpm_low()
                    + "," + r.getRpm_high() + "," + r.getSFC() + ","
                    + throttleID + "," + energyID + ")";
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(insertTableSQL);
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     * Save Vehicle in database
     *
     * @param projectIDProject
     * @param v
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public void saveVehicle(int projectIDProject, Vehicle v) throws SQLException,
            ParserConfigurationException, SAXException, IOException {

        int IDVehicle = 0;
        String getID = "SELECT MAX(R.IDVEHICLE)FROM Vehicle R";
        try {
            Statement statement = connection.createStatement();
            ResultSet IDe = statement.executeQuery(getID);
            IDe.next();
            int nID = IDe.getInt(1);

            if (nID != 0) {
                IDVehicle = nID + 1;
            } else {
                IDVehicle = 1;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        String insertTableSQL = "INSERT INTO VEHICLE "
                + "(IDVEHICLE, NAME, description ,TYPE, MASS, LOAD, DRAGCOEFFICIENT,"
                + " FUEL, RRC, WHEELSIZE, MOTORIZATION, FRONTALAREA, PROJECTIDPROJECT) "
                + "VALUES(" + IDVehicle + ",'" + v.getDescription() + "','" + v.getName() + "','" + v.getType() + "','"
                + v.getMass() + "','" + v.getMass() + "', " + v.getDrag_coef() + " ,'" + v.getFuel() + "', "
                + v.getRrc() + ", " + v.getWheel_size() + " ,'" + v.getMotor() + "', " + v.getFrontal_area()
                + " , " + projectIDProject + ")";

        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(insertTableSQL);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        try {
            saveEnergy(v.getEnergy().getMin_rpm(), v.getEnergy().getMax_rpm(),
                    (float) v.getEnergy().getFinal_drive_ratio(), IDVehicle, v);

        } catch (IOException ex) {
            Logger.getLogger(DataHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        //SaveEnergy -- 
        saveVelLimit(v, IDVehicle);

    }

    /**
     * Save simulation in database
     *
     * @param s
     * @param p
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public void saveSimulation(Simulation s, Project p) throws SQLException, ParserConfigurationException, SAXException, IOException {
        int IDsimulation = 0;
        int IDProj = 0;
        String getID = "SELECT MAX(R.IDsimulation)" + "FROM Simulation R";
        String getProjectID = "SELECT (R.idProject) FROM Project R where R.name ='" + p.getName() + "'";
        try {
            Statement statement = connection.createStatement();
            ResultSet IDe = statement.executeQuery(getID);
            IDe.next();

            int nID = IDe.getInt(1);

            if (nID != 0) {
                IDsimulation = nID + 1;
            } else {
                IDsimulation = 1;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        try {
            Statement statement = connection.createStatement();
            ResultSet IDP = statement.executeQuery(getProjectID);
            IDP.next();

            int nP = IDP.getInt(1);
            IDProj = nP;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        System.out.println("IDSIMULATION:" + IDsimulation);
        String insertTableSQL = "INSERT INTO SIMULATION"
                + "(IDSIMULATION,Name, DESCRIPTION, PROJECTIDPROJECT)"
                + "VALUES"
                + "(" + IDsimulation + ",'" + s.getId() + "','" + s.getDescription() + "'," + IDProj + ")";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(insertTableSQL);
            saveTrafficPattern(s, IDsimulation);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

    /**
     * Save TrafficPattern in database
     *
     * @param s
     * @param IDsimulation
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public void saveTrafficPattern(Simulation s, int IDsimulation) throws SQLException, ParserConfigurationException, SAXException, IOException {
        int traffic_patternID = 0;
        ArrayList<TrafficPattern> tp = s.getlPattern();
        for (TrafficPattern tp1 : tp) {

            String getID = "SELECT MAX(S.TRAFFIC_PARTTENID)" + "FROM Traffic_pattern S";
            try {
                Statement statement = connection.createStatement();
                ResultSet IDe = statement.executeQuery(getID);
                IDe.next();
                int nID = IDe.getInt(1);

                if (nID != 0) {
                    traffic_patternID = nID + 1;
                } else {
                    traffic_patternID = 1;
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            System.out.println("traffic_patternID" + traffic_patternID);
            String insertTableSQL = "INSERT INTO TRAFFIC_PATTERN"
                    + "(TRAFFIC_PARTTENID, VEHICLE, ARRIVAL_RATE, SIMULATIONIDSIMULATION, BEGIN_NODE, END_NODE)"
                    + "VALUES"
                    + "(" + traffic_patternID + ",'" + tp1.getlVeh() + "','" + tp1.getArrival_rate()
                    + "'," + IDsimulation + ",'" + tp1.getBegin() + "','" + tp1.getEnd() + "')";
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(insertTableSQL);
            } catch (SQLException e) {
                System.out.println(e.getMessage());

            }
        }

    }

    /**
     * Save Simulation Results in data basex
     *
     * @param runController
     * @param runSimulationName
     * @throws SQLException
     */
    public void saveSimulationResults(RunSimulatorController runController, int runSimulationID) throws SQLException {
        int simulationResults = 0;
        String getID = "SELECT MAX(S.SIMULATIONRESULTSID) FROM SIMULATIONRESULTS S";
        try {
            Statement statement = connection.createStatement();
            ResultSet IDe = statement.executeQuery(getID);
            IDe.next();
            int nID = IDe.getInt(1);

            if (nID != 0) {
                simulationResults = nID + 1;
            } else {
                simulationResults = 1;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        ArrayList<Vehicle> v = runController.getFinalVehicles();
        for (Vehicle v1 : v) {

            ArrayList<Time> in = v1.getInTime();
            ArrayList<Time> out = v1.getOutTime();
            for (Time in1 : in) {
                for (Time out1 : out) {
                    System.out.println("SimulationResults");
                    String insertTableSQL = "INSERT INTO SimulationResults"
                            + "(SIMULATIONRESULTSID, RUNSIMULATIONRUNSIMULATIONID,"
                            + " VEHICLEIDVEHICLE,SEGMENTID, INST_IN, INST_OUT)" + "VALUES"
                            + "(" + simulationResults + "," + runSimulationID + ",'"
                            + v1.getName() + "'," + in1.getS().getId_segment() + "," + in1.getTime() + ","
                            + out1.getTime() + ")";
                    try {
                        Statement statement = connection.createStatement();
                        statement.executeUpdate(insertTableSQL);
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }
                }
            }

        }

    }

    /**
     * Save Simulation drops in database
     *
     * @param runController
     * @param SimulationRun
     * @throws SQLException
     */
    public void saveSimulationDrop(RunSimulatorController runController,
            int SimulationRun) throws SQLException {
        int dropID = 0;
        String getID = "SELECT MAX(S.DROPID) FROM SimulationDrop S";

        Statement statement = connection.createStatement();
        ResultSet IDe = statement.executeQuery(getID);
        IDe.next();
        int nID = IDe.getInt(1);

        if (nID != 0) {
            dropID = nID + 1;
        } else {
            dropID = 1;
        }

        ArrayList<Vehicle> v = runController.getGarbage();
        for (Vehicle v1 : v) {
            ArrayList<Time> out = v1.getOutTime();
            for (Time out1 : out) {
                System.out.println("SimulationDrop");
                String insertTableSQL = "INSERT INTO SimulationDrop"
                        + "(DROPID, INSTANT_DROP, "
                        + "RUNSIMULATIONRUNSIMULATIONID, VEHICLENAME)" + "VALUES"
                        + "(" + dropID + "," + out1.getTime() + "," + SimulationRun + ",'" + v1.getName() + "')";
                try {

                    statement.executeUpdate(insertTableSQL);
                } catch (SQLException e) {
                    System.out.println(e.getMessage());
                }
            }

        }

    }

    /**
     * saveSimulaton in dataBase
     *
     * @param r
     * @param IDsimulation
     * @param alg
     * @param runController
     * @throws SQLException
     */
    public void saveSimulationRun(SimulationRun r, String IDsimulation, String alg,
            RunSimulatorController runController) throws SQLException {

        int IDSimulation = 0;
        int SimulationRun = 0;
        String getID = "SELECT MAX(S.RUNSIMULATIONID)" + "FROM runSimulation S";
        String getProjectID = "SELECT (R.IDSimulation) FROM Simulation R where R.name ='" + IDsimulation + "'";
        try {
            Statement statement = connection.createStatement();
            ResultSet IDe = statement.executeQuery(getID);
            IDe.next();
            int nID = IDe.getInt(1);

            if (nID != 0) {
                SimulationRun = nID + 1;
            } else {
                SimulationRun = 1;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        try {
            Statement statement = connection.createStatement();
            ResultSet IDP = statement.executeQuery(getProjectID);
            IDP.next();

            int nP = IDP.getInt(1);
            IDSimulation = nP;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("RunSimulation");
        String insertTableSQL = "INSERT INTO RunSimulation"
                + "(RUNSIMULATIONID, NAME, DURATION, TIMESTEP, ALGORITNAME, "
                + "SIMULATIONIDSIMULATION)" + "VALUES"
                + "(+" + SimulationRun + ",'" + r.getName() + "'," + r.getDuration()
                + "," + r.getTimeStep() + ",'" + alg + "',"
                + "'" + IDSimulation + "')";

        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(insertTableSQL);
            saveSimulationDrop(runController, SimulationRun);
            saveSimulationResults(runController, SimulationRun);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

    /**
     * deleteProject (not functional)
     *
     * @param p
     * @param name
     * @throws SQLException
     */
    public void deleteProject(ProjectController pc, String name, int index) throws SQLException {
       
        Statement stmt = connection.createStatement();
        boolean add = true;
        String query = "DELETE FROM Project CASCADE WHERE name='" + name + "'";

        ResultSet rs = stmt.executeQuery(query);
        

    }

    /**
     * getProjects from database
     *
     * @param p
     * @throws SQLException
     */
    public void getAllProjects(ProjectController p) throws SQLException {

        Statement stmt = getConnection().createStatement();

        String query = "SELECT * FROM Project";
        try {
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                int bdID = rs.getInt(1);
                String name = rs.getString(2);
                String description = rs.getString(3);

                Project pa = new Project(bdID, name, description);
                ArrayList<Project> lP = p.getlProjects();
                lP.add(pa);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Get Vehicles from databasex
     *
     * @param p
     * @throws SQLException
     */
    public ArrayList<Vehicle> getVehicle(Project p) throws SQLException {
        this.openConnection();
        Statement stmt = connection.createStatement();
        ArrayList<Vehicle> lP = new ArrayList();
        String query = "select * from vehicle where projectIDproject=" + p.bdID;
        try {
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                int IDVEHICLE = rs.getInt(1);
                String name = rs.getString(2);
                String type = rs.getString(3);
                String mass = rs.getString(4);
                String load = rs.getString(5);
                double drag = rs.getFloat(6);
                String fuel = rs.getString(7);
                double rrc = rs.getFloat(8);
                double wheel = rs.getFloat(9);
                String motor = rs.getString(10);
                double frontal = rs.getFloat(11);
                int projectIDProject = rs.getInt(12);
                String description = rs.getString(13);

                Energy e = getEnergy(IDVEHICLE);
                v = getVelocity(IDVEHICLE);

                Vehicle pa = new Vehicle(IDVEHICLE, name, description, type, motor, fuel, mass, load, rrc, wheel,
                        drag, frontal, e, v, g);

                lP.add(pa);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        p.setListVehicles(lP);
        return lP;
    }

    /**
     * getEnergy from database
     *
     * @param IDVEHICLE
     * @return
     * @throws SQLException
     */
    public Energy getEnergy(int IDVEHICLE) throws SQLException {

        Statement stmt = connection.createStatement();
        ArrayList<Gear> g = new ArrayList();
        ArrayList<Throttle> t = new ArrayList();
        Energy e;
        String query = "select * from energy where vehicleIDvehicle=" + IDVEHICLE;
        try {
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                int ENERGYID = rs.getInt(1);
                int MINRPM = rs.getInt(2);
                int MAXRPM = rs.getInt(3);
                double FINALDRIVERATIO = rs.getFloat(4);
                int VEHICLEIDVEHICLE = rs.getInt(5);
                g = getGear(ENERGYID);
                t = getThrottle(ENERGYID);
                e = new Energy(MINRPM, MAXRPM, FINALDRIVERATIO, g, t);
                return e;
            }
        } catch (SQLException m) {
            System.out.println(m.getMessage());
        }
        return null;
    }

    /**
     * getGear from database
     *
     * @param ENERGYID
     * @return
     * @throws SQLException
     */
    public ArrayList<Gear> getGear(int ENERGYID) throws SQLException {

        Statement stmt = connection.createStatement();

        ArrayList<Gear> g = new ArrayList();

        String query = "select * from gear where ENERGYENERGYID=" + ENERGYID;
        try {
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                int GEARID = rs.getInt(1);
                double RATIO = rs.getFloat(2);

                Gear e = new Gear(GEARID, RATIO);
                g.add(e);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return g;
    }

    /**
     * getThrottle from database
     *
     * @param ENERGYID
     * @return
     * @throws SQLException
     */
    public ArrayList<Throttle> getThrottle(int ENERGYID) throws SQLException {

        Statement stmt = connection.createStatement();

        ArrayList<Throttle> lThrottles = new ArrayList();
        String query = "select * from Throttle where ENERGYENERGYID=" + ENERGYID;
        try {
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                int THROTTLEID = rs.getInt(1);
                int ENERGYENERGYID = rs.getInt(2);

                ArrayList<Regime> r = getRegime(ENERGYENERGYID, THROTTLEID);
                Throttle t = new Throttle(THROTTLEID, r);
                lThrottles.add(t);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return lThrottles;
    }

    /**
     * getRegime from database
     *
     * @param ENERGYID
     * @return
     * @throws SQLException
     */
    public ArrayList<Regime> getRegime(int ENERGYID, int THROTTLEID) throws SQLException {

        Statement stmt = connection.createStatement();
        ArrayList<Regime> lRegimes = new ArrayList();
        String query = "select * from Regime where THROTTLEENERGYENERGYID=" + ENERGYID
                + " and THROTTLETHROTTLEID=" + THROTTLEID;
        try {
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                int REGIMEID = rs.getInt(1);
                int TORQUE = rs.getInt(2);
                int RPM_LOW = rs.getInt(3);
                int RPM_HIGH = rs.getInt(4);
                double SFC = rs.getFloat(5);

                Regime r = new Regime(TORQUE, RPM_LOW, RPM_HIGH, SFC);
                lRegimes.add(r);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return lRegimes;
    }

    /**
     * getVelocity from database
     *
     * @param IDVEHICLE
     * @return
     * @throws SQLException
     */
    public ArrayList<Velocity> getVelocity(int IDVEHICLE) throws SQLException {

        Statement stmt = connection.createStatement();

        String query = "select * from Vellimit where VEHICLEIDVEHICLE=" + IDVEHICLE;
        try {
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                String SEGMENTTYPE = rs.getString(2);
                int LIMIT = rs.getInt(3);

                Velocity e = new Velocity(SEGMENTTYPE, LIMIT);
                v.add(e);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return v;
    }

    public void editProject(String oldName, String oldDescription, String newName, String newDescription) throws SQLException {

        String editProject = "UPDATE PROJECT" + " SET NAME = REPLACE(NAME,'" + oldName + "','" + newName + "')"
                + "WHERE NAME LIKE '" + oldName + "'";

        String editProject2 = "UPDATE PROJECT" + " SET DESCRIPTION = REPLACE(DESCRIPTION,'" + oldDescription + "','" + newDescription + "')"
                + "WHERE NAME LIKE '" + newName + "'";

        Statement statement = connection.createStatement();
        statement.executeQuery(editProject);
        statement.executeQuery(editProject2);

//UPDATE PROJECT
//SET NAME = REPLACE(NAME, 'nome1st', 'novo')
//WHERE NAME LIKE 'nome1st';
//
//UPDATE PROJECT
//SET DESCRIPTION = REPLACE(DESCRIPTION, '1st', 'novo')
//WHERE NAME LIKE 'novo';
    }

    public void editSimulation(String oldName, String oldDescription, String newName, String newDescription) throws SQLException {

        String editSimulationName = "UPDATE SIMULATION" + " SET NAME = REPLACE(NAME,'" + oldName + "','" + newName + "')"
                + "WHERE NAME LIKE '" + oldName + "'";

        String editSimulationDescription = "UPDATE SIMULATION" + " SET DESCRIPTION = REPLACE(DESCRIPTION,'" + oldDescription + "','" + newDescription + "')"
                + "WHERE NAME LIKE '" + newName + "'";

        Statement statement = connection.createStatement();
        statement.executeQuery(editSimulationName);
        statement.executeQuery(editSimulationDescription);
    }

    /**
     * Get simulations from database
     *
     * @param s
     * @param p
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public void getSimulation(Project p, SimulationController s) throws SQLException, ParserConfigurationException, SAXException, IOException {

        Statement stmt = connection.createStatement();
        String query = "select * from simulation where ProjectIDProject=" + p.getBdID();

        try {
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                int IDSIMULATION = rs.getInt(1);
                String DESCRIPTION = rs.getString(2);
                String NAME = rs.getString(4);

                ArrayList<TrafficPattern> tp = getTrafficPattern(IDSIMULATION);
                Simulation r = new Simulation(NAME, DESCRIPTION, p.getName(), tp, p.getRn());

                s.getlSims().add(r);

            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

    /**
     * Get simulations from database
     *
     * @param s
     * @param p
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public ArrayList<TrafficPattern> getTrafficPattern(int s) throws SQLException, ParserConfigurationException, SAXException, IOException {

        Statement stmt = connection.createStatement();
        ArrayList<TrafficPattern> lTrafficPattern = new ArrayList();
        String query = "select * from Traffic_Pattern where SIMULATIONIDSIMULATION=" + s;
        try {
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                String VEHICLE = rs.getString(2);
                String ARRIVAL_RATE = rs.getString(3);
                String BEGIN_NODE = rs.getString(5);
                String END_NODE = rs.getString(6);

                TrafficPattern r = new TrafficPattern(BEGIN_NODE, END_NODE, VEHICLE, ARRIVAL_RATE);
                lTrafficPattern.add(r);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return lTrafficPattern;

    }

}
