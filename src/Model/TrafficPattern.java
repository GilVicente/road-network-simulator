package Model;

import Vehicle.Vehicle;
import org.w3c.dom.Node;

/**
 *
 * @author Luís Rocha
 */
public class TrafficPattern {

    private String begin;
    private String end;
    private String lVeh;
    private String arrival_rate;
    private double clock;

    public TrafficPattern() {
        this.clock=0;
    }

    public TrafficPattern(String begin, String end, String lVeh, String arrival_rate) {
        this.begin = begin;
        this.end = end;
        this.arrival_rate = arrival_rate;
        this.lVeh = lVeh;
        this.clock=0;
    }

    public String getBegin() {
        return begin;
    }

    public void setBegin(String begin) {
        this.begin = begin;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getlVeh() {
        return lVeh;
    }

    public void setlVeh(String lVeh) {
        this.lVeh = lVeh;
    }

    public String getArrival_rate() {
        return arrival_rate;
    }

    public void setArrival_rate(String arrival_rate) {
        this.arrival_rate = arrival_rate;
    }

    /**
     * @return the clock
     */
    public double getClock() {
        return clock;
    }

    /**
     * @param clock the clock to set
     */
    public void setClock(double clock) {
        this.clock = clock;
    }
}
