/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;

/**
 *
 * @author Yahkiller
 */
public class SimulationController {

    private ArrayList<Simulation> lSims;

    public SimulationController() {
        this.lSims = new ArrayList();
    }

    public void addSimulation(Simulation s) {
        this.getlSims().add(s);
    }

    public Simulation getSimulation(String name) {
        for (Simulation s : this.getlSims()) {
            if (s.getId().equalsIgnoreCase(name)) {
                return s;
            }
        }
        return null;
    }

    public boolean hasSimulation(String teste) {
        boolean flag = false;
        for (Simulation s : this.getlSims()) {
            if (teste.equalsIgnoreCase(s.getId())) {
                flag = true;
            }
        }
        return flag;
    }

    public boolean hasOtherSimulation(Simulation activ, String teste) {
        boolean flag = false;
        for (Simulation s : this.lSims) {
            if (teste.equalsIgnoreCase(s.getId()) && !s.equals(activ)) {
                flag = true;
            }
        }
        return flag;
    }

    public ArrayList<Simulation> getProjectSimulations(String projectName) {
        ArrayList<Simulation> ls = new ArrayList();
        for (Simulation s : lSims) {
            if (s.getProjectName().equalsIgnoreCase(projectName)) {
                ls.add(s);
            }

        }
        return ls;
    }

    public void remove(int index) {
        this.lSims.remove(index);
    }

    /**
     * @return the lSims
     */
    public ArrayList<Simulation> getlSims() {
        return lSims;
    }

    /**
     * @param lSims the lSims to set
     */
    public void setlSims(ArrayList<Simulation> lSims) {
        this.lSims = lSims;
    }

}
