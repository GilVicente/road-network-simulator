package Model;

import Import.ImportRoadNetwork;
import Import.ImportVehicles;
import Vehicle.Vehicle;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import road.RoadNetwork;
import road.Section;

/**
 *
 * @author Windows 8
 */
public class Project {

    private String name;
    private String description;
    private File rn_dir;
    private File veh_dir;
    private File sim_dir;
    private RoadNetwork rn;
    int bdID;

    private ArrayList<Vehicle> listVehicles;

    public Project(String name, String description, File rn_dir, File veh_dir) throws ParserConfigurationException, SAXException, IOException {
        this.name = name;
        this.description = description;
        this.rn_dir = rn_dir;
        this.veh_dir = veh_dir;
        this.rn = new RoadNetwork();
        ImportRoadNetwork IR = new ImportRoadNetwork(getRn(), rn_dir.getAbsolutePath());
        IR.importAll(rn);
        ImportVehicles IC = new ImportVehicles(veh_dir.getAbsolutePath());
        IC.importAll();
        listVehicles = IC.getlVehicles();
    }

    public Project(String name, String description, RoadNetwork rn, ArrayList<Vehicle> lVehicles) {
        this.name = name;
        this.description = description;
        this.rn = rn;
        this.listVehicles = lVehicles;
    }

    public Project() {
        this.listVehicles = new ArrayList();
        this.rn = new RoadNetwork();
    }

    public Project(int bdID, String name, String description) {
        this.bdID = bdID;
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the rn_dir
     */
    public File getRn_dir() {
        return rn_dir;
    }

    /**
     * @return the veh_dir
     */
    public File getVeh_dir() {
        return veh_dir;
    }

    /**
     * @return the rn
     */
    public RoadNetwork getRn() {
        return rn;
    }

    /**
     * @param rn_dir the rn_dir to set
     */
    public void setRn_dir(File rn_dir) {
        this.rn_dir = rn_dir;
    }

    /**
     * @param veh_dir the veh_dir to set
     */
    public void setVeh_dir(File veh_dir) {
        this.veh_dir = veh_dir;
    }

    /**
     * @param rn the rn to set
     */
    public void setRn(RoadNetwork rn) {
        this.rn = rn;
    }

    public ArrayList<Vehicle> getListVehicles() {
        return listVehicles;
    }

    public void setListVehicles(ArrayList<Vehicle> listVehicles) {
        this.listVehicles = listVehicles;
    }

    public void addOtherVehicle(Vehicle v1) {
        int count = 0;
        for (Vehicle v2 : listVehicles) {

            if (v2.getName().equalsIgnoreCase(v1.getName()) && vehicleIsEqual(v2, v1) == false) {
                count++;
            }
        }
        if (count > 0) {
            v1.setName(v1.getName() + "(" + count++ + ")");
            this.listVehicles.add(v1);
        }

    }

    public boolean addOtherSection(Section v1) {
        int count = 0;
        for (int i = 0; i <rn.getlSections().size(); i++) {
            Section v2 = rn.getlSections().get(i);
            if ((v2.getRoad_name().equalsIgnoreCase(v1.getRoad_name())
                    && v2.getBeginning_node().equalsIgnoreCase(v1.getBeginning_node())
                    && v2.getEnding_node().equalsIgnoreCase(v1.getEnding_node()))) {
                count++;
                v1.setRoad_name(v1.getRoad_name() + "(" + count++ + ")");
                this.rn.addSection(v1);

            }
            if (count == 0) {
                return false;
            }
        }
        return true;
    }

    public boolean vehicleIsEqual(Vehicle v1, Vehicle v2) {

        if (v1.getDescription().equalsIgnoreCase(v2.getDescription())
                && v1.getMass().equalsIgnoreCase(v2.getMass())
                && v1.getFuel().equalsIgnoreCase(v2.getFuel())
                && v1.getFrontal_area() == v2.getFrontal_area()
                && v1.getDrag_coef() == v2.getDrag_coef()
                && v1.getWheel_size() == v2.getWheel_size()) {
            return true;
        }
        return false;
    }

    public Vehicle getVehicle(String vName) {
        for (Vehicle v : listVehicles) {
            if (v.getName().equalsIgnoreCase(vName)) {
                return v;
            }
        }
        return null;
    }

    public int getBdID() {
        return bdID;
    }

    public void setBdID(int bdID) {
        this.bdID = bdID;
    }

}
