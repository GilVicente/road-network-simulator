/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Vehicle.Vehicle;
import java.util.ArrayList;
import road.RoadNetwork;

/**
 *
 * @author Yahkiller
 */
public class Simulation {

    private String id;
    private String description;
    private String projectName;

    private RoadNetwork rn;
    private ArrayList<Vehicle> lVeh;

    private ArrayList<TrafficPattern> lPattern;
    private ArrayList<SimulationRun> lRuns = new ArrayList();

    public Simulation() {
        this.lPattern = new ArrayList();
    }

    public Simulation(String id, String description, String projectName, ArrayList<TrafficPattern> lPattern,
            RoadNetwork rn) {
        this.id = id;
        this.description = description;
        this.lPattern = lPattern;
        this.projectName = projectName;
        this.rn = rn;
    }

    public Simulation(String id, String description, String projectName, ArrayList<TrafficPattern> lPattern,
            RoadNetwork rn, ArrayList<Vehicle> lVeh) {
        this.id = id;
        this.description = description;
        this.lPattern = lPattern;
        this.projectName = projectName;
        this.rn = rn;
        this.lVeh = lVeh;
    }

    

    /**
     * @return the rn
     */
    public RoadNetwork getRn() {
        return rn;
    }

    /**
     * @param rn the rn to set
     */
    public void setRn(RoadNetwork rn) {
        this.rn = rn;
    }

    /**
     * @return the lVeh
     */
    public ArrayList<Vehicle> getlVeh() {
        return lVeh;
    }

    /**
     * @param lVeh the lVeh to set
     */
    public void setlVeh(ArrayList<Vehicle> lVeh) {
        this.lVeh = lVeh;
    }

    /**
     * @return the name
     */
    public String getId() {
        return id;
    }

    /**
     * @param name the name to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the projectName
     */
    public String getProjectName() {
        return projectName;
    }

    /**
     * @param projectName the projectName to set
     */
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public ArrayList<TrafficPattern> getlPattern() {
        return lPattern;
    }

    public void setlPattern(ArrayList<TrafficPattern> lPattern) {
        this.lPattern = lPattern;
    }

    public void addTrafficPattern(TrafficPattern t) {
        this.lPattern.add(t);
    }

    /**
     * @return the lRuns
     */
    public ArrayList<SimulationRun> getlRuns() {
        return lRuns;
    }

    /**
     * @param lRuns the lRuns to set
     */
    public void setlRuns(ArrayList<SimulationRun> lRuns) {
        this.lRuns = lRuns;
    }
    
    public boolean containsRun(String name){
        boolean bool = false;
        for (SimulationRun run: this.lRuns) {
            if(name.equalsIgnoreCase(run.getName())){
                bool = true;
            }
        }
        return bool;
    }
}
