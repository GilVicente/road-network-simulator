/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Graphs;

/**
 *
 * @author Yahkiller
 */
public class dataWorkSFC {
    private double f;
    private double rpm;
    
    public dataWorkSFC(){
        
    }
    
    public dataWorkSFC(double w, double rpm){
        this.f=w;
        this.rpm=rpm;
    }

    /**
     * @return the w
     */
    public double getF() {
        return f;
    }

    /**
     * @param w the w to set
     */
    public void setF(double w) {
        this.f = w;
    }

    /**
     * @return the rpm
     */
    public double getRpm() {
        return rpm;
    }

    /**
     * @param rpm the rpm to set
     */
    public void setRpm(double rpm) {
        this.rpm = rpm;
    }
}
