/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Graphs;

import Utils.Utils;
import Vehicle.Vehicle;
import graphbase.Graph;
import graphbase.GraphAlgorithms;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import road.Junction;
import road.RoadNetwork;
import road.Section;
import road.Segment;

/**
 *
 * @author Yahkiller
 */
public class FastestGraph {

    private Graph<String, Double> roadNetwork;
    private ArrayList<Junction> lNodes;
    private ArrayList<Section> lSections;
    private RoadNetwork rn;
    private Vehicle v;

    public FastestGraph(RoadNetwork rn, Vehicle v, boolean isDirected) {
        this.rn = rn;
        this.v = v;
        this.lNodes = rn.getlNodes();
        this.lSections = rn.getlSections();
        this.roadNetwork = new Graph(isDirected);
        insertAllNodes();
        insertAllSections();
    }

    public boolean insertAllNodes() {
        if (!lNodes.isEmpty()) {
            //    addNode(sourceNode.getKey());
            for (Junction n : lNodes) {
                addNode(n.getKey());
            }
            // addActivity(finalNode.getKey());
            return true;
        } else {
            return false;
        }

    }

    public boolean addNode(String a) {
        if (!a.isEmpty()) {
            this.roadNetwork.insertVertex(a);
            return true;
        } else {
            System.out.println("Node Empty!");
            return false;
        }
    }

    public void insertAllSections() {
        double length = 0.0;

        for (Section s : lSections) {
            double vehTypLimit = v.getVelocityByTypology(s.getTypology());
            double time = 0.0;
            ArrayList<Segment> lsegms = rn.getOrderedSegments(s);
            for (Segment seg : lsegms) {
                double segVel = Utils.fromKmToDouble(seg.getMaxVel());
                if (segVel <= vehTypLimit) {
                    time += (Utils.fromKmToDouble(seg.getLenght()) / (segVel));
                } else {
                    time += (Utils.fromKmToDouble(seg.getLenght()) / (vehTypLimit));
                }
            }
            if (s.getDirection().equalsIgnoreCase("Direct")) {
                addEdge(s.getBeginning_node(), s.getEnding_node(), time, 0);
            } else if (s.getDirection().equalsIgnoreCase("Bidirectional")) {
                addEdge(s.getBeginning_node(), s.getEnding_node(), time, 0);
                addEdge(s.getEnding_node(), s.getBeginning_node(), time, 0);
            } else {
                addEdge(s.getEnding_node(), s.getBeginning_node(), time, 0);
            }
        }
    }

    public boolean addEdge(String n1, String n2, Double weight, int x) {
        if (!n1.isEmpty() && !n2.isEmpty() && weight >= 0) {
            this.roadNetwork.insertEdge(n1, n2, weight, 0);
            return true;
        } else {
            return false;
        }
    }

    public HashMap<ArrayList<Junction>, Double> getAllPaths(Junction a, Junction b) {
        ArrayList<Deque<String>> allPaths = GraphAlgorithms.allPaths(this.roadNetwork, a.getKey(), b.getKey());
        HashMap<ArrayList<Junction>, Double> map = new HashMap<ArrayList<Junction>, Double>();
        for (Deque<String> p : allPaths) {
            ArrayList<Junction> lNodes = new ArrayList<>();
            for (String n : p) {
                lNodes.add(rn.getNode(n));
            }
            double time = getPathTime(lNodes);
            map.put(lNodes, time * 60);
        }
        return map;
    }

    public double getPathTime(ArrayList<Junction> lNodes) {
        double time = 0.0;
        ArrayList<Section> lSections = routeEdgesByTime(lNodes, v);
        for (Section s : lSections) {
            ArrayList<Segment> lsegms = rn.getOrderedSegments(s);
            for (Segment seg : lsegms) {
                time += (Utils.fromKmToDouble(seg.getLenght()) / (Utils.fromKmToDouble(seg.getMaxVel())));
            }
        }
        return time;
    }

    public ArrayList<Junction> getShortestPath(Junction a, Junction b) {
        HashMap<ArrayList<Junction>, Double> map = getAllPaths(a, b);
        if (map.isEmpty()) {
            return null;
        }
        return Utils.getMinPath(map);
    }

    public double shortestPathLength(Junction a, Junction b) {
        Deque<String> d = new LinkedList();
        double time = GraphAlgorithms.shortestPath(roadNetwork, a.getKey(), b.getKey(), d);
        return time;
    }

    //Rout Edges
    public ArrayList<Section> routeEdgesByTime(ArrayList<Junction> lNodes, Vehicle v) {
        ArrayList<Section> lSecs = new ArrayList<>();
        for (int i = 0; i < lNodes.size() - 1; i++) {

            ArrayList<Section> ls = rn.getSections(lNodes.get(i).getKey(), lNodes.get(i + 1).getKey());
            Section s = getMinTimeSection(ls, v);
            lSecs.add(s);
        }
        return lSecs;
    }

    public double getSectionTime(Section s, Vehicle v) {
        double time = 0.0;
        double vMax = v.getVelocityByTypology(s.getTypology());
        ArrayList<Segment> lsegms = rn.getOrderedSegments(s);
        for (Segment seg : lsegms) {

            time += getSegmentTime(s, seg, v);
        }

        return time;
    }

    public double getSegmentTime(Section s, Segment seg, Vehicle v) {
        double vMax = v.getVelocityByTypology(s.getTypology());
        double time = 0.0;
        double segVel = Utils.fromKmToDouble(seg.getMaxVel());
        if (vMax < segVel && vMax != 0.0) {
            time = (Utils.fromKmToDouble(seg.getLenght()) / vMax);
        } else {
            time = (Utils.fromKmToDouble(seg.getLenght()) / segVel);
        }
        return time;
    }

    public Section getMinTimeSection(ArrayList<Section> ls, Vehicle v) {
        HashMap<Section, Double> map = new HashMap();
        for (Section s : ls) {
            map.put(s, getSectionTime(s, v));
        }
        return Utils.getMinSection(map);
    }

}
