/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Graphs;

import AnalisysController.EfficientController;
import Physics.CarPhysics;
import Utils.Utils;
import Vehicle.Vehicle;
import graphbase.Graph;
import graphbase.GraphAlgorithms;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import road.Junction;
import road.RoadNetwork;
import road.Section;
import road.Segment;

/**
 *
 * @author Yahkiller
 */
public class ConsumptionGraph {

    private Graph<String, Double> roadNetwork;
    private ArrayList<Junction> lNodes;
    private ArrayList<Section> lSections;
    private RoadNetwork rn;
    private double consumption;
    private EfficientController ec;

    public ConsumptionGraph(EfficientController ec, boolean isDirected) {
        this.ec = ec;
        this.rn = ec.getRn();
        this.lNodes = rn.getlNodes();
        this.lSections = rn.getlSections();
        this.roadNetwork = new Graph(isDirected);
        insertAllNodes();
        insertAllSections();
    }

    public boolean insertAllNodes() {
        if (!lNodes.isEmpty()) {
            //    addNode(sourceNode.getKey());
            for (Junction n : lNodes) {
                addNode(n.getKey());
            }
            // addActivity(finalNode.getKey());
            return true;
        } else {
            return false;
        }

    }

    public boolean addNode(String a) {
        if (!a.isEmpty()) {
            this.roadNetwork.insertVertex(a);
            return true;
        } else {
            System.out.println("Node Empty!");
            return false;
        }
    }

    public void insertAllSections() {

        for (Section s : lSections) {
            double length = rn.getSectionLength(s);
            double en = (consumption * length) / 100;

            if (s.getDirection().equalsIgnoreCase("Direct")) {
                addEdge(s.getBeginning_node(), s.getEnding_node(), en, 0);
            } else if (s.getDirection().equalsIgnoreCase("Bidirectional")) {
                addEdge(s.getBeginning_node(), s.getEnding_node(), en, 0);
                addEdge(s.getEnding_node(), s.getBeginning_node(), en, 0);
            } else {
                addEdge(s.getEnding_node(), s.getBeginning_node(), en, 0);
            }
        }
    }

    public boolean addEdge(String n1, String n2, Double weight, int x) {
        if (!n1.isEmpty() && !n2.isEmpty() && weight >= 0) {
            this.roadNetwork.insertEdge(n1, n2, weight, 0);
            return true;
        } else {
            return false;
        }
    }

    public HashMap<ArrayList<Junction>, Double> getAllPaths(Junction a, Junction b) {
        ArrayList<Deque<String>> allPaths = GraphAlgorithms.allPaths(this.roadNetwork, a.getKey(), b.getKey());
        HashMap<ArrayList<Junction>, Double> map = new HashMap<ArrayList<Junction>, Double>();
        for (Deque<String> p : allPaths) {
            ArrayList<Junction> lNodes = new ArrayList<>();
            for (String n : p) {
                lNodes.add(rn.getNode(n));
            }
            double en = getPathConsumption(lNodes);
            map.put(lNodes, en);
        }
        return map;
    }

    public double getPathConsumption(ArrayList<Junction> lNodes) {
        double en = 0.0;

        ArrayList<Section> lSections = ec.routeEdgesByConsumption(lNodes, ec.getV());
        return getPathConsumptionSections(lSections);
    }

    public double getPathConsumptionSections(ArrayList<Section> lSections) {
        double en = 0.0;
        int gear = ec.getV().getEnergy().GetMaxGear();
        int throt = 0;

        for (Section s : lSections) {
            Data1 d = getSectionConsumption(s, ec.getV(), gear, throt);
            gear = d.getGear();
            throt = d.getThrottle();
            en += d.getEnergy();
        }
        return en;
    }

    public Data1 getSectionConsumption(Section s, Vehicle v, int gear, int throt) {
        double en = 0.0;
        Data1 d = new Data1();
        for (Segment seg : s.getlSegments()) {

            if (v.getMotor().equalsIgnoreCase("Combustion")) {

                if (Utils.withoutPercent(seg.getSlope()) != 0) {
                    d = combustion(false, s, seg, gear, throt);
                    if (v.getFuel().equalsIgnoreCase("Gasoline")) {
                        en += d.getEnergy() * 44.4;
                    } else if (v.getFuel().equalsIgnoreCase("Diesel")) {
                        en += d.getEnergy() * 48;
                    }

                    gear = d.getGear();
                    throt = d.getThrottle();
                } else {
                    d = combustion(true, s, seg, gear, throt);
                    if (v.getFuel().equalsIgnoreCase("Gasoline")) {
                        en += d.getEnergy() * 44.4;
                    } else if (v.getFuel().equalsIgnoreCase("Diesel")) {
                        en += d.getEnergy() * 48;
                    }
                    gear = d.getGear();
                    throt = d.getThrottle();
                }
            } else if (v.getMotor().equalsIgnoreCase("Electric")) {
                if (Utils.withoutPercent(seg.getSlope()) == 0) {
                    d = electric(true, s, seg, gear, throt);
                    en += d.getEnergy() / 1000;
                    gear = d.getGear();
                    throt = d.getThrottle();
                } else if (Utils.withoutPercent(seg.getSlope()) > 0) {
                    d = electric(false, s, seg, 1, throt);
                    en += d.getEnergy() / 1000;
                    gear = d.getGear();
                    throt = d.getThrottle();
                } else {
                    d = electric(false, s, seg, 1, throt);
                    double regen = electricRegenerartion(ec.getV(), s, seg, throt, 1);
                    en += (d.getEnergy() + regen) / 1000;
                    gear = d.getGear();
                    throt = d.getThrottle();

                }
            }

        }
        d.setEnergy(en);
        d.setGear(gear);
        return d;
    }

    public ArrayList<Junction> getLessConsumptionPath(Junction a, Junction b) {
        HashMap<ArrayList<Junction>, Double> map = getAllPaths(a, b);
        return Utils.getMinPath(map);
    }

    public double lessConsumptionPathLength(Junction a, Junction b) {
        Deque<String> d = new LinkedList();
        double en = GraphAlgorithms.shortestPath(roadNetwork, a.getKey(), b.getKey(), d);
        return en;

    }

    public Data1 combustion(boolean isPlane, Section s, Segment seg, int g, int throt) {
        Data1 d = new Data1();
        
        dataWorkSFC d2 = CarPhysics.calculateForce(isPlane, ec.getV(), s, seg, throt, g);
        double f = d2.getF();

        while (f <= 0  && throt < 2) {
            throt++;
            d2 = CarPhysics.calculateForce(isPlane, ec.getV(), s, seg, throt, g);
            f = d2.getF();
        }

        while (f <= 0 && g > 0) {
            throt = 1;
            g--;
            d2 = CarPhysics.calculateForce(isPlane, ec.getV(), s, seg, throt, g);
            f = d2.getF();

            while (f <= 0 && throt <2) {
                throt++;
                d2 = CarPhysics.calculateForce(isPlane, ec.getV(), s, seg, throt, g);
                f = d2.getF();
            }
        }
        //aumentar mudanca
        if (f > 0 && throt < 3 && throt > 0) {
            int throt2 = throt - 1;
            int throt_id2 = getThrottleID(throt2);
            double f2 = CarPhysics.calculateForce(isPlane, ec.getV(), s, seg, throt2, g).getF();
            while (f2 > 0 && throt2 >= 0) {
                throt_id2 = ec.getV().getEnergy().getlThrottle().get(throt2).getId();
                d2 = CarPhysics.calculateForce(isPlane, ec.getV(), s, seg, throt2, g);
                f = d2.getF();
                throt--;
                throt2 = throt - 1;
            }
        }

        if (f > 0 && g < ec.getV().getEnergy().GetMaxGear()) {
            int g2 = g + 1;
            while ((CarPhysics.calculateForce(isPlane, ec.getV(), s, seg, 0, g2).getF() > 0) && (g <= ec.getV().getEnergy().GetMaxGear()) && (g2 < (ec.getV().getEnergy().GetMaxGear()))) {
                throt = 1;
                g++;
                d2 = CarPhysics.calculateForce(isPlane, ec.getV(), s, seg, 0, g);
                f = d2.getF();
                g2++;
            }
        }

        double rpm = d2.getRpm();
        double w = CarPhysics.calculateWork(f, (Utils.fromKmToDouble(seg.getLenght()) * 1000));
  
        
        if(throt>2){
            System.out.println("X");
        }
        double SFC = CarPhysics.calculateSFC(rpm, ec.getV(), throt);
        double kwh = Utils.converterJtoKWh(w);
        double res = SFC * kwh;

        d.setEnergy(res);
        d.setGear(g);
        d.setThrottle(throt);
        return d;
    }

    public Data1 electric(boolean isPlane, Section s, Segment seg, int g, int throt) {
        Data1 d = new Data1();
     
        dataWorkSFC d2 = CarPhysics.calculateForce(isPlane, ec.getV(), s, seg, throt, g);
        double f = d2.getF();

        while (f <= 0 && throt < 2) {
            throt++;
          
            d2 = CarPhysics.calculateForce(isPlane, ec.getV(), s, seg, throt, g);
            f = d2.getF();
        }

        if (f > 0 && throt <= 3 && throt > 0) {
            int throt2 = throt - 1;
            int throt_id2 = ec.getV().getEnergy().getlThrottle().get(throt2).getId();
            double f2 = CarPhysics.calculateForce(isPlane, ec.getV(), s, seg, throt2, g).getF();
            while (f2 > 0 && throt2 >= 0) {
                throt_id2 = ec.getV().getEnergy().getlThrottle().get(throt2).getId();
                d2 = CarPhysics.calculateForce(isPlane, ec.getV(), s, seg, throt2, g);
                f = d2.getF();
                throt--;
                throt2 = throt - 1;
            }
        }

        double w = CarPhysics.calculateWork(f, (Utils.fromKmToDouble(seg.getLenght()) * 1000));
//        double res = Utils.converterJtoKWh(w);

        d.setEnergy(w);
        d.setGear(g);
        d.setThrottle(throt);
        return d;
    }

    public static double electricRegenerartion(Vehicle vehicle, Section sec, Segment seg, int throt, int gear) {
//        throttle_id = vehicle.getEnergy().getlThrottle().get(throt).getId();
        double fMotor = CarPhysics.calculateCarForce(vehicle, sec, seg, throt, gear);
        double fT = CarPhysics.calculateSlopeTotalResistanceForces(vehicle, sec, seg);
        if (fMotor >= fT) {
            double w = fT * Utils.fromKmToDouble(seg.getLenght()) * vehicle.getEnergy().getEnergy_regeneration_ratio() * 1000;
            return w;
        } else {
            double w = fMotor * Utils.fromKmToDouble(seg.getLenght()) * vehicle.getEnergy().getEnergy_regeneration_ratio() * 1000;
            return w;
        }
    }

    public static int getThrottleID(int throt) {
        if (throt == 0) {
            return 25;
        } else if (throt == 1) {
            return 50;
        } else if (throt == 2) {
            return 100;
        }
        return 0;
    }

}
