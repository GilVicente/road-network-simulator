/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Graphs;

/**
 *
 * @author Yahkiller
 */
public class Data1 {

    private int gear;
    private int throttle;
    private double energy;

    public Data1() {

    }

    public Data1(int gear, double energy, int throttle) {
        this.gear = gear;
        this.energy = energy;
        this.throttle =throttle;
    }

    /**
     * @return the gear
     */
    public int getGear() {
        return gear;
    }

    /**
     * @param gear the gear to set
     */
    public void setGear(int gear) {
        this.gear = gear;
    }

    /**
     * @return the energy
     */
    public double getEnergy() {
        return energy;
    }

    /**
     * @param energy the energy to set
     */
    public void setEnergy(double energy) {
        this.energy = energy;
    }

    /**
     * @return the throttle
     */
    public int getThrottle() {
        return throttle;
    }

    /**
     * @param throttle the throttle to set
     */
    public void setThrottle(int throttle) {
        this.throttle = throttle;
    }
}
