/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Graphs;

import Utils.Utils;
import graphbase.Graph;
import graphbase.GraphAlgorithms;
import graphbase.Vertex;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import road.Junction;
import road.RoadNetwork;
import road.Section;

/**
 *
 * @author Yahkiller
 */
public class TollGraph {

    private Graph<String, Double> roadNetwork;
    private ArrayList<Junction> lNodes;
    private ArrayList<Section> lSections;
    private RoadNetwork rn;

    public TollGraph(RoadNetwork rn, boolean isDirected) {
        this.rn = rn;
        this.lNodes = rn.getlNodes();
        this.lSections = rn.getlSections();
        this.roadNetwork = new Graph(isDirected);
        insertAllNodes();
        insertAllSections();
    }

    public boolean insertAllNodes() {
        if (!lNodes.isEmpty()) {
            //    addNode(sourceNode.getKey());
            for (Junction n : lNodes) {
                addNode(n.getKey());
            }
            // addActivity(finalNode.getKey());
            return true;
        } else {
            return false;
        }

    }

    public boolean addNode(String a) {
        if (!a.isEmpty()) {
            this.roadNetwork.insertVertex(a);
            return true;
        } else {
            System.out.println("Node Empty!");
            return false;
        }
    }

    public void insertAllSections() {
        for (Section s : lSections) {
            if (s.getDirection().equalsIgnoreCase("Direct")) {
                addEdge(s.getBeginning_node(), s.getEnding_node(), s.getToll(), 0);
            } else if (s.getDirection().equalsIgnoreCase("Bidirectional")) {
                addEdge(s.getBeginning_node(), s.getEnding_node(), s.getToll(), 0);
                addEdge(s.getEnding_node(), s.getBeginning_node(), s.getToll(), 0);
            } else {
                addEdge(s.getEnding_node(), s.getBeginning_node(), s.getToll(), 0);
            }
        }
    }

    public boolean addEdge(String n1, String n2, Double weight, int x) {
        if (!n1.isEmpty() && !n2.isEmpty() && weight >= 0) {
            this.roadNetwork.insertEdge(n1, n2, weight, 0);
            return true;
        } else {
            return false;
        }
    }

    public HashMap<ArrayList<Junction>, Double> getAllPaths(Junction a, Junction b) {
        ArrayList<Deque<String>> allPaths = GraphAlgorithms.allPaths(this.roadNetwork, a.getKey(), b.getKey());
        HashMap<ArrayList<Junction>, Double> map = new HashMap<ArrayList<Junction>, Double>();
        for (Deque<String> p : allPaths) {
            ArrayList<Junction> lNodes = new ArrayList<>();
            for (String n : p) {
                lNodes.add(rn.getNode(n));
            }
            double tollSum = getPathTollCost(lNodes);
            map.put(lNodes, tollSum);
        }
        return map;
    }

    public ArrayList<Junction> getShortestPathTollCost(Junction a, Junction b) {
        HashMap<ArrayList<Junction>, Double> map = getAllPaths(a, b);
        return Utils.getMinPath(map);
    }

    public double getPathTollCost(ArrayList<Junction> lNodes) {
        double tollSum = 0;
        ArrayList<Section> lSections = routeEdgesByToll(lNodes);
        for (Section s : lSections) {
            tollSum += s.getToll();
        }
        return tollSum;
    }

    public double lowestPathTollCost(Junction a, Junction b) {
        Deque<String> d = new LinkedList();
        double length = GraphAlgorithms.shortestPath(roadNetwork, a.getKey(), b.getKey(), d);
        return length;
    }

    public ArrayList<Section> routeEdgesByToll(ArrayList<Junction> lNodes) {
        ArrayList<Section> lSecs = new ArrayList<>();
        for (int i = 0; i < lNodes.size() - 1; i++) {

            ArrayList<Section> ls = rn.getSections(lNodes.get(i).getKey(), lNodes.get(i + 1).getKey());
            Section s = getMinSectionToll(ls);
            lSecs.add(s);
        }
        return lSecs;
    }

    public Section getMinSectionToll(ArrayList<Section> ls) {
        HashMap<Section, Double> map = new HashMap();
        for (Section s : ls) {
            map.put(s, s.getToll());
        }
        return Utils.getMinSection(map);
    }

}
