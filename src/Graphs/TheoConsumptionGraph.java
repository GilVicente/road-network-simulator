/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Graphs;

import AnalisysController.EfficientController;
import AnalisysController.TheoEfficientController;
import Physics.CarPhysics;
import Utils.Utils;
import Vehicle.Vehicle;
import graphbase.Graph;
import graphbase.GraphAlgorithms;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import road.Junction;
import road.RoadNetwork;
import road.Section;
import road.Segment;

/**
 *
 * @author Yahkiller
 */
public class TheoConsumptionGraph {

    private Graph<String, Double> roadNetwork;
    private ArrayList<Junction> lNodes;
    private ArrayList<Section> lSections;
    private RoadNetwork rn;
    private double consumption;
    private TheoEfficientController ec;

    public TheoConsumptionGraph(TheoEfficientController ec, boolean isDirected) {
        this.ec = ec;
        this.rn = ec.getRn();
        this.lNodes = rn.getlNodes();
        this.lSections = rn.getlSections();
        this.roadNetwork = new Graph(isDirected);
        insertAllNodes();
        insertAllSections();
    }

    public boolean insertAllNodes() {
        if (!lNodes.isEmpty()) {
            //    addNode(sourceNode.getKey());
            for (Junction n : lNodes) {
                addNode(n.getKey());
            }
            // addActivity(finalNode.getKey());
            return true;
        } else {
            return false;
        }

    }

    public boolean addNode(String a) {
        if (!a.isEmpty()) {
            this.roadNetwork.insertVertex(a);
            return true;
        } else {
            System.out.println("Node Empty!");
            return false;
        }
    }

    public void insertAllSections() {

        for (Section s : lSections) {
            double length = rn.getSectionLength(s);
            double en = (consumption * length) / 100;

            if (s.getDirection().equalsIgnoreCase("Direct")) {
                addEdge(s.getBeginning_node(), s.getEnding_node(), en, 0);
            } else if (s.getDirection().equalsIgnoreCase("Bidirectional")) {
                addEdge(s.getBeginning_node(), s.getEnding_node(), en, 0);
                addEdge(s.getEnding_node(), s.getBeginning_node(), en, 0);
            } else {
                addEdge(s.getEnding_node(), s.getBeginning_node(), en, 0);
            }
        }
    }

    public boolean addEdge(String n1, String n2, Double weight, int x) {
        if (!n1.isEmpty() && !n2.isEmpty() && weight >= 0) {
            this.roadNetwork.insertEdge(n1, n2, weight, 0);
            return true;
        } else {
            return false;
        }
    }

    public HashMap<ArrayList<Junction>, Double> getAllPaths(Junction a, Junction b) {
        ArrayList<Deque<String>> allPaths = GraphAlgorithms.allPaths(this.roadNetwork, a.getKey(), b.getKey());
        HashMap<ArrayList<Junction>, Double> map = new HashMap<ArrayList<Junction>, Double>();
        for (Deque<String> p : allPaths) {
            ArrayList<Junction> lNodes = new ArrayList<>();
            for (String n : p) {
                lNodes.add(rn.getNode(n));
            }
            double en = getPathConsumption(lNodes);
            map.put(lNodes, en);
        }
        return map;
    }

    public double getPathConsumption(ArrayList<Junction> lNodes) {
        double en = 0.0;

        ArrayList<Section> lSections = ec.routeEdgesByConsumption(lNodes, ec.getV());
        return getPathConsumptionSections(lSections);
    }

    public double getPathConsumptionSections(ArrayList<Section> lSections) {
        double en = 0.0;
        int gear = ec.getV().getEnergy().GetMaxGear();
        int throt = 0;

        for (Section s : lSections) {
            Data1 d = getSectionConsumption(s, ec.getV(), gear, throt);
            gear = d.getGear();
            throt = d.getThrottle();
            en += d.getEnergy();
        }
        return en;
    }

    public Data1 getSectionConsumption(Section s, Vehicle v, int gear, int throt) {
        double en = 0.0;
        Data1 d = new Data1();
        double l = 0.0;
        for (Segment seg : s.getlSegments()) {

            if (v.getMotor().equalsIgnoreCase("Combustion")) {

                if (Utils.withoutPercent(seg.getSlope()) != 0) {
                    l += Utils.fromKmToDouble(seg.getLenght());
                    en += combustion(false, s, seg, gear, throt);

                } else {
                    en += combustion(true, s, seg, gear, throt);
                    l += Utils.fromKmToDouble(seg.getLenght());
                }
            } else if (v.getMotor().equalsIgnoreCase("Electric")) {
                if (Utils.withoutPercent(seg.getSlope()) != 0) {
                    l += Utils.fromKmToDouble(seg.getLenght());
                    en += combustion(false, s, seg, 1, throt);

                } else {
                    en += combustion(true, s, seg, 1, throt);
                    l += Utils.fromKmToDouble(seg.getLenght());
                }
            }

        }
        double work = en * l * 1000; //joules
        d.setEnergy(work/1000);
        d.setGear(gear);
        return d;
    }

    public double combustion(boolean isPlane, Section s, Segment seg, int g, int throt) {
        if (isPlane == true) {
            return CarPhysics.calculatePlaneTotalResistanceForces(ec.getV(), s, seg);
        }
        return CarPhysics.calculateSlopeTotalResistanceForces(ec.getV(), s, seg);
    }

    public ArrayList<Junction> getLessConsumptionPath(Junction a, Junction b) {
        HashMap<ArrayList<Junction>, Double> map = getAllPaths(a, b);
        return Utils.getMinPath(map);
    }

    public double lessConsumptionPathLength(Junction a, Junction b) {
        Deque<String> d = new LinkedList();
        double en = GraphAlgorithms.shortestPath(roadNetwork, a.getKey(), b.getKey(), d);
        return en;

    }
}
