/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SimulationUI;

import AnalisysController.FastestController;
import DataHandlerRoadNetworkSelects.SelectNodes;
import DataHandlerRoadNetworkSelects.SelectSections;
import Model.DataHandler;
import Model.Project;
import Model.Simulation;
import Model.SimulationRun;
import Model.TrafficPattern;
import Vehicle.Time;
import Vehicle.Vehicle;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import road.RoadNetwork;
import road.RoadNetworkController;
import road.Section;
import road.Segment;

/**
 *
 * @author Yahkiller
 */
public class RunSimulatorController {

    private SimulationRun s_run;
    private ArrayList<Vehicle> finalVehicles;
    private ArrayList<Vehicle> garbage = new ArrayList();

    String jdbcUrl = "jdbc:oracle:thin://@gandalf.dei.isep.ipp.pt:1521/pdborcl";

    String username = "LAPR3_32";
    String password = "qwerty";

    DataHandler dh = new DataHandler(jdbcUrl, username, password);

    boolean isRunnig;
    private String alg;
    private Project actProj;
    private Simulation s;

    public RunSimulatorController(Simulation s, SimulationRun s_run, Project actProj, String alg) {
        this.s = s;
        this.s_run = s_run;
        this.actProj = actProj;
        this.alg = alg;
        this.finalVehicles = new ArrayList();
    }

    public RunSimulatorController() {

    }

    public void run() throws IOException, SQLException {
        dh.openConnection();
        dh.getVehicle(actProj);
        SelectNodes d = new SelectNodes();
        SelectSections f = new SelectSections();
        SelectSections c = new SelectSections();
        d.getAllNodes(dh, actProj);
        f.getAllSections(dh, actProj);
        c.getAllSections(dh, actProj);

        RoadNetworkController r = new RoadNetworkController(true, actProj.getRn());
        RoadNetwork rn = r.getRn();

        ArrayList<TrafficPattern> lPats = s.getlPattern();

        for (double i = 0; i < s_run.getDuration(); i += s_run.getTimeStep()) {

            for (TrafficPattern pat : lPats) {
                ArrayList<Section> ls = r.bestPathSections(alg, pat.getBegin(), pat.getEnd(), actProj.getVehicle(pat.getlVeh()));
                FastestController fc = new FastestController(r);

                double nVeh = s_run.getTimeStep() / poissonExpression(pat.getArrival_rate());
                int n = (int) nVeh;

                for (int j = 0; j < n; j++) {
                    Vehicle v = actProj.getVehicle(pat.getlVeh());
                    Time in = new Time(i, ls.get(0).getlSegments().get(0));
                    if (ls.get(0).getlSegments().get(0).getDue().size() < ls.get(0).getlSegments().get(0).getMax_vehicles()) {
                        if (!v.hasSegInTime(in)) {
                            v.getInTime().add(in);
                        }

                        ls.get(0).getlSegments().get(0).addVehicle(v);
                    } else {
                        this.garbage.add(v);
                    }
                }
//Rebenta nas sections e nos segs
                ArrayList<Segment> lSecSeg = getAllSegments(ls);
                percorreSegmentos(lSecSeg, rn, fc, i);
            }
        }
        this.printVeh(this.finalVehicles);

    }

    public void percorreSegmentos(ArrayList<Segment> lSegments, RoadNetwork rn, FastestController fc, double i) {

        for (int x = 0; x < lSegments.size() - 1; x++) {
            Segment seg = lSegments.get(x);
            Section sec = rn.getSection(seg.getId_section());
            Segment nextSeg = lSegments.get(x++);
            if (!seg.getDue().isEmpty()) {
                for (int j = 0; j < seg.getDue().size(); j++) {
                    Vehicle v = seg.getDue().get(j);
                    double segTime = fc.getSegmentTime(sec, seg, v);

                    if (v.getInTime(seg) + segTime < i) {
                        if (nextSeg.getDue().size() < nextSeg.getMax_vehicles()) {
                            Time t = new Time(i, seg);
                            if (!v.hasSegOutTime(t)) {
                                v.addOutTime(t);
                            }
                            Time t2 = new Time(i, nextSeg);
                            nextSeg.addVehicle(v);
                            if (!v.hasSegInTime(t2)) {
                                v.addInTime(t2);
                            }
                            //seg.setVehicleOut(v);//ativar uma flag
                            seg.getDue().remove(v);
                        }
                    }

                }

            }
        }

        int max = lSegments.size() - 1;
        Segment seg = lSegments.get(max);
        Section sec = rn.getSection(seg.getId_section());

        ArrayList<Vehicle> toBeRemoved = new ArrayList();
        if (!seg.getDue().isEmpty()) {
            for (Vehicle v : seg.getDue()) {
                if (v.getInTime(seg) + fc.getSegmentTime(sec, seg, v) < i) {
                    Time t = new Time(i, seg);
                    if (!v.hasSegOutTime(t)) {
                        v.addOutTime(t);
                    }
                    this.finalVehicles.add(v);
                    toBeRemoved.add(v);
                }
            }
            seg.getDue().removeAll(toBeRemoved);
        }

    }

    public ArrayList<Segment> getAllSegments(ArrayList<Section> lSections) {
        ArrayList<Segment> lSecSeg = new ArrayList();
        for (Section s : lSections) {
            for (Segment seg : s.getlSegments()) {
                lSecSeg.add(seg);
            }
        }
        return lSecSeg;
    }

    public static double poissonExpression(String str_rate) {
        String temp = str_rate.substring(0, str_rate.length() - 4);
        int rate = Integer.parseInt(temp);
        double r = 1d / rate;
        double U = Math.random();
        double log = -((Math.log(1 - U)));
        double t = log / rate;
        return t;
    }

    public void printVeh(ArrayList<Vehicle> lVeh) {
        for (Vehicle v : lVeh) {
            System.out.println("");
            System.out.println(v.getName());
            ArrayList<Time> in = v.getInTime();
            System.out.print("IN TIME: ");
            for (Time t1 : in) {
                System.out.print(t1.getS().getId_section() + ""
                        + t1.getS().getId_segment() + " " + t1.getTime() + " | ");
            }
            System.out.println("");
            System.out.println("OUT TIME: ");
            ArrayList<Time> out = v.getOutTime();
            for (Time t2 : out) {
                System.out.print(t2.getS().getId_section() + "" + t2.getS().getId_segment() + " " + t2.getTime() + " | ");
            }
        }

        System.out.println("GARBAGE");

        for (Vehicle v : this.garbage) {
            System.out.println(v.getName());
        }
    }

    public ArrayList<Vehicle> getFinalVehicles() {
        return finalVehicles;
    }

    public void setFinalVehicles(ArrayList<Vehicle> finalVehicles) {
        this.finalVehicles = finalVehicles;
    }

    public ArrayList<Vehicle> getGarbage() {
        return garbage;
    }

    public void setGarbage(ArrayList<Vehicle> garbage) {
        this.garbage = garbage;
    }

}
