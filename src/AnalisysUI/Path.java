/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AnalisysUI;

import Vehicle.Vehicle;
import java.util.ArrayList;
import road.Junction;

/**
 *
 * @author Yahkiller
 */
public class Path {

    private String orig;
    private String dest;
    private String algorithm;
    private String value;
    private double cost;
    private double time;
    private double lenght;
    private double energy;
    private String strPath;
    private Vehicle v;
    private ArrayList<Junction> path;

    public Path(String algorithm, String orig, String dest, ArrayList<Junction> path) {
        this.algorithm = algorithm;
        this.orig = orig;
        this.dest = dest;
        this.path = path;
    }

    /**
     * @return the orig
     */
    public String getOrig() {
        return orig;
    }

    /**
     * @param orig the orig to set
     */
    public void setOrig(String orig) {
        this.orig = orig;
    }

    /**
     * @return the dest
     */
    public String getDest() {
        return dest;
    }

    /**
     * @param dest the dest to set
     */
    public void setDest(String dest) {
        this.dest = dest;
    }

    /**
     * @return the algorithm
     */
    public String getAlgorithm() {
        return algorithm;
    }

    /**
     * @param algorithm the algorithm to set
     */
    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    /**
     * @return the path
     */
    public ArrayList<Junction> getPath() {
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(ArrayList<Junction> path) {
        this.path = path;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public double getLenght() {
        return lenght;
    }

    public void setLenght(double lenght) {
        this.lenght = lenght;
    }

    public double getEnergy() {
        return energy;
    }

    public void setEnergy(double energy) {
        this.energy = energy;
    }

    public String getStrPath() {
        return strPath;
    }

    public void setStrPath(String strPath) {
        this.strPath = strPath;
    }

    public Vehicle getV() {
        return v;
    }

    public void setV(Vehicle v) {
        this.v = v;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }
}
